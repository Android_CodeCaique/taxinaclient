package com.example.taxinaclient.utils;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.Log;

import java.util.Locale;

public class LanguageHelper {
    public static void ChangeLang(Resources resources , String locale1){
        Configuration Config ;
        Config = new Configuration(resources.getConfiguration());

        switch (locale1){
            case "ar" :
                Locale locale = new Locale(locale1);
                Config.locale = new Locale("ar");
                Config.setLayoutDirection(locale);
                Log.d("HERE" , "ar");
                break;

            case "en" :
                Locale locale2 = new Locale(locale1);
                Config.locale = new Locale("en");
                Log.d("HERE" , "en");
                Config.setLayoutDirection(locale2);
                break;
        }
        resources.updateConfiguration(Config,resources.getDisplayMetrics());
    }
}
