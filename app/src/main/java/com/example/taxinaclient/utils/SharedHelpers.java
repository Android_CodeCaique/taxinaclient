package com.example.taxinaclient.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedHelpers {
    SharedPreferences sharedPreferences  = null ;
    SharedPreferences.Editor editor= null ;

    public static void  putKey(Context context, String Key, String Value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Taxina", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Key, Value);
        editor.commit();
    }

    public static String   getKey(Context contextGetKey, String Key)  {
        SharedPreferences sharedPreferences = contextGetKey.getSharedPreferences("Taxina", Context.MODE_PRIVATE);
        return sharedPreferences.getString(Key, "");
    }
}
