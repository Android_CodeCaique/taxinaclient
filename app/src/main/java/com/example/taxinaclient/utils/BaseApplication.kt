package com.example.taxinaclient.utils

//import ApiClient.netModule

import android.app.Application

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

class BaseApplication: Application() {

//    var listofModules = module {
//        single { ApiClientGeneral() }
//    }

    override fun onCreate() {
        super.onCreate()

        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

//        startKoin {
//
//            androidLogger()
//            androidContext(this@BaseApplication)
//            modules(listOf(homeGeneralModule,
//                homeGeneralContactUs,
//                homeGeneralAboutUs,
//                welcomeModule,
//                welcomeModuleLogin,
//                welcomeModuleRegister,
//                welcomeModuleForgetPassword,
//                welcomeModuleResetPassword,
//                welcomeModuleVerifyCode,
//                welcomeModuleChangePasswordHome,
//                welcomeModuleMore,
//                welcomeModuleProfile,
//                homeServiceModule,
//                homeServiceModuleCart,
//                homeServiceModulePaymentWay,
//                homeServiceModuleShipping,
//                homeServiceModuleFavorite,
//                homeServiceModuleHome,
//                homeServiceModuleFilter,
//                homeServiceModuleMoreSetting,
//                homeServiceModuleNotification,
//                homeServiceModuleOrderData,
//                homeServiceModuleOrderStatus,
//                homeServiceModuleProduct,
//                homeServiceModuleProductDetails,
//                homeServiceModuleMainActivity))
//        }
    }
}