package com.example.taxinaclient.family.familyAccepted;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.responses.FamilyMembersAccepted;
import com.example.taxinaclient.ui.location.FavoriteLocRecyclerViewAdabter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FamilyAcceptedRecyclerAdapter extends RecyclerView.Adapter<ViewHolderFamilyAccpted> {
    List<FamilyMembersAccepted.DataBean> arrayList;
    Context context;
//    Geocoder geocoder;

    private FavoriteLocRecyclerViewAdabter.OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int i);
    }

    public void setOnItemClickListener(FavoriteLocRecyclerViewAdabter.OnItemClickListener listener) {
        this.mListener = listener;
    }


    public FamilyAcceptedRecyclerAdapter(List<FamilyMembersAccepted.DataBean> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
//        geocoder = new Geocoder(context);

    }

    View view;

    @NonNull
    @Override
    public ViewHolderFamilyAccpted onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.family_member_accepted_item,parent,false);
        ViewHolderFamilyAccpted viewHolderNotification = new ViewHolderFamilyAccpted(view,mListener);
        return viewHolderNotification;

       }

//    @Override
//    public void onBindViewHolder(@NonNull ViewHolderFamilyAccpted holder, int position) {
//        FamilyMembersAccepted.DataBean dataBean = arrayList.get(position);
//
//    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderFamilyAccpted holder, int position) {
        FamilyMembersAccepted.DataBean dataBean = arrayList.get(position);
        holder.fullName.setText(dataBean.getFirst_name()+" "+dataBean.getLast_name());
        holder.phone.setText(dataBean.getPhone());
        Picasso.get().load(dataBean.getImage()).into(holder.familyPersonImage);
        if(dataBean.getState().equals("yes"))
            holder.state.setBackgroundColor(Color.parseColor("#FF2ED22A"));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

class ViewHolderFamilyAccpted extends RecyclerView.ViewHolder{

    TextView fullName;
    TextView phone;
    ImageView familyPersonImage , state;
    public ViewHolderFamilyAccpted(@NonNull View itemView, final FavoriteLocRecyclerViewAdabter.OnItemClickListener listener ) {
        super(itemView);

        fullName = itemView.findViewById(R.id.name);
        phone = itemView.findViewById(R.id.phone);

        familyPersonImage = itemView.findViewById(R.id.personImage);
        state = itemView.findViewById(R.id.stateImg);


//        deleteImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (listener != null) {
//                    int position = getAdapterPosition();
//                    if (position != -1) {
//                        listener.onItemClick(position);
//                    }
//                }
//            }
//        });
    }
}