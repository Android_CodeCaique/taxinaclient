package com.example.taxinaclient.family.familyAccepted;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.taxinaclient.databinding.FragmentFamilyAcceptedBinding;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.pojo.responses.FamilyMembersAccepted;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FamilyAccepted extends Fragment {

    FragmentFamilyAcceptedBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentFamilyAcceptedBinding.inflate(inflater);

        getFamilyAccepted();

        return binding.getRoot();
    }

    private void getFamilyAccepted(){
        binding.mkLoaderId.setVisibility(View.VISIBLE);

        ((Car) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Car.class))
                .ShowFamilyMembersAccepted("bearer "+ SharedHelpers.getKey(requireContext(),"TokenFirebase"))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    binding.mkLoaderId.setVisibility(View.GONE);
                                    FamilyMembersAccepted myFavoritesResponse = new Gson().fromJson(responseData, FamilyMembersAccepted.class);
//                                    Toast.makeText(getActivity(), myFavoritesResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    FamilyAcceptedRecyclerAdapter familyAcceptedRecyclerAdapter = new FamilyAcceptedRecyclerAdapter(myFavoritesResponse.getData(),requireContext());
                                    binding.familyAcceptedRecyclerViewId.setAdapter(familyAcceptedRecyclerAdapter);
//                                    favoriteLocRecyclerViewAdabter.setOnItemClickListener(new FavoriteLocRecyclerViewAdabter.OnItemClickListener() {
//                                        @Override
//                                        public void onItemClick(int i) {
//                                            deleteFavorites(i+1);
//                                        }
//                                    });
                                }
                                else
                                {
                                    binding.mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                binding.mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            binding.mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        binding.mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

}