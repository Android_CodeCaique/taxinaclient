package com.example.taxinaclient.family;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.taxinaclient.family.familyAccepted.FamilyAccepted;
import com.example.taxinaclient.family.familyWaited.FamilyWaited;


public class PageAdapter extends FragmentPagerAdapter {

    int numoftabs;

    public PageAdapter(@NonNull FragmentManager fm, int numoftabs) {
        super(fm);
        this.numoftabs = numoftabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case (0):
                return new FamilyAccepted();
            case (1):
                return new FamilyWaited();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return numoftabs;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}