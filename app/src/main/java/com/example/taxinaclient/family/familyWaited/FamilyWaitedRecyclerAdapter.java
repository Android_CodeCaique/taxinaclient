package com.example.taxinaclient.family.familyWaited;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.responses.FamilyMembersAccepted;
import com.example.taxinaclient.pojo.responses.FamilyMembersWait;
import com.example.taxinaclient.ui.location.FavoriteLocRecyclerViewAdabter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FamilyWaitedRecyclerAdapter extends RecyclerView.Adapter<ViewHolderFamilyWaited> {
    List<FamilyMembersWait.DataBean> arrayList;
    Context context;
//    Geocoder geocoder;

    private FavoriteLocRecyclerViewAdabter.OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int i);
    }

    public void setOnItemClickListener(FavoriteLocRecyclerViewAdabter.OnItemClickListener listener) {
        this.mListener = listener;
    }


    public FamilyWaitedRecyclerAdapter(List<FamilyMembersWait.DataBean> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
//        geocoder = new Geocoder(context);

    }

    View view;

    @NonNull
    @Override
    public ViewHolderFamilyWaited onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.family_member_waited_item,parent,false);
        ViewHolderFamilyWaited viewHolderNotification = new ViewHolderFamilyWaited(view,mListener);
        return viewHolderNotification;

       }

//    @Override
//    public void onBindViewHolder(@NonNull ViewHolderFamilyAccpted holder, int position) {
//        FamilyMembersAccepted.DataBean dataBean = arrayList.get(position);
//
//    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderFamilyWaited holder, int position) {
        FamilyMembersWait.DataBean dataBean = arrayList.get(position);
        holder.fullName.setText(dataBean.getFirst_name()+" "+dataBean.getLast_name());
        holder.phone.setText(dataBean.getPhone());
        Picasso.get().load(dataBean.getImage()).into(holder.familyPersonImage);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

class ViewHolderFamilyWaited extends RecyclerView.ViewHolder{

    TextView fullName;
    TextView phone;
    ImageView familyPersonImage , state;
    Button acceptBtn;
    public ViewHolderFamilyWaited(@NonNull View itemView, final FavoriteLocRecyclerViewAdabter.OnItemClickListener listener ) {
        super(itemView);

        fullName = itemView.findViewById(R.id.name);
        phone = itemView.findViewById(R.id.phone);

        familyPersonImage = itemView.findViewById(R.id.personImage);
        state = itemView.findViewById(R.id.stateImg);

        acceptBtn = itemView.findViewById(R.id.acceptBtnId);

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != -1) {
                        listener.onItemClick(position);
                    }
                }
            }
        });
    }
}