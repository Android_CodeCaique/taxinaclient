package com.example.taxinaclient.family;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.taxinaclient.R;
import com.example.taxinaclient.databinding.FragmentFamilyTrackingBinding;
import com.example.taxinaclient.networking.Authentication;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.pojo.responses.BaseResponse;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.LoginResponse;
import com.example.taxinaclient.pojo.responses.SearchPhoneFamilyResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class FamilyTracking extends Fragment {

    FragmentFamilyTrackingBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentFamilyTrackingBinding.inflate(inflater);

        binding.searchBtnId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchFamily();
            }
        });

        return binding.getRoot();
    }

    private void searchFamily() {

        if(binding.etPhone.getText().toString().isEmpty()){
            Toast.makeText(requireContext(),getResources().getString(R.string.pleaseAddPhoneNumber),Toast.LENGTH_SHORT).show();
        }
        else {
            final String Token = SharedHelpers.getKey(getActivity(), "TokenFirebase");
            binding.mkLoaderId.setVisibility(View.VISIBLE);

            ((Car) new Retrofit.Builder()
                    .baseUrl("http://taxiApi.codecaique.com/api/")
                    .build()
                    .create(Car.class))
                    .searchPhoneFamily("bearer "+Token,binding.etPhone.getText().toString() )
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            if (response.code() == 200) {
                                try {
                                    String responseData = response.body().string();

                                    GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                    if (generalResponse.getError() == 0) {
                                        binding.mkLoaderId.setVisibility(View.GONE);
                                        SearchPhoneFamilyResponse searchPhoneFamilyResponse = new Gson().fromJson(responseData, SearchPhoneFamilyResponse.class);
                                        sendFamilyRequest(searchPhoneFamilyResponse.getData().getId());
//                                        Toast.makeText(requireContext(),searchPhoneFamilyResponse.getMessage(),Toast.LENGTH_SHORT).show();

                                    } else {
                                        binding.mkLoaderId.setVisibility(View.GONE);
                                        Toast.makeText(requireContext(),"No user data",Toast.LENGTH_SHORT).show();
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    binding.mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                binding.mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            binding.mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    public void sendFamilyRequest(int userId){
        final String Token = SharedHelpers.getKey(getActivity(), "TokenFirebase");
        binding.mkLoaderId.setVisibility(View.VISIBLE);

        ((Car) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Car.class))
                .sendFamilyRequest("bearer "+Token,userId )
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200) {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    binding.mkLoaderId.setVisibility(View.GONE);
                                    BaseResponse searchPhoneFamilyResponse = new Gson().fromJson(responseData, BaseResponse.class);

                                    Toast.makeText(requireContext(),searchPhoneFamilyResponse.getMessage(),Toast.LENGTH_SHORT).show();

                                } else {
                                    binding.mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(requireContext(),"No user data",Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                binding.mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            binding.mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        binding.mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}