package com.example.taxinaclient.updateprofile;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Authentication;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.InfoDriverTripResponse;
import com.example.taxinaclient.pojo.responses.LoginResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateProfile extends Fragment {
    private View view ;
    private EditText email , phone , password , confirmPassword;
    private MKLoader mkLoaderId ;
    private Button btn_UpdateProfile ;
    private boolean ChangePasswordT ;

    public UpdateProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_update_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        email = view.findViewById(R.id.email);
        phone = view.findViewById(R.id.phone);
        password = view.findViewById(R.id.password);
        confirmPassword = view.findViewById(R.id.confirmPassword);

        mkLoaderId = view.findViewById(R.id.mkLoaderId);
        btn_UpdateProfile = view.findViewById(R.id.btn_UpdateProfile);
        btn_UpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });

        showInfoUser();
    }

    private void showInfoUser(){
        mkLoaderId.setVisibility(View.VISIBLE);

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Authentication buse = (Authentication) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Authentication.class);


        buse.showProfile().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0) {
                            mkLoaderId.setVisibility(View.GONE);
                            //Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                            LoginResponse loginResponse = new Gson().fromJson(responseData, LoginResponse.class);

                            email.setText(loginResponse.getData().getEmail());
                            phone.setText(loginResponse.getData().getPhone());


//                            SharedHelpers.putKey(getActivity() , "TokenFirebase" , loginResponse.getData().getToken());
//                            SharedHelpers.putKey(getActivity() , "Image" , loginResponse.getData().getImage());
//                            SharedHelpers.putKey(getActivity() , "Name" , loginResponse.getData().getFirst_name()+" "+loginResponse.getData().getLast_name());
//                            SharedHelpers.putKey(getActivity() , "Email" , loginResponse.getData().getEmail());


                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mkLoaderId.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void changePassword(){
        mkLoaderId.setVisibility(View.VISIBLE);

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Authentication buse = (Authentication) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Authentication.class);


        buse.changePasswordProfile(password.getText().toString() ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0) {

                            Toast.makeText(getActivity(), generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                            mkLoaderId.setVisibility(View.GONE);
                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mkLoaderId.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void updateProfile(){
        boolean Cancel = false ;
        ChangePasswordT = false ;
        if (email.getText().toString().isEmpty())
        {
            email.setError(getActivity().getString(R.string.Empty));
            Cancel = true ;
        }
        else{
            if (email.getText().toString().contains("@") )
            {
                if (!email.getText().toString().contains("."))
                {
                    Cancel = true ;
                    email.setError(getActivity().getString(R.string.EmailNotValid));
                }

            }
            else
            {
                Cancel = true ;
                email.setError(getActivity().getString(R.string.EmailNotValid));
            }
        }
        if (phone.getText().toString().isEmpty())
        {
            phone.setError(getString(R.string.Empty));
            Cancel = true ;
        }
        if (!password.getText().toString().isEmpty())
        {
            if (confirmPassword.getText().toString().isEmpty())
            {
                Cancel = true ;
                confirmPassword.setError(getActivity().getString(R.string.Empty));
            }
            else
            {
                if (password.getText().toString().equals(confirmPassword.getText().toString()))
                {
                    ChangePasswordT = true ;
                }
                else
                {
                    Cancel = true ;
                    confirmPassword.setError(getActivity().getString(R.string.NotMatching));
                }
            }
        }


        if (!Cancel)
        {
            mkLoaderId.setVisibility(View.VISIBLE);

            final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
            Interceptor interceptor = new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
                }
            };

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.interceptors().add(interceptor);
            final Authentication buse = (Authentication) new Retrofit.Builder()
                    .baseUrl("http://taxiApi.codecaique.com/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(builder.build()).build()
                    .create(Authentication.class);


            buse.updateProfile(email.getText().toString() , phone.getText().toString()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.code() == 200)
                    {
                        try {
                            String responseData = response.body().string();

                            GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                            if (generalResponse.getError() == 0) {


                                if (ChangePasswordT)
                                {
                                    changePassword();
                                }
                                else
                                {
                                    Toast.makeText(getActivity(), generalResponse.getMessage_en() , Toast.LENGTH_SHORT).show();
                                    mkLoaderId.setVisibility(View.GONE);

                                }
                            }
                            else
                            {
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
