package com.example.taxinaclient.base;

import android.animation.ObjectAnimator;
import android.view.View;

public class OnlineTaxiAnimations {


    public static void animationTranslationY(View view, int value)
    {
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(view, "translationY", 0,value);
        scaleYAnimator.setDuration(800);
        scaleYAnimator.start();
    }

    public static void animationTranslationYAfterOtherAnimate(View view ,int value)
    {
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(view, "translationY", value);
        scaleYAnimator.setDuration(600);
        scaleYAnimator.setStartDelay(600);
        scaleYAnimator.start();
    }
}
