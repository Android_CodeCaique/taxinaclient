package com.example.taxinaclient.promotion;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Promotion;
import com.example.taxinaclient.networking.Ride;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.PromotionResponse;
import com.example.taxinaclient.pojo.responses.RidesResponse;
import com.example.taxinaclient.ride.ReidesAdapter;
import com.example.taxinaclient.ui.chat.ChatRecyclerViewAdabter;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPromotions extends Fragment {

    public MyPromotions() {
        // Required empty public constructor
    }
    private View view ;
    private RecyclerView rv_promotion;
    private MKLoader mkLoaderId ;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view= inflater.inflate(R.layout.fragment_my_promotions, container, false);

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mkLoaderId = view.findViewById(R.id.mkLoaderId);
        rv_promotion = view.findViewById(R.id.rv_promotion);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL,false);
        rv_promotion.setLayoutManager(linearLayoutManager);

        getPromotions();
    }

    private void getPromotions(){
        mkLoaderId.setVisibility(View.VISIBLE);

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Promotion buse = (Promotion) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Promotion.class);

        buse.ShowPromotions().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0) {
                             mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                            PromotionResponse promotionResponse = new Gson().fromJson(responseData, PromotionResponse.class);
                            PromotionRecyclerViewAdabter promotionRecyclerViewAdabter = new PromotionRecyclerViewAdabter(getActivity() , promotionResponse.getData());
                            rv_promotion.setAdapter(promotionRecyclerViewAdabter);
                            promotionRecyclerViewAdabter.notifyDataSetChanged();
                        }
                        else
                        {
                             mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                         mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                     mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                 mkLoaderId.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
