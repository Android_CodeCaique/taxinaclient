package com.example.taxinaclient.promotion;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.responses.PromotionResponse;
import com.google.gson.internal.$Gson$Preconditions;

import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;


public class PromotionRecyclerViewAdabter extends RecyclerView.Adapter<ViewHolderPromotion> {
    public PromotionRecyclerViewAdabter(Context context , List<PromotionResponse.data> Promotions ) {
        this.context = context;
        this.Promotions = Promotions ;
    }


    private View view;
    private Context context;
    private List<PromotionResponse.data> Promotions ;


    @NonNull
    @Override
    public ViewHolderPromotion onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.item_promotion,parent,false);
        ViewHolderPromotion viewHolderNotification = new ViewHolderPromotion(view);
        return viewHolderNotification;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderPromotion holder,final int position) {
        holder.notification_title.setText(Promotions.get(position).getTitle());
        holder.notification_details.setText(context.getString(R.string.Valid)+Promotions.get(position).getEnd_date());
        holder.use_now.setText(context.getString(R.string.use)+Promotions.get(position).getPromo_code());

        holder.use_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", Promotions.get(position).getPromo_code());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(context, context.getString(R.string.copied), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return Promotions.size();
    }
}

class ViewHolderPromotion extends RecyclerView.ViewHolder{

    TextView notification_title , notification_details , use_now;
    public ViewHolderPromotion(@NonNull View itemView) {
        super(itemView);
        notification_title = itemView.findViewById(R.id.notification_title);
        notification_details = itemView.findViewById(R.id.notification_details);
        use_now = itemView.findViewById(R.id.use_now);
    }
}