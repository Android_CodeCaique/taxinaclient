package com.example.taxinaclient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import com.example.taxinaclient.MainActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taxinaclient.notification11_1.Notification11_1;
import com.example.taxinaclient.notifications.dialogs.ForegroundDialog;
import com.example.taxinaclient.promotion.MyPromotions;
import com.example.taxinaclient.ride.MyRide;
import com.example.taxinaclient.ui.location.EnterDestination;
import com.example.taxinaclient.utils.LanguageHelper;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.core.view.Change;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static androidx.navigation.ui.NavigationUI.setupActionBarWithNavController;
import static com.google.android.gms.common.util.CollectionUtils.setOf;

public class Main2Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    AppBarConfiguration appBarConfiguration;
    static final float  END_SCALE= 0.7f;
    public static DrawerLayout drawerLayout;
    NavigationView navigationView;
    ConstraintLayout home_content;
    private CircleImageView user_image ;
    private TextView nameUser , phoneNumberUser ;
    private NavController navController ;

    //Fragment fragment2;

    FragmentManager manager;


    // For Forground
    private BroadcastReceiver mHandler = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ForegroundDialog dialog = new ForegroundDialog();
            dialog.show(getSupportFragmentManager() , "Foreground");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LocalBroadcastManager.getInstance(this).registerReceiver(mHandler, new IntentFilter("FCM_MESSAGE" ));

        setContentView(R.layout.activity_main2);

        manager =getSupportFragmentManager();
        initiation();
        navigation();
        navigationView.setNavigationItemSelectedListener(this);
    }
    private void navigationAnimation() {


        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                final float diffScaledOffSet=slideOffset*(1-END_SCALE);
                final float offsetScale=1-diffScaledOffSet;
                home_content.setScaleX(offsetScale);
                home_content.setScaleY(offsetScale);

                final float xOffset=drawerView.getWidth()*slideOffset;
                final float xOffsetDiff=home_content.getWidth()*diffScaledOffSet/2;
                final float xtranslation=xOffset-xOffsetDiff;
                home_content.setTranslationX(xtranslation);

            }

        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
               drawerLayout.closeDrawer(GravityCompat.START);
        }
        else  super.onBackPressed();
    }


    public final void navigation() {
        navigationView.bringToFront();
        navigationView.setCheckedItem(R.id.nav_Home);



//        DrawerLayout drawerLayout= findViewById(R.id.drawer_layout);
//        NavigationView navView = findViewById(R.id.nav_view);
//        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        appBarConfiguration = AppBarConfiguration(setOf(
//                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow), drawerLayout);
//        setupActionBarWithNavController(navController, appBarConfiguration);
//        navigationView.setupWithNavController(navController);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


        switch (menuItem.getItemId()) {
            case R.id.nav_Home:
                drawerLayout.closeDrawer(GravityCompat.START);
               navController.navigate(R.id.chooseKind);
                return true;
            case R.id.nav_Rides:
                drawerLayout.closeDrawer(GravityCompat.START);
                navController.navigate(R.id.myRide);
                return true;
            case R.id.nav_Promotion:
                drawerLayout.closeDrawer(GravityCompat.START);
                navController.navigate(R.id.myPromotions);
                return true;
            case R.id.nav_language:
                drawerLayout.closeDrawer(GravityCompat.START);
                ChangeLanguage changeLanguage = new ChangeLanguage();
                changeLanguage.show(getSupportFragmentManager() , "ChangeLanguage");
                return true;
            case R.id.nav_Notification:
                drawerLayout.closeDrawer(GravityCompat.START);
                navController.navigate(R.id.notification11_1);
                return true;
                case R.id.nav_Favorite:
                drawerLayout.closeDrawer(GravityCompat.START);
                navController.navigate(R.id.myFavorites);
                return true;
            case R.id.nav_Profile:
                drawerLayout.closeDrawer(GravityCompat.START);
                navController.navigate(R.id.updateProfile);
                return true;
            case R.id.nav_FamilyMembers:
                drawerLayout.closeDrawer(GravityCompat.START);
                navController.navigate(R.id.familyMembers);
                return true;
            case R.id.nav_Tracking:
                drawerLayout.closeDrawer(GravityCompat.START);
                navController.navigate(R.id.familyTracking);
                return true;

            case R.id.nav_Logout:
                SharedHelpers.putKey(this , "TokenFirebase" , "");
                SharedHelpers.putKey(this, "Image" , "");
                SharedHelpers.putKey(this, "Name" , "");
                SharedHelpers.putKey(this, "Email" , "");
                drawerLayout.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(Main2Activity.this , MainActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return false;
        }
    }

    private void initiation(){
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigation_view);
        home_content=findViewById(R.id.home_content);
        navController= Navigation.findNavController(this , R.id.nav_host_fragment);
        View header = navigationView.getHeaderView(0);
        user_image = header.findViewById(R.id.user_image);
        nameUser = header.findViewById(R.id.nameUser);
        phoneNumberUser = header.findViewById(R.id.phoneNumberUser);

        if (!SharedHelpers.getKey(this , "Email").isEmpty())
        {
          phoneNumberUser.setText(SharedHelpers.getKey(this , "Email"));
        }
        if (!SharedHelpers.getKey(this , "Name").isEmpty())
        {
           nameUser.setText(SharedHelpers.getKey(this , "Name"));
        }
        if (!SharedHelpers.getKey(this , "Image").isEmpty())
            Picasso.get().load(SharedHelpers.getKey(this , "Image")).error(R.drawable.logo).into(user_image);


        if (!SharedHelpers.getKey(this , "OPEN").equals("OPEN") &&  !SharedHelpers.getKey( this, "END" ).equals("1"))
        {
            SharedHelpers.putKey(this , "OPEN" , "OPEN");
            SharedHelpers.putKey(this , "END" , "0");
            setDataForNotification();
        }
    }


    private void setDataForNotification() {

        Log.d("HERE" , "HEREHOME");
        String id = getIntent().getExtras().getString("redirect_id");
        String status = getIntent().getExtras().getString("status");
        String type = getIntent().getExtras().getString("type");
        navigationView = findViewById(R.id.navigation_view);
        navController= Navigation.findNavController(this , R.id.nav_host_fragment);

        Log.d("DATA" , id+" : "+status+" : "+type);
        if(status != null)
        {
            Log.d("ERROE" , "Notification is not null"+status);
            if (status.equals("1"))
            {
                // Open Graph  ....

                SharedHelpers.putKey(this , "OPENGRAPH" , "1");
                SharedHelpers.putKey(this , "TripID" , id);
                navController.navigate(R.id.tracking);
            }
            else if (status.equals("2"))
            {
                // Details Trip
                SharedHelpers.putKey(this , "IdTripShow" , id);
                SharedHelpers.putKey(this , "TypeTripShow" , type);
                navController.navigate(R.id.thank_You);
            }
            else if (status.equals("3"))
            {
                // Details Trip
                SharedHelpers.putKey(this , "IdTripShow" , id);
                SharedHelpers.putKey(this , "TypeTripShow" , type);
                navController.navigate(R.id.thank_You);
            }
            else if (status.equals("4") || status.equals("5"))
            {
                // Details Trip
                SharedHelpers.putKey(this , "IDTrip" , id);
                navController.navigate(R.id.inMicroBus);
            }
        }

        else
        {
            Log.d("ERROE" , "Notification is null");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedHelpers.putKey(this , "OPEN" , "");

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandler);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedHelpers.putKey(this , "OPEN" , "OPEN");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        SharedHelpers.putKey(this , "OPEN" , "OPEN");
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedHelpers.putKey(this , "OPEN" , "");
    }
}
