package com.example.taxinaclient.notifications;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.widget.Toast;



public class MyJobService extends JobService {
    @Override
    public boolean onStartJob(JobParameters job) {
        startService(new Intent(this, MyFirebaseMessagingService.class));
        return false; // Answers the question: "Is there still work going on?"
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        Toast.makeText(getApplicationContext(),"stop",Toast.LENGTH_LONG).show();
        return false; // Answers the question: "Should this job be retried?"
    }
}