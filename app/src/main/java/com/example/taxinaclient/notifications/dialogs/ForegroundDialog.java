package com.example.taxinaclient.notifications.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.utils.SharedHelpers;

public class ForegroundDialog extends DialogFragment {

    private View view ;
    private TextView Title , Content;
    private Button ShowPost ;
    private NavController navController ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.foreground_notification_dialog, container, false);

        init();
        return view;
    }

    private void init(){
        Title = view.findViewById(R.id.Title);
        Content = view.findViewById(R.id.Content);
        ShowPost = view.findViewById(R.id.ShowPost);

        Title.setText(SharedHelpers.getKey(getActivity() , "Title"));
        Content.setText(SharedHelpers.getKey(getActivity() , "Body"));


        final String status = SharedHelpers.getKey(getActivity(),"Status"  );
        final String id = SharedHelpers.getKey(getActivity(),"Redirect"  );
        final String type = SharedHelpers.getKey(getActivity(),"Type" );

        ShowPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (status != null)
                {
                    navController= Navigation.findNavController(getActivity() , R.id.nav_host_fragment);
                    Log.d("error" , " ya welcome b success ");
                    if (status.equals("1"))
                    {
                        SharedHelpers.putKey(getActivity() , "OPENGRAPH" , "1");
                        SharedHelpers.putKey(getActivity() , "TripID" , id);
                        SharedHelpers.putKey(getActivity() , "AllowLatLngBound" , "false");

                        navController.navigate(R.id.tracking);

                    }
                    else if (status.equals("2"))
                    {

                        SharedHelpers.putKey(getActivity() , "IdTripShow" , id);
                        SharedHelpers.putKey(getActivity() , "TypeTripShow" , type);

                        navController.navigate(R.id.thank_You);
                    }
                    else if (status.equals("3"))
                    {

                        SharedHelpers.putKey(getActivity() , "IdTripShow" , id);
                        SharedHelpers.putKey(getActivity() , "TypeTripShow" , type);

                        navController.navigate(R.id.thank_You);
                    }

                }
                else
                {
                    Log.d("error" , " ya welcome b error ");
                }

            }
        });
    }
}
