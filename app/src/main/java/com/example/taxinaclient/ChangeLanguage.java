package com.example.taxinaclient;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;


import com.example.taxinaclient.utils.LanguageHelper;
import com.example.taxinaclient.utils.SharedHelpers;

public class ChangeLanguage extends DialogFragment {

    private View view ;

    private RadioButton arabic , english ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.change_language_dialog, container, false);
        getDialog().getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.back_edit_chat));
        Window window = getDialog().getWindow();
        if(window != null)
        {
            WindowManager.LayoutParams params = window.getAttributes();
            params.width = 450;
            params.height = 300;
            window.setAttributes(params);
        }
        init();
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        if(window == null) return;
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = 450;
        params.height = 300;
        window.setAttributes(params);
    }
    private void init(){
        english = view.findViewById(R.id.english);
        arabic = view.findViewById(R.id.arabic);






        if (!SharedHelpers.getKey(getActivity() , "LANG").isEmpty())
        {
            if (SharedHelpers.getKey(getActivity() , "LANG").equals("en"))
            {
                english.setChecked(true);
                english.setTextDirection(View.TEXT_DIRECTION_LTR);
                arabic.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
            else if (SharedHelpers.getKey(getActivity() , "LANG").equals("ar"))
            {
                arabic.setChecked(true);
                english.setTextDirection(View.TEXT_DIRECTION_RTL);
                arabic.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
        }
        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (english.isChecked())
                {

                    LanguageHelper.ChangeLang(getResources() , "en");
                    SharedHelpers.putKey(getActivity() , "LANG" , "en");
                    SharedHelpers.putKey(getActivity() , "END" , "1");
                    Intent intent = new Intent(getActivity() , Main2Activity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arabic.isChecked())
                {
                    LanguageHelper.ChangeLang(getResources() , "ar");
                    SharedHelpers.putKey(getActivity() , "LANG" , "ar");
                    SharedHelpers.putKey(getActivity() , "END" , "1");
                    Intent intent = new Intent(getActivity() , Main2Activity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });
    }
}
