package com.example.taxinaclient.ui.Auth;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.utils.SharedHelpers;

/**
 * A simple {@link Fragment} subclass.
 */
public class GetStarted extends Fragment {

    public GetStarted() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_get_started, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController= Navigation.findNavController(view);
        SharedHelpers sharedHelper = new SharedHelpers();
        sharedHelper.putKey(getActivity() , "Intro" , "1");

        Button btn_Start= view.findViewById(R.id.btn_Start);
        btn_Start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity() , Main2Activity.class);
                startActivity(intent);
                getActivity().finish();

            }
        });

    }
}
