package com.example.taxinaclient.ui.location.dialogs;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.utils.SharedHelpers;

import java.util.Calendar;

public class DatePickerDialog extends DialogFragment {
    private View view;
    private CheckBox OrderNow, OrderSchedule;
    private TextView titleTypeOne, typeOne, titleTypeTwo, typeTwo, time, scheduleDate, scheduleTime , title;

    private View con1, con2;

    private Calendar calendar;
    private int yearT, monthT, dayT, minuteT, hourT;

    private Button Scheduling ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bottom_sheet_pick_up_time, container, false);
        init();
        return view;
    }

    private void init() {

        calendar = Calendar.getInstance();
        yearT = calendar.get(Calendar.YEAR);
        monthT = calendar.get(Calendar.MONTH);
        dayT = calendar.get(Calendar.DAY_OF_MONTH);
        hourT = calendar.get(Calendar.HOUR_OF_DAY);
        minuteT = calendar.get(Calendar.MINUTE);


        OrderNow = view.findViewById(R.id.OrderNow);
        OrderSchedule = view.findViewById(R.id.OrderSchedule);

        titleTypeOne = view.findViewById(R.id.titleTypeOne);
        titleTypeTwo = view.findViewById(R.id.titleTypeTwo);

        typeOne = view.findViewById(R.id.typeOne);
        typeTwo = view.findViewById(R.id.typeTwo);

        con1 = view.findViewById(R.id.con1);
        con2 = view.findViewById(R.id.con2);

        time = view.findViewById(R.id.time);
        title = view.findViewById(R.id.title);

        scheduleDate = view.findViewById(R.id.scheduleDate);
        scheduleTime = view.findViewById(R.id.scheduleTime);

        OrderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OrderNow.isChecked()) {
                    OrderSchedule.setChecked(false);

                    con1.setBackground(getActivity().getDrawable(R.drawable.back_edit_chat));
                    con2.setBackground(getActivity().getDrawable(R.drawable.back_10));

                    titleTypeOne.setTextColor(getActivity().getColor(R.color.yellowdark));
                    typeOne.setTextColor(getActivity().getColor(R.color.yellowdark));

                    titleTypeTwo.setTextColor(getActivity().getColor(R.color.black));
                    typeTwo.setTextColor(getActivity().getColor(R.color.hint));


                    scheduleDate.setVisibility(View.GONE);
                    scheduleTime.setVisibility(View.GONE);

                    time.setVisibility(View.VISIBLE);
                    title.setVisibility(View.VISIBLE);


                    time.setText(hourT+"-"+minuteT);
                } else {
                    OrderNow.setChecked(false);

                    con1.setBackground(getActivity().getDrawable(R.drawable.back_10));

                    titleTypeOne.setTextColor(getActivity().getColor(R.color.black));
                    typeOne.setTextColor(getActivity().getColor(R.color.hint));

                    time.setVisibility(View.GONE);
                    title.setVisibility(View.GONE);
                }
            }
        });

        OrderSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OrderSchedule.isChecked()) {
                    OrderNow.setChecked(false);

                    con1.setBackground(getActivity().getDrawable(R.drawable.back_10));
                    con2.setBackground(getActivity().getDrawable(R.drawable.back_edit_chat));


                    titleTypeTwo.setTextColor(getActivity().getColor(R.color.yellowdark));
                    typeTwo.setTextColor(getActivity().getColor(R.color.yellowdark));

                    titleTypeOne.setTextColor(getActivity().getColor(R.color.black));
                    typeOne.setTextColor(getActivity().getColor(R.color.hint));

                    scheduleDate.setVisibility(View.VISIBLE);
                    scheduleTime.setVisibility(View.VISIBLE);


                   time.setVisibility(View.GONE);
                   title.setVisibility(View.GONE);
                } else {
                    OrderNow.setChecked(false);

                    con2.setBackground(getActivity().getDrawable(R.drawable.back_10));

                    titleTypeTwo.setTextColor(getActivity().getColor(R.color.black));
                    typeTwo.setTextColor(getActivity().getColor(R.color.hint));

                    time.setVisibility(View.GONE);
                    title.setVisibility(View.GONE);
                }
            }
        });

        scheduleDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.app.DatePickerDialog datePickerDialog = new android.app.DatePickerDialog(getActivity(), new android.app.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        scheduleDate.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
                    }
                }, yearT, monthT, dayT);
                datePickerDialog.show();
            }
        });
        scheduleTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.app.TimePickerDialog datePickerDialog = new android.app.TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        scheduleTime.setText(hourOfDay + "-" + minute );
                    }
                }, hourT, minuteT, false);
                datePickerDialog.show();
            }
        });


        Scheduling = view.findViewById(R.id.Scheduling);
        Scheduling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean Cancel = false ;
                if (!OrderNow.isChecked() && !OrderSchedule.isChecked())
                {
                    Cancel = true ;
                    titleTypeOne.setError(getActivity().getString(R.string.Empty));
                    titleTypeTwo.setError(getActivity().getString(R.string.Empty));
                }
                else
                {
                    if (OrderNow.isChecked())
                    {
                       // Toast.makeText(getActivity(), "TYPE ONE", Toast.LENGTH_SHORT).show();
                        if (time.getText().toString().isEmpty())
                        {
                            Cancel = true ;
                            titleTypeOne.setError(getActivity().getString(R.string.Empty));
                        }
                    }
                    else if(OrderSchedule.isChecked())
                    {
                        if (scheduleTime.getText().toString().isEmpty())
                        {
                           // Toast.makeText(getActivity(), "TYPE TWO 1", Toast.LENGTH_SHORT).show();
                            scheduleTime.setError(getActivity().getString(R.string.Empty));
                            Cancel = true ;
                        }
                        else if (scheduleDate.getText().toString().isEmpty())
                        {
                          //  Toast.makeText(getActivity(), "TYPE TWO 2", Toast.LENGTH_SHORT).show();
                            scheduleDate.setError(getActivity().getString(R.string.Empty));
                            Cancel = true ;
                        }

                    }
                }


                if (!Cancel)
                {
                    String Type = "" ;
                    String Time = "" ;
                    String Date = "" ;
                    if (OrderNow.isChecked())
                    {
                        Type = "0";
                    }
                    else if (OrderSchedule.isChecked())
                    {
                        Type = "1";
                        Time = scheduleTime.getText().toString() ;
                        Date = scheduleDate.getText().toString() ;

                        SharedHelpers.putKey(getActivity() , "TimeOrder" , Time);
                        SharedHelpers.putKey(getActivity() , "DateOrder" , Date);
                    }
                    SharedHelpers.putKey(getActivity() , "TypeTimeOrder" , Type);
                }
                dismiss();
            }
        });
    }
}
