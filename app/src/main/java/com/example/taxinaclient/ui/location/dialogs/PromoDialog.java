package com.example.taxinaclient.ui.location.dialogs;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.pojo.responses.CarAvilableResponse;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.PromoCodeReponse;
import com.example.taxinaclient.ui.location.AvailableCarsAdapter;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PromoDialog extends DialogFragment {

    private View view ;
    private EditText et_promo_code ;
    private Button ApplyPromo;
    private ProgressDialog progressDialog ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.bottom_sheet_apply_promo_code, container, false);
        init();
        return view ;
    }

    private void init(){
        progressDialog = new ProgressDialog(getContext());
        et_promo_code = view.findViewById(R.id.et_promo_code);
        ApplyPromo = view.findViewById(R.id.ApplyPromo);
        ApplyPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_promo_code.getText().toString().isEmpty())
                {
                    et_promo_code.setError(getActivity().getString(R.string.Empty));
                }
                else
                {
                    CheckPromo();
                }
            }
        });
    }

    private void CheckPromo(){
        this.progressDialog.setTitle("Promo code checking process is loading");
        this.progressDialog.setMessage("Please wait ..... ");
        this.progressDialog.show();
        this.progressDialog.setCanceledOnTouchOutside(false);
        this.progressDialog.setCancelable(false);
        progressDialog.show();
        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Car buse = (Car) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Car.class);

        buse.checkPromoCode( et_promo_code.getText().toString())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    progressDialog.dismiss();
                                    final PromoCodeReponse promoCodeReponse = new Gson().fromJson(responseData, PromoCodeReponse.class);
                                    Toast.makeText(getActivity(), promoCodeReponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    et_promo_code.setText("");
                                    SharedHelpers.putKey(getActivity() , "PromoTripTaxi" , promoCodeReponse.getData());
                                    dismiss();
                                }
                                else
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }
}
