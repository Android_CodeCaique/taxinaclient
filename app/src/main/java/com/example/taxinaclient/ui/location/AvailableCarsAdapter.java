package com.example.taxinaclient.ui.location;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.model.Package;
import com.example.taxinaclient.pojo.responses.CarAvilableResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

public class AvailableCarsAdapter extends RecyclerView.Adapter<AvailableCarsAdapter.AvailableCarsViewHolder> {
    private List<Package>Cars ;
    private Context context ;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int i);
    }

    public AvailableCarsAdapter(List<Package> cars, Context context) {
        Cars = cars;
        this.context = context;
    }

    @NonNull
    @Override
    public AvailableCarsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AvailableCarsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book_car, parent, false) , mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull AvailableCarsViewHolder holder, int position) {
        if (Cars.get(position).isCheck())
        {
            holder.itemView.setBackground(context.getDrawable(R.drawable.back_btn_yellow));
            holder.cost.setTextColor(context.getColor(R.color.white));
        }
        else {
            holder.itemView.setBackground(context.getDrawable(R.drawable.back_btn_hint));
            holder.cost.setTextColor(context.getColor(R.color.hint));
        }
        Picasso.get().load(Cars.get(position).getImage()).error(R.drawable.book_car).into(holder.car_photo);
        holder.car_name.setText(Cars.get(position).getName());
        holder.cost.setText(new DecimalFormat("##.##").format(Double.parseDouble(Cars.get(position).getPrice()+"")));
    }

    @Override
    public int getItemCount() {
        return Cars.size();
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }


    public static class AvailableCarsViewHolder extends RecyclerView.ViewHolder {
        ImageView car_photo ;
        TextView car_name ,cost;
        AvailableCarsViewHolder(View itemView, final OnItemClickListener listener ) {
            super(itemView);
            car_photo = itemView.findViewById(R.id.car_photo);
            car_name = itemView.findViewById(R.id.car_name);
            cost = itemView.findViewById(R.id.cost);


            itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != -1) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }
}
