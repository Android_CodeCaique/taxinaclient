package com.example.taxinaclient.ui.Auth;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.taxinaclient.R;
import com.example.taxinaclient.databinding.FragmentPhoneOTPBinding;
import com.example.taxinaclient.networking.Authentication;
import com.example.taxinaclient.pojo.responses.BaseResponse;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.LoginSocialResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.hbb20.CountryCodePicker;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PhoneOTP extends Fragment {

    FragmentPhoneOTPBinding binding;
    SharedHelpers sharedHelpers;
    String countryCodeSelected = "218";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentPhoneOTPBinding.inflate(inflater);
        sharedHelpers = new SharedHelpers();

        binding.btnSendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPhone(requireContext(),binding.phoneID.getText().toString());
            }
        });

        binding.ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
//                Toast.makeText(requireContext(),binding.ccp.getSelectedCountryCode(),Toast.LENGTH_LONG).show();
                countryCodeSelected = binding.ccp.getSelectedCountryCode();
            }
        });

        return binding.getRoot();
    }


    private void checkPhone(final Context context ,final  String phone ){
        binding.mkLoaderId.setVisibility(View.VISIBLE);

        ((Authentication) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Authentication.class))
                .checkPhone(phone)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    binding.mkLoaderId.setVisibility(View.INVISIBLE);
                                    BaseResponse loginResponse = new Gson().fromJson(responseData, BaseResponse.class);
                                    Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                                if (generalResponse.getError() == 1) {
                                    binding.mkLoaderId.setVisibility(View.INVISIBLE);
                                    BaseResponse loginResponse = new Gson().fromJson(responseData, BaseResponse.class);
//                                    getActivationCode(context,phone);

//                                    SharedHelpers.putKey(requireContext() , "ActiveCode"  , verificationId);
                                    SharedHelpers.putKey(requireContext() , "VerificationPhone"  , "+"+countryCodeSelected + phone);
                                    Navigation.findNavController(requireView()).navigate(R.id.action_phoneOTP_to_verifyCode);
                                }

                                else
                                {
                                    binding.mkLoaderId.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                binding.mkLoaderId.setVisibility(View.INVISIBLE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            binding.mkLoaderId.setVisibility(View.INVISIBLE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        binding.mkLoaderId.setVisibility(View.INVISIBLE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    public void getActivationCode(final Context context , final String phone)
    {
        binding.mkLoaderId.setVisibility(View.VISIBLE);

        PhoneAuthProvider.getInstance().verifyPhoneNumber("+"+countryCodeSelected + phone,
                60,
                TimeUnit.SECONDS,
                requireActivity(),
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                binding.mkLoaderId.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(requireContext(), "Exception "+e.getMessage(), Toast.LENGTH_SHORT).show();
                binding.mkLoaderId.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                binding.mkLoaderId.setVisibility(View.INVISIBLE);
//                Toast.makeText(requireContext(), "Code sent", Toast.LENGTH_SHORT).show();


            }
        });
    }



}