package com.example.taxinaclient.ui.Auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Authentication;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.LoginResponse;
import com.example.taxinaclient.pojo.responses.LoginSocialResponse;
import com.example.taxinaclient.pojo.responses.RegisterResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.example.taxinaclient.utils.Validation;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.google.android.gms.common.util.CollectionUtils.listOf;

/**
 * A simple {@link Fragment} subclass.
 */
public class Login extends Fragment {
    private EditText Password , Email ;
    private Button Login ;
    private View v ;
    NavController navController ;
    private MKLoader mkLoaderId;
    private TextView tv_forget ;
    private ImageView fbLogin;
    CallbackManager callbackManager;
    public Login() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_login, container, false);
        return v ;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

        Email = v.findViewById(R.id.et_email);
        Password = v.findViewById(R.id.et_password);
        Login = v.findViewById(R.id.btn_Login);
        fbLogin = v.findViewById(R.id.fbLogin);
        fbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginFacebook();
            }
        });
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginClick();
            }
        });
        TextView tv_signup= view.findViewById(R.id.tv_signup);
        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_login_to_phoneOTP);

            }
        });
        mkLoaderId =view.findViewById(R.id.mkLoaderId);

        tv_forget = view.findViewById(R.id.tv_forget);
        tv_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.forgetPasswordFragment);
            }
        });
    }

    private void loginFacebook() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email","public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                try {
//                                    Toast.makeText(getActivity(),object.getString("email")
//                                            +"\nFName : "+object.getString("first_name"),Toast.LENGTH_LONG).show();
                                    mkLoaderId.setVisibility(View.VISIBLE);

                                    loginSocailApi(object.getString("email"));
                                } catch (JSONException e) {
                                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name , last_name , email , id");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void loginClick(){
        boolean Cancel = false ;


        if (Email.getText().toString().isEmpty())
        {
            Cancel = true ;
            Email.setError(getString(R.string.Empty));
        }

        else
        {
            if (!Email.getText().toString().contains("@") || !Email.getText().toString().contains("."))
            {
                Cancel = true ;
                Email.setError(getString(R.string.EmailNotValid));
            }
        }

        if (Password.getText().toString().isEmpty())
        {
            Cancel = true ;
            Password.setError(getString(R.string.Empty));
        }else
        {
            if (!(Password.getText().toString().length() > 5))
            {
                Cancel = true ;
                Password.setError(getString(R.string.PasswordNotValid));
            }
        }


        Log.d("Cancel" , Cancel+"");
        if (!Cancel) {
            Log.d("Cancel" , Cancel+"HERE");


            if (Validation.isOnline(getActivity()))
            {
                mkLoaderId.setVisibility(View.VISIBLE);
                loginFunctionApi(Email.getText().toString() , Password.getText().toString() );
            }
            else
                Toast.makeText(getActivity(), "Please check your connection with internet", Toast.LENGTH_SHORT).show();

        }
    }
    private void loginFunction(final  String EmailT , final  String PasswordT){
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        Log.d("Cancel" , "in real function HERE");
        firebaseAuth.signInWithEmailAndPassword(EmailT, PasswordT)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                        {
                            String ID = firebaseAuth.getCurrentUser().getUid();


                            Email.setText("");
                            Password.setText("");

                            Toast.makeText(getActivity(), "Done , Login process is Successful", Toast.LENGTH_SHORT).show();

                            SharedHelpers sharedHelper = new SharedHelpers();
                            sharedHelper.putKey(getActivity() , "TokenFirebase" , ID);

                            mkLoaderId.setVisibility(View.GONE);
                            if ( sharedHelper.getKey(getActivity() , "Intro" ).equals("1"))
                            {
                                Intent intent = new Intent(getActivity() , Main2Activity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                            else
                            {
                                navController.navigate(R.id.action_login_to_intro1);
                            }


                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), " Login process is Failure", Toast.LENGTH_SHORT).show();
                            Log.d("Failure" , "Failure");
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), " Login process is Failure because "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("Failure" , "Failure"+e.getMessage());
                    }
                });

    }

    private void loginFunctionApi(final  String EmailT , final  String PasswordT){
        ((Authentication) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Authentication.class))
                .login(EmailT , PasswordT , FirebaseInstanceId.getInstance().getToken())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    mkLoaderId.setVisibility(View.GONE);
                                    LoginResponse loginResponse = new Gson().fromJson(responseData, LoginResponse.class);
                                    Toast.makeText(getActivity(), loginResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                    SharedHelpers.putKey(getActivity() , "TokenFirebaseCountry" , loginResponse.getData().getToken());
                                    SharedHelpers.putKey(getActivity() , "OPEN" , "OPEN");
                                    navController.navigate(R.id.action_login_to_userCountryFragment);

//                                    Log.d("Firebase " , loginResponse.getData().getToken());
//                                    SharedHelpers.putKey(getActivity() , "IDUser" , loginResponse.getData().getId()+"");
//                                    SharedHelpers.putKey(getActivity() , "Image" , loginResponse.getData().getImage());
//                                    SharedHelpers.putKey(getActivity() , "Name" , loginResponse.getData().getFirst_name()+" "+loginResponse.getData().getLast_name());
//                                    SharedHelpers.putKey(getActivity() , "Email" , loginResponse.getData().getEmail());
//
//                                    Email.setText("");
//                                    Password.setText("");
//
//                                    if ( SharedHelpers.getKey(getActivity() , "Intro" ).equals("1"))
//                                    {
//                                        Intent intent = new Intent(getActivity() , Main2Activity.class);
//                                        startActivity(intent);
//                                        getActivity().finish();
//                                    }
//                                    else
//                                    {
//                                        navController.navigate(R.id.action_login_to_intro1);
//                                    }
                                }
                                else
                                {
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "عفوا بيانات الدخول خاطئه من فضلك حاول مره اخري", Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void loginSocailApi(final  String EmailT ){
        ((Authentication) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Authentication.class))
                .loginSocial(EmailT)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    mkLoaderId.setVisibility(View.GONE);
                                    LoginSocialResponse loginResponse = new Gson().fromJson(responseData, LoginSocialResponse.class);
                                    Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    SharedHelpers.putKey(getActivity() , "TokenFirebaseCountry" , loginResponse.getData().getToken());
                                    SharedHelpers.putKey(getActivity() , "OPEN" , "OPEN");
                                    navController.navigate(R.id.action_login_to_userCountryFragment);

//                                    Log.d("Firebase " , loginResponse.getData().getToken());
//                                    SharedHelpers.putKey(getActivity() , "TokenFirebase" , loginResponse.getData().getToken());
//                                    SharedHelpers.putKey(getActivity() , "IDUser" , loginResponse.getData().getId()+"");
//                                    SharedHelpers.putKey(getActivity() , "Image" , loginResponse.getData().getImage());
//                                    SharedHelpers.putKey(getActivity() , "Name" , loginResponse.getData().getFirst_name()+" "+loginResponse.getData().getLast_name());
//                                    SharedHelpers.putKey(getActivity() , "Email" , loginResponse.getData().getEmail());
//                                    SharedHelpers.putKey(getActivity() , "OPEN" , "OPEN");
//
//                                    Email.setText("");
//                                    Password.setText("");
//
//                                    if ( SharedHelpers.getKey(getActivity() , "Intro" ).equals("1"))
//                                    {
//                                        Intent intent = new Intent(getActivity() , Main2Activity.class);
//                                        startActivity(intent);
//                                        getActivity().finish();
//                                    }
//                                    else
//                                    {
//                                        navController.navigate(R.id.action_login_to_intro1);
//                                    }
                                }
                                else
                                {
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
