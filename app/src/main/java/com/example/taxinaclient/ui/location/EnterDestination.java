package com.example.taxinaclient.ui.location;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.SharedElementCallback;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.MapsActivity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.ui.location.dialogs.FavoriteFragmentDialog;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.EncodedPolyline;
import com.logicbeanzs.carrouteanimation.CarMoveAnim;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import com.google.android.libraries.places.api.Places;
import com.tuyenmonkey.mkloader.MKLoader;

import de.hdodenhof.circleimageview.CircleImageView;
import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnterDestination extends Fragment {
    private ImageView go;
    private NavController navController;
    private CircleImageView userImage;
    private double latDes, lngDes, latFrom, lngFrom;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private FusedLocationProviderClient fusedLocationClient;

    private View view;
    private MapView mMapView;
    private GoogleMap googleMap;

    TextView start_loc,end_loc;

    Marker marker ;

    private AutocompleteSupportFragment autocompleteFragment , autocompleteDes_fragment;
    private int AUTOCOMPLETE_REQUEST_CODE = 1;
    private String Start , End;

    private MKLoader mkLoaderId ;
    private ImageView fromMap , favoriteFrom , favoriteTo;
    private double startLat , startLng ;
    private String LocationText ;

    private List<MarkerOptions>markerOptionsList;
    private List<String>Locations;
    private MarkerOptions startMarker ,endMarker ;

    String TAG = "EnterDestination";

    public EnterDestination() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_enter_destination, container, false);

        favoriteFrom = view.findViewById(R.id.favoriteFromId);
        favoriteTo = view.findViewById(R.id.favoriteToId);

        start_loc = view.findViewById(R.id.start_loc_tv);
        end_loc = view.findViewById(R.id.end_loc_tv);

        favoriteFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavoriteFragmentDialog favoriteFragmentDialog  = new FavoriteFragmentDialog(1);
                favoriteFragmentDialog.show(requireActivity().getSupportFragmentManager(),"");
                favoriteFragmentDialog.locationNameStart.observe(getViewLifecycleOwner(), new Observer<String>() {
                    @Override
                    public void onChanged(String s) {
                        if(!s.isEmpty()) {
                            Start = s;
                            autocompleteFragment.setText(s);
                        }
                    }
                });
                favoriteFragmentDialog.startLat.observe(getViewLifecycleOwner(), new Observer<String>() {
                    @Override
                    public void onChanged(String s) {
                        if(!s.isEmpty()) {
                            latFrom = Double.parseDouble(s);

                        }
                    }
                });
                favoriteFragmentDialog.startLon.observe(getViewLifecycleOwner(), new Observer<String>() {
                    @Override
                    public void onChanged(String s) {
                        if(!s.isEmpty()) {
                            lngFrom = Double.parseDouble(s);

                        }
                    }
                });

//                favoriteFragmentDialog.onDismiss(new DialogInterface() {
//                    @Override
//                    public void cancel() {
//
//                    }
//
//                    @Override
//                    public void dismiss() {
//                        latFrom = Double.parseDouble(SharedHelpers.getKey(requireContext(),"StartLat1").toString());
//                        lngFrom = Double.parseDouble(SharedHelpers.getKey(requireContext(),"EndLat1").toString());
//                        Start = SharedHelpers.getKey(requireContext(),"LocationName1").toString();
//                    }
//                });
            }
        });
        favoriteTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavoriteFragmentDialog favoriteFragmentDialog  = new FavoriteFragmentDialog(2);
                favoriteFragmentDialog.show(requireActivity().getSupportFragmentManager(),"");
//                favoriteFragmentDialog.onDismiss(new DialogInterface() {
//                    @Override
//                    public void cancel() {
//
//                    }
//
//                    @Override
//                    public void dismiss() {
//                        latDes = Double.parseDouble(SharedHelpers.getKey(requireContext(),"StartLat2").toString());
//                        lngDes = Double.parseDouble(SharedHelpers.getKey(requireContext(),"EndLat2").toString());
//                        End = SharedHelpers.getKey(requireContext(),"LocationName2").toString();
//                    }
//                });
                favoriteFragmentDialog.locationNameEnd.observe(getViewLifecycleOwner(), new Observer<String>() {
                    @Override
                    public void onChanged(String s) {
                        if(!s.isEmpty()) {
                            End = s;
                            autocompleteDes_fragment.setText(s);
                        }
                    }
                });
                favoriteFragmentDialog.endLat.observe(getViewLifecycleOwner(), new Observer<String>() {
                    @Override
                    public void onChanged(String s) {
                        if(!s.isEmpty()) {
                            latDes = Double.parseDouble(s);
                        }
                    }
                });
                favoriteFragmentDialog.endLon.observe(getViewLifecycleOwner(), new Observer<String>() {
                    @Override
                    public void onChanged(String s) {
                        if(!s.isEmpty()) {
                            lngDes = Double.parseDouble(s);

                        }
                    }
                });
            }
        });




        mkLoaderId = view.findViewById(R.id.mkLoaderId);
        mkLoaderId.setVisibility(View.GONE);
        latDes = 0;
        latFrom = 0;
        lngDes = 0;
        lngFrom = 0;
        startLat = 0;
        startLng = 0;

        markerOptionsList = new ArrayList<>();

        Locations = new ArrayList<>();

        startMarker = new MarkerOptions();
        endMarker = new MarkerOptions();


        markerOptionsList.add(0, null);
        markerOptionsList.add(1, null);

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                getDrivers();
                if (checkPermission())
                {
                    googleMap.setMyLocationEnabled(true);
                }
                else
                {
                    requestPermission() ;
                }

                final Geocoder geocoder = new Geocoder(getContext() , new Locale("ar"));

                final List<Address>[] addresses = new List[]{new ArrayList<>()};


                start_loc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Toast.makeText(getContext(), R.string.set_startloc, Toast.LENGTH_SHORT).show();

                        googleMap.clear();

                        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                Double lat = latLng.latitude;
                                Double lng = latLng.longitude;
                                MarkerOptions markerOptions = new MarkerOptions();
                                googleMap.clear();
                                // Setting the position for the marker
                                markerOptions.position(latLng);

                                try {
                                    addresses[0] = geocoder.getFromLocation(lat,lng,1);

                                    autocompleteFragment.setText(addresses[0].get(0).getAddressLine(0));

                                    Start = addresses[0].get(0).getAddressLine(0);
                                    latFrom = addresses[0].get(0).getLatitude();
                                    lngFrom = addresses[0].get(0).getLongitude();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                Log.e(TAG, "onMapClick: " + lat + ">==<" + lng + ">==<" + latLng   );



                                googleMap.addMarker(markerOptions);
                            }
                        });
                    }
                });

                end_loc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getContext(), R.string.set_endloc, Toast.LENGTH_SHORT).show();

                        googleMap.clear();

                        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                Double lat = latLng.latitude;
                                Double lng = latLng.longitude;
                                MarkerOptions markerOptions = new MarkerOptions();
                                googleMap.clear();
                                // Setting the position for the marker
                                markerOptions.position(latLng);

                                try {
                                    addresses[0] = geocoder.getFromLocation(lat,lng,1);

                                    autocompleteDes_fragment.setText(addresses[0].get(0).getAddressLine(0));

                                    End = addresses[0].get(0).getAddressLine(0);

                                    latDes = addresses[0].get(0).getLatitude();
                                    lngDes = addresses[0].get(0).getLongitude();


                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                Log.e(TAG, "onMapClick: " + lat + ">==<" + lng + ">==<" + latLng   );



                                googleMap.addMarker(markerOptions);
                            }
                        });
                    }
                });


            }
        });


        return view;

    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);

        userImage = view.findViewById(R.id.userImage);
        if (!SharedHelpers.getKey(getActivity(), "Image").isEmpty())
            Picasso.get().load(SharedHelpers.getKey(getActivity(), "Image")).error(R.drawable.logo).into(userImage);
        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2Activity.drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        Start = "" ;
        End = "" ;
        go = view.findViewById(R.id.go);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                directionClick();

            }
        });
        fromMap = view.findViewById(R.id.fromMap);
        fromMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (googleMap != null)
                {
                    latFrom = startLat ;
                    lngFrom = startLng ;
                    LatLng latLng = new LatLng(startLat , startLng);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
                    googleMap.animateCamera(cameraUpdate);
                    startMarker = new MarkerOptions();
                    // Setting the position for the marker
                    startMarker.position(latLng);
                    // Placing a marker on the touched position
                    autocompleteFragment.setText(LocationText);
                    Start = LocationText ;

                    prepareLocationsInMap(0);

                }

            }
        });
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }

        // Check if has GPS by using Google play service
        buildLocationSettingsRequest();

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            double lat = location.getLatitude();
                            double lng = location.getLongitude();

                            startLat = lat ;
                            startLng = lng ;

                            LatLng latLng = new LatLng(lat, lng);
                            // Setting the position for the marker
                            // Clears the previously touched position
                            if (googleMap != null) {
                                googleMap.clear();
                                // Animating to the touched position
                                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
                                googleMap.animateCamera(cameraUpdate);



                                try {
                                    Geocoder geocoder;
                                    List<Address> addresses;
                                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                                    addresses = geocoder.getFromLocation(startLat, startLng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                                    LocationText = addresses.get(0).getAddressLine(0);// If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()


                                } catch (IOException e) {
                                    e.printStackTrace();

                                }
                            }

                        } else {
                            Log.d("HERe", "ERROR to get the location");
                        }
                    }
                });


        Places.initialize(getActivity().getApplicationContext(), "AIzaSyB4ski2q_cAYGl2LA4aHyjU3LALRspXZvM");
        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(com.google.android.gms.location.places.Place.TYPE_COUNTRY).setCountry("EG").build();

        String locale = getActivity().getResources().getConfiguration().locale.getCountry();
        Log.i("My locale ",getUserCountry(getActivity()));
        autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteDes_fragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocompleteDes_fragment);


        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG , Place.Field.ADDRESS)).setCountry(getUserCountry(getActivity()));
        autocompleteDes_fragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG , Place.Field.ADDRESS)).setCountry(getUserCountry(getActivity()));

        autocompleteFragment.setHint(getString(R.string.enterStart));
        autocompleteDes_fragment.setHint(getString(R.string.enterDestination));



        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.

                if(place.getLatLng() != null) {
                    Log.e("MAPP", "Place: " + place.getName() + " , place  " + place.getLatLng().latitude);

                    Start = place.getAddress();

                    latFrom = place.getLatLng().latitude;
                    lngFrom = place.getLatLng().longitude;

                    LatLng latLng = new LatLng(latFrom, lngFrom);
                    startMarker = new MarkerOptions();
                    // Setting the position for the marker
                    startMarker.position(latLng);
                    startMarker.title(getString(R.string.startLocation));
                    startMarker.snippet(getString(R.string.startLocationDes));
                    // Clears the previously touched position
                    if (googleMap != null) {

                        prepareLocationsInMap(0);
                    }

               }
                else{
                    Log.e("MAPP", "Place: null 2547" );

                }

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("MAPP", "An error occurred: " + status);
            }

        });

        autocompleteDes_fragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.

                if(place.getLatLng() != null) {
                    Log.e("MAPP", "Place: " + place.getName() + " , place  " + place.getLatLng().latitude);


//                    View view1 = getActivity().getCurrentFocus();
//                    if(view1 != null)
//                        view1 = new View(getActivity());
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0);



                    End = place.getAddress() ;

                    latDes = place.getLatLng().latitude;
                    lngDes = place.getLatLng().longitude;

                    LatLng latLng = new LatLng(latDes, lngDes);
                    endMarker = new MarkerOptions();
                    // Setting the position for the marker
                    endMarker.position(latLng);
                    endMarker.title(getString(R.string.desLocation));
                    endMarker.snippet(getString(R.string.desLocationDes));
                    // Clears the previously touched position
                    if (googleMap != null) {

                        prepareLocationsInMap(1);

                    }
                }
                else{
                    Log.e("MAPP", "Place: null 2547" );

                }

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("MAPP", "An error occurred: " + status);
            }

        });

    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.allowLocation))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getString(R.string.No), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 200);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i("MAP", "Place: " + place.getName() + ", " + place.getId());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("MAP", status.getStatusMessage());
            } else if (resultCode == getActivity().RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
    private boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void drawLine(double latFrom , double lngFrom , double latDes , double lngDes){
        final String origin = ""+latFrom+","+lngFrom;
        final String des = ""+latDes+","+lngDes;
        //Define list to get all latlng for the route
        List<LatLng> path = new ArrayList();

        //Execute Directions API request
        GeoApiContext context = new GeoApiContext().setQueryRateLimit(3).setApiKey("AIzaSyB4ski2q_cAYGl2LA4aHyjU3LALRspXZvM")
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
        DirectionsApiRequest req = DirectionsApi.getDirections(context, origin , des);
        try {
            DirectionsResult res = req.await();

            //Loop through legs and steps to get encoded polylines of each step
            if (res.routes != null && res.routes.length > 0) {
                DirectionsRoute route = res.routes[0];

                if (route.legs !=null) {
                    for(int i=0; i<route.legs.length; i++) {
                        DirectionsLeg leg = route.legs[i];
                        if (leg.steps != null) {
                            for (int j=0; j<leg.steps.length;j++){
                                DirectionsStep step = leg.steps[j];
                                if (step.steps != null && step.steps.length >0) {
                                    for (int k=0; k<step.steps.length;k++){
                                        DirectionsStep step1 = step.steps[k];
                                        EncodedPolyline points1 = step1.polyline;
                                        if (points1 != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                            for (com.google.maps.model.LatLng coord1 : coords1) {
                                                path.add(new LatLng(coord1.lat, coord1.lng));
                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline points = step.polyline;
                                    if (points != null) {
                                        //Decode polyline and add points to list of route coordinates
                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
                                        for (com.google.maps.model.LatLng coord : coords) {
                                            path.add(new LatLng(coord.lat, coord.lng));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch(Exception ex) {
            Log.e("ERROR", ex.getLocalizedMessage());
        }

        //Draw the polyline
        if (path.size() > 0) {
            PolylineOptions opts = new PolylineOptions().addAll(path).color(Color.BLUE).width(5);
            googleMap.addPolyline(opts);
        }

        googleMap.getUiSettings().setZoomControlsEnabled(true);

    }

    private void directionClick(){
        boolean Cancel = false ;
        Log.i("StartDire",Start);
        if (Start.isEmpty())
        {
            Cancel = true ;
            Toast.makeText(getActivity(), "Please select start position1", Toast.LENGTH_SHORT).show();
        }
        else {
            if (latFrom != 0 && lngFrom != 0)
            {

            }
            else
            {
                Cancel = true ;
                Toast.makeText(getActivity(), "Please select start position2", Toast.LENGTH_SHORT).show();
            }
        }

        if (End.isEmpty())
        {
            Cancel = true ;
            Toast.makeText(getActivity(), "Please select end position1", Toast.LENGTH_SHORT).show();
        }
        else {
            if (latDes != 0 && lngDes != 0)
            {

            }
            else
            {
                Cancel = true ;
                Toast.makeText(getActivity(), "Please select end position2", Toast.LENGTH_SHORT).show();
            }
        }
        if (!Cancel)
        {
            Log.d("DataBOBA" , latFrom +" : "+lngFrom+" : "+latDes+" : "+lngDes+" : "+Start+" : "+End);
            SharedHelpers.putKey(getActivity() , "StartTripTaxiLat" , latFrom+"");
            SharedHelpers.putKey(getActivity() , "StartTripTaxiLng" , lngFrom+"");
            SharedHelpers.putKey(getActivity() , "EndTripTaxiLat" , latDes+"");
            SharedHelpers.putKey(getActivity() , "EndTripTaxiLng" , lngDes+"");
            SharedHelpers.putKey(getActivity() , "EndTripTaxi" , End);
            SharedHelpers.putKey(getActivity() , "StartTripTaxi" , Start);
            navController.navigate(R.id.action_enterDestination_to_tracking);
            Start = "" ;
            End = "" ;
        }
    }


    private void zoomBetweenTwoLocation(){

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (MarkerOptions marker : markerOptionsList) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int padding = 0; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);

        //googleMap.moveCamera(cu);
        googleMap.animateCamera(cu);
    }


    private void prepareLocationsInMap(int type ){


        googleMap.clear();
        getDrivers();

        if (type == 0)
        {
            if (startMarker!=null ){

                markerOptionsList.set(0, startMarker);
                googleMap.addMarker(startMarker);
                Log.d("MARKERS" ,"NEW 0");
                Log.d("MARKERS" , startMarker.getPosition().latitude +"HEEJHJEJ");
                Log.d("MARKERS" , startMarker.getPosition().longitude+"HEEJHJEJ");
            }

        }

        else if (type == 1)
        {
            if (endMarker != null )
            {

                markerOptionsList.set(1,endMarker);
                googleMap.addMarker(endMarker);
                Log.d("MARKERS" ,"NEW 1");
                Log.d("MARKERS" , markerOptionsList.size()+" HEEJHJEJ");
                Log.d("MARKERS" , endMarker.getPosition().latitude+"HEEJHJEJ");
                Log.d("MARKERS" , endMarker.getPosition().longitude+"HEEJHJEJ");
            }
        }





        googleMap.clear();
        getDrivers();

        if (markerOptionsList.size() > 0)
        {
            Log.d("AMMMMM" , markerOptionsList.size()+"");
            for ( int i = 0 ; i < markerOptionsList.size() ;i++)
            {
                if(markerOptionsList.get(i) != null)
                    googleMap.addMarker(markerOptionsList.get(i));
            }

            if (markerOptionsList.size() == 2)
            {
                if(markerOptionsList.get(0) != null && markerOptionsList.get(1) != null ) {
                    drawLine(markerOptionsList.get(0).getPosition().latitude, markerOptionsList.get(0).getPosition().longitude, markerOptionsList.get(1).getPosition().latitude, markerOptionsList.get(1).getPosition().longitude);
                    zoomBetweenTwoLocation();
                }
            }
        }
    }

    public String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            }
            else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        }
        catch (Exception e) {
            Toast.makeText(context, "Exception "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    final Marker[] markerList = new Marker[5];
    final ArrayList<Marker> markerArrayList = new ArrayList<>(5);
    final ArrayList<String> usersArrayList = new ArrayList<>();
    int index = -1;
    Boolean allowAddMarkers = true;
    Map<String,Marker> markerMap = new HashMap<>();
    private void getDrivers(){
        Log.i("DATALOC0","Entered");
        try {
            FirebaseDatabase.getInstance().getReference().child("driverLocation").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (final DataSnapshot ds : dataSnapshot.getChildren()) {
                        Log.i("DATALOC1", dataSnapshot.toString());
                        final String key = ds.getKey();
                        usersArrayList.add(key);

                        FirebaseDatabase.getInstance().getReference().child("driverLocation").child(Objects.requireNonNull(key)).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Log.i("DATALOC3", dataSnapshot.toString());

                                if (dataSnapshot.exists()) {
                                    Log.i("DATALOC4", dataSnapshot.toString());

                                        double lat = Double.parseDouble(dataSnapshot.child("lat").getValue().toString());
                                        double lng = Double.parseDouble(dataSnapshot.child("lon").getValue().toString());
                                        String userId = dataSnapshot.child("userId").getValue().toString();
                                        Log.i("Lat", lat + "");
                                        Log.i("Lon", lng + "");
                                        LatLng driverLatLng = new LatLng(lat, lng);
                                    Log.i("Tracking0",String.valueOf(ds.getChildrenCount()));
                                    Log.i("Tracking10",String.valueOf(usersArrayList.size()));

                                        if(allowAddMarkers){
                                        for(int i = 0 ; i < usersArrayList.size() ; i++){
//                                            marker.setTag(usersArrayList.get(i));
//                                            usersArrayList.add(marker.getPosition());

                                            markerMap.put(usersArrayList.get(i),googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_test_one))));
//                                            marker =  googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_test_one)));
                                        }
                                        allowAddMarkers = false;
                                        Log.i("Tracking1","True");
                                        }
                                    Log.i("Tracking2",FirebaseDatabase.getInstance().getReference().child("driverLocation").child(key).getKey());
                                    Log.i("Tracking3",FirebaseDatabase.getInstance().getReference().child("driverLocation").child(key).getRef().toString());

//                                    if(!allowAddMarkers) {
                                        if (FirebaseDatabase.getInstance().getReference().child("driverLocation").child(key).getKey().equals(userId)) {
//                                            Marker marker1 = marker.getTag().equals("");
//                                            String tagId= String.valueOf(marker.getTag());
//                                            marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_test_one)));

                                            marker = markerMap.get(userId);

                                            animateMarker(marker, driverLatLng, false);
//                                        }
                                    }



//                                Marker marker = googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cat_top_two)));


//                                    if(index < 5){
//                                        index += 1;
//                                        markerArrayList.add(googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cat_top_two))));
////                                        markerList[index] = googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cat_top_two)));
//                                        marker = markerArrayList.get(index);
////                                        if(marker == null) {
////                                            marker = googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cat_top_two)));
////                                        }
//
//                                        animateMarker(marker, driverLatLng, false);
//                                    }

//                                        MarkerOptions markerOptions = new MarkerOptions();
//                                        markerOptions.position(driverLatLng);
//                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cat_top_two));


//                                    if(marker == null) {
//                                        marker = googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cat_top_two)));
//                                    }
////                                    animateMarker(marker, driverLatLng, false);
//                                    CarMoveAnim.startcarAnimation(marker,googleMap, driverLatLng,driverLatLng,0,new GoogleMap.CancelableCallback() {
//                                        @Override
//                                        public void onFinish() {
//                                        }
//                                        @Override
//                                        public void onCancel() {
//                                        }
//                                    });
//                                        googleMap.addMarker(marker1);

                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }catch (Exception e){
            Toast.makeText(requireContext(), e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }
    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler;
        handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = googleMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }


}
