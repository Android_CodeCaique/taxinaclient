package com.example.taxinaclient.ui.chat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.model.Message;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Chat extends Fragment {
    private RecyclerView rv_chat;
    private ChatRecyclerViewAdabter chatRecyclerViewAdabter ;
    private List<Message>Messages ;
    private String TripID ;
    private String UserID ;
    private String CurrentTime ;
    private Calendar calendar;
    private int  minuteT, hourT;

    private String ImageDriver , NameDriver , PhoneDriver , ContentDriver ;

    private CircleImageView userImage ;
    private ImageView call ;
    private TextView userName ,content;


    private  ImageView send ;
    private EditText contentMessage ;


    private MKLoader mkLoaderId;
    public Chat() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_chat, container, false);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mkLoaderId = view.findViewById(R.id.mkLoaderId);

        TripID = SharedHelpers.getKey(getActivity() , "TripID");
        UserID = SharedHelpers.getKey(getActivity() , "IDUser");
        ImageDriver = SharedHelpers.getKey(getActivity() , "DriverImage"  );
        NameDriver = SharedHelpers.getKey(getActivity() , "DriverName" );
        PhoneDriver = SharedHelpers.getKey(getActivity() , "DriverPhone"  );
        ContentDriver = SharedHelpers.getKey(getActivity() , "DriverTrip");


        userImage = view.findViewById(R.id.userImage);

        if (!ImageDriver.isEmpty())
            Picasso.get().load(ImageDriver).error(R.drawable.logo).into(userImage);


        call = view.findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PhoneDriver.isEmpty())
                {
                    if (!PhoneDriver.equals(""))
                    {

                        if(checkPermission()){
                            callingProcess(PhoneDriver);
                        }else{
                            requestPermission();

                        }

                    }
                }
            }
        });

        userName = view.findViewById(R.id.userName);
        userName.setText(NameDriver);


        content = view.findViewById(R.id.content);
        content.setText(ContentDriver);

        contentMessage = view.findViewById(R.id.contentMessage);

        send = view.findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contentMessage.getText().toString().isEmpty())
                {
                    contentMessage.setError(getActivity().getString(R.string.Empty));
                }
                else
                {
                    sendMessage(contentMessage.getText().toString());
                }
            }
        });


        rv_chat = view.findViewById(R.id.rv_chat);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL,false);
        rv_chat.setLayoutManager(linearLayoutManager);

        Messages = new ArrayList<>();
        getMessages();
    }

    private void getMessages(){
        mkLoaderId.setVisibility(View.VISIBLE);

        DatabaseReference DB = FirebaseDatabase.getInstance().getReference().child(TripID).child("Messages");
        DB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists())
                {
                    Messages = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren())
                    {
                        if (snapshot.child("User").exists())
                        {
//                            if (snapshot.child("User").getValue().equals("1"))
//                            {
                                Message message = new Message();
                                message.setId(snapshot.child("User").getValue().toString());
                                message.setContent(snapshot.child("Content").getValue().toString());
                                if (snapshot.child("Time").getValue() != null)
                                {
                                    message.setCurrentTime(snapshot.child("Time").getValue().toString());
                                }
                                Messages.add(message);
//                            }

                        }

                    }
                    mkLoaderId.setVisibility(View.GONE);
                    chatRecyclerViewAdabter = new ChatRecyclerViewAdabter(getActivity() , Messages);
                    rv_chat.setAdapter(chatRecyclerViewAdabter);
                }
                else {
                    mkLoaderId.setVisibility(View.GONE);
                    chatRecyclerViewAdabter = new ChatRecyclerViewAdabter(getActivity() , Messages);
                    rv_chat.setAdapter(chatRecyclerViewAdabter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mkLoaderId.setVisibility(View.GONE);
                chatRecyclerViewAdabter = new ChatRecyclerViewAdabter(getActivity() , Messages);
                rv_chat.setAdapter(chatRecyclerViewAdabter);
                Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void sendMessage(String message){

        calendar = Calendar.getInstance();
        hourT = calendar.get(Calendar.HOUR_OF_DAY);
        minuteT = calendar.get(Calendar.MINUTE);

        String CurrentTime = hourT+"-"+minuteT ;
        DatabaseReference DB = FirebaseDatabase.getInstance().getReference().child(TripID);
        DB.child("User").setValue(UserID);
        String id = DB.push().getKey();
        DB.child("Messages").child(id).child("Content").setValue(message);
        DB.child("Messages").child(id).child("Time").setValue(CurrentTime);
        DB.child("Messages").child(id).child("User").setValue("1");
        chatRecyclerViewAdabter.notifyDataSetChanged();
        contentMessage.setText("");

    }


    private void callingProcess(String phone){

        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }

    private boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        return result1 == PackageManager.PERMISSION_GRANTED ;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE  }, 200);
    }

}
