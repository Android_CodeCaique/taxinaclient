package com.example.taxinaclient.ui.location;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.LongDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.MapsActivity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.journey.SeatsAdapter;
import com.example.taxinaclient.networking.Authentication;
import com.example.taxinaclient.networking.Buse;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.networking.Ride;
import com.example.taxinaclient.pojo.model.Package;
import com.example.taxinaclient.pojo.model.Seat;
import com.example.taxinaclient.pojo.responses.CarAvilableResponse;
import com.example.taxinaclient.pojo.responses.DetailsTripResponse;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.GoogleDircetionsAPIResponse;
import com.example.taxinaclient.pojo.responses.InfoDriverTripResponse;
import com.example.taxinaclient.pojo.responses.LoginResponse;
import com.example.taxinaclient.pojo.responses.MakeTripResponse;
import com.example.taxinaclient.pojo.responses.SpecialTripResponse;
import com.example.taxinaclient.pojo.responses.TripReposne;
import com.example.taxinaclient.ui.location.dialogs.DatePickerDialog;
import com.example.taxinaclient.ui.location.dialogs.NoteDialog;
import com.example.taxinaclient.ui.location.dialogs.PromoDialog;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.EncodedPolyline;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tracking extends Fragment {
    private ImageView go ;
    private NavController navController ;
    private TextView currant_loc , destination , timeCarArrivedProcess;
    private CircleImageView userImage ;

    private View view;
    private MapView mMapView;
    private GoogleMap googleMap;
    private double latDes, lngDes, latFrom, lngFrom;
    private FusedLocationProviderClient fusedLocationClient;

    private RadioButton male , female ;
    private TextView gender ;

    private TextView distanceTxtViewAPI , durationTxtViewId;

    private View BookNowContent ,  genderContent , ProcessingContent , CancelConfirmTripContent , driverInfoContent;

    // BookNow Views
    private ImageView calender , openNote , promoCode;
    private RecyclerView rv_cars ;
    private int id = -1 , promo = 0;
    private String price ;
    private Button ConfirmOrderAfterChooseCar ;
    private AvailableCarsAdapter availableCarsAdapter ;
    private TextView cash_payment_tv ;


    // Processing Views
    private Button cancelOrder ;

    // Cancel Views
    private  Button CancelTripBT ;

    // Info Driver Views
    private  CircleImageView driverImage ;
    private ImageView calling , chating;
    private TextView nameDriver , time ,tv_user_name , tripCode ,carCode;
    private RatingBar rate ;
    private String PhoneNumber , driverID;

    private List<MarkerOptions>markerOptionsList;
    private MKLoader mkLoaderId ;
    private ImageView fromto ;

    Marker marker ;

    String durationGoogleAPI;

    Boolean getTimeForDriverFirstTime = true;

    public Tracking() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tracking, container, false);


        latDes = 0;
        latFrom = 0;
        lngDes = 0;
        lngFrom = 0;

        PhoneNumber = "" ;
        driverID = "" ;

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mkLoaderId = view.findViewById(R.id.mkLoaderId);
        fromto = view.findViewById(R.id.fromto);
        mkLoaderId.setVisibility(View.GONE);





        mMapView.onResume();


        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                Log.i("DATALOC-1","Map Ready");


                // For showing a move to my location button
              //  googleMap.setMyLocationEnabled(true);

                latFrom = Double.parseDouble(SharedHelpers.getKey(getActivity() , "StartTripTaxiLat" ));
                lngFrom = Double.parseDouble(SharedHelpers.getKey(getActivity() , "StartTripTaxiLng" ));
                latDes = Double.parseDouble(SharedHelpers.getKey(getActivity() , "EndTripTaxiLat" ));
                lngDes = Double.parseDouble(SharedHelpers.getKey(getActivity() , "EndTripTaxiLng" ));


                LatLng latLngDes = new LatLng(latDes, lngDes);
                MarkerOptions markerOptionsDes = new MarkerOptions();
                // Setting the position for the marker
                markerOptionsDes.position(latLngDes);
                markerOptionsDes.title(getString(R.string.desLocation));
                markerOptionsDes.snippet(getString(R.string.desLocationDes));
                // Clears the previously touched position
                // Animating to the touched position
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLngDes));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLngDes, 18);
                googleMap.animateCamera(cameraUpdate);

                // Placing a marker on the touched position
                googleMap.addMarker(markerOptionsDes);

                LatLng latLngFrom = new LatLng(latFrom, lngFrom);
                MarkerOptions markerOptionsFrom = new MarkerOptions();
                // Setting the position for the marker
                markerOptionsFrom.position(latLngFrom);
                markerOptionsFrom.title(getString(R.string.startLocation));
                markerOptionsFrom.snippet(getString(R.string.startLocationDes));
                // Clears the previously touched position
                // Animating to the touched position
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLngFrom));
                googleMap.animateCamera(cameraUpdate);
                // Placing a marker on the touched position
                googleMap.addMarker(markerOptionsFrom);

                markerOptionsList = new ArrayList<>();
                markerOptionsList.add(markerOptionsFrom);
                markerOptionsList.add(markerOptionsDes);


                if (googleMap!= null)
                {
                    if (markerOptionsList.size() == 2)
                    {
                        if(markerOptionsList.get(0) != null && markerOptionsList.get(1) != null ) {
                            drawLine(markerOptionsList.get(0).getPosition().latitude, markerOptionsList.get(0).getPosition().longitude, markerOptionsList.get(1).getPosition().latitude, markerOptionsList.get(1).getPosition().longitude,false);
                            zoomBetweenTwoLocation();
                        }
                    }
                }


            }
        });


        return view ;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);


        markerOptionsList = new ArrayList<>();
        userImage = view.findViewById(R.id.userImage);
        if (!SharedHelpers.getKey(getActivity(), "Image").isEmpty())
            Picasso.get().load(SharedHelpers.getKey(getActivity(), "Image")).error(R.drawable.logo).into(userImage);
        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2Activity.drawerLayout.openDrawer(GravityCompat.START);
            }
        });


        male = view.findViewById(R.id.male);
        female = view.findViewById(R.id.female);
        gender = view.findViewById(R.id.gender);
        go = view.findViewById(R.id.go);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directionGenderClick();
                //navController.navigate(R.id.action_tracking_to_confirmCarOrderScreen);
            }
        });
        currant_loc = view.findViewById(R.id.currant_loc);
        destination = view.findViewById(R.id.destination);
        destination.setText( SharedHelpers.getKey(getActivity() , "EndTripTaxi" ));
        currant_loc.setText( SharedHelpers.getKey(getActivity() , "StartTripTaxi" ));



        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            double lat = location.getLatitude();
                            double lng = location.getLongitude();

                            LatLng latLng = new LatLng(lat, lng);
                            MarkerOptions markerOptions = new MarkerOptions();
                            // Setting the position for the marker
                            markerOptions.position(latLng);
                            // Clears the previously touched position
                            if (googleMap != null) {
                              //  googleMap.clear();
                                // Animating to the touched position
//                                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
//                                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
//                                googleMap.animateCamera(cameraUpdate);

                            }

                        } else {
                            Log.d("HERe", "ERROR to get the location");
                        }
                    }
                });
        // Now we work in layouts that include in this screen .


        BookNowContent  = view.findViewById(R.id.BookNowContent);
        genderContent = view.findViewById(R.id.genderContent);
        ProcessingContent = view.findViewById(R.id.ProcessingContent);
        CancelConfirmTripContent = view.findViewById(R.id.CancelConfirmTripContent);
        driverInfoContent = view.findViewById(R.id.driverInfoContent);

        // BookNow
        calender = BookNowContent.findViewById(R.id.calender);
        cash_payment_tv = BookNowContent.findViewById(R.id.cash_payment_tv);

        rv_cars = BookNowContent.findViewById(R.id.rv_cars);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_cars.setLayoutManager(linearLayoutManager);
        rv_cars.setHasFixedSize(true);

        calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog();
                datePickerDialog.show(getActivity().getSupportFragmentManager() , "DatePickerDialog");
            }
        });


        distanceTxtViewAPI = BookNowContent.findViewById(R.id.tripDistanceId);
        durationTxtViewId = BookNowContent.findViewById(R.id.tripDurationId);

        ConfirmOrderAfterChooseCar = BookNowContent.findViewById(R.id.ConfirmOrderAfterChooseCar);
        ConfirmOrderAfterChooseCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id != -1)
                {
                    makeTrip();
                }
                else
                {
                    Toast.makeText(getActivity(), getString(R.string.pleaseSelect), Toast.LENGTH_SHORT).show();
                }
            }
        });

        openNote = BookNowContent.findViewById(R.id.openNote);
        openNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoteDialog noteDialog = new NoteDialog();
                noteDialog.show(getActivity().getSupportFragmentManager() , "NoteDialog");
            }
        });


        promoCode = BookNowContent.findViewById(R.id.promoCode);
        promoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PromoDialog promoDialog = new PromoDialog();
                promoDialog.show(getActivity().getSupportFragmentManager() , "PromoDialog");
            }
        });


        // processing
        cancelOrder = ProcessingContent.findViewById(R.id.cancelOrder);

        cancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProcessingContent.setVisibility(View.GONE);
                CancelConfirmTripContent.setVisibility(View.VISIBLE);
            }
        });

//        latFrom = Double.parseDouble(SharedHelpers.getKey(getActivity() , "StartTripTaxiLat" ));
//        lngFrom = Double.parseDouble(SharedHelpers.getKey(getActivity() , "StartTripTaxiLng" ));


        // Cancel
        CancelTripBT = CancelConfirmTripContent.findViewById(R.id.CancelTripBT);
        CancelTripBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelTrip();
            }
        });

        // info Driver
        driverImage = driverInfoContent.findViewById(R.id.driverImage);
        calling = driverInfoContent.findViewById(R.id.calling);
        chating = driverInfoContent.findViewById(R.id.chating);
        nameDriver = driverInfoContent.findViewById(R.id.nameDriver);
        rate = driverInfoContent.findViewById(R.id.rate);
        time = driverInfoContent.findViewById(R.id.time);
        tv_user_name = driverInfoContent.findViewById(R.id.tv_user_name);
        tripCode = driverInfoContent.findViewById(R.id.tripCode);
        carCode = driverInfoContent.findViewById(R.id.carCode);
        timeCarArrivedProcess = driverInfoContent.findViewById(R.id.time);


        chating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_tracking_to_chat);
            }
        });
        calling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PhoneNumber.isEmpty())
                {
                    if (!PhoneNumber.equals(""))
                    {

                        if(checkPermission()){
                            callingProcess(PhoneNumber);
                        }else{
                            requestPermission();

                        }

                    }
                }
            }
        });

        if (SharedHelpers.getKey(getActivity() , "OPENGRAPH").equals("1"))
        {
            getDetailsTrip();
            driverInfoContent.setVisibility(View.VISIBLE);
            getInfoDriverTrip();
            SharedHelpers.putKey(getActivity() , "OPENGRAPH" , "0");
            drawLineDriverAfterAccept();
        }


        if (SharedHelpers.getKey(getActivity() , "LANG").equals("ar"))
        {
            fromto.setImageDrawable(getActivity().getDrawable(R.drawable.ic_arrow_back_black_24dp));
            cash_payment_tv.setCompoundDrawables(getActivity().getDrawable(R.drawable.ic_keyboard_arrow_left_black_24dp) , null , null , null);
        }


    }


    private void drawLine(double latFrom , double lngFrom , double latDes , double lngDes , boolean isDriver){
        final String origin = ""+latFrom+","+lngFrom;
        final String des = ""+latDes+","+lngDes;
        //Define list to get all latlng for the route
        List<LatLng> path = new ArrayList();

        //Execute Directions API request
        GeoApiContext context = new GeoApiContext().setQueryRateLimit(3).setApiKey("AIzaSyB4ski2q_cAYGl2LA4aHyjU3LALRspXZvM")
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
        DirectionsApiRequest req = DirectionsApi.getDirections(context, origin , des);
        try {
            DirectionsResult res = req.await();

            //Loop through legs and steps to get encoded polylines of each step
            if (res.routes != null && res.routes.length > 0) {
                DirectionsRoute route = res.routes[0];

                if (route.legs !=null) {
                    for(int i=0; i<route.legs.length; i++) {
                        DirectionsLeg leg = route.legs[i];
                        if (leg.steps != null) {
                            for (int j=0; j<leg.steps.length;j++){
                                DirectionsStep step = leg.steps[j];
                                if (step.steps != null && step.steps.length >0) {
                                    for (int k=0; k<step.steps.length;k++){
                                        DirectionsStep step1 = step.steps[k];
                                        EncodedPolyline points1 = step1.polyline;
                                        if (points1 != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                            for (com.google.maps.model.LatLng coord1 : coords1) {
                                                path.add(new LatLng(coord1.lat, coord1.lng));
                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline points = step.polyline;
                                    if (points != null) {
                                        //Decode polyline and add points to list of route coordinates
                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
                                        for (com.google.maps.model.LatLng coord : coords) {
                                            path.add(new LatLng(coord.lat, coord.lng));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch(Exception ex) {
            Log.e("ERROR", ex.getLocalizedMessage());
        }

        //Draw the polyline
        if (path.size() > 0) {
            PolylineOptions opts;
            if(isDriver) {
                opts = new PolylineOptions().addAll(path).color(Color.BLACK).width(5);
            }else{
                opts = new PolylineOptions().addAll(path).color(Color.BLUE).width(5);
            }
            googleMap.addPolyline(opts);
        }

        googleMap.getUiSettings().setZoomControlsEnabled(true);

    }

    private void directionGenderClick(){
        boolean Cancel = false ;
        String Gender = "" ;
        if (!male.isChecked())
        {
            if (female.isChecked())
            {
                Gender = "1";
            }
            else
            {
                Cancel = true ;
            }
        }
        else if (!female.isChecked())
        {
            if (male.isChecked())
            {
                Gender = "0";
            }
            else
            {
                Cancel = true ;
            }
        }

        if (!Cancel)
        {
            SharedHelpers.putKey(getActivity() , "GenderDriver" , Gender);
            genderContent.setVisibility(View.GONE);
            BookNowContent.setVisibility(View.VISIBLE);
            loadCars();
        }
        else
        {
            gender.setError(getActivity().getString(R.string.Empty));
        }


    }

    private void loadCars(){

        mkLoaderId.setVisibility(View.VISIBLE);

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Car buse = (Car) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Car.class);

        getTimeBetweenTwoLocations(String.valueOf(latFrom),String.valueOf(lngFrom),String.valueOf(latDes),String.valueOf(lngDes),true);


        String distance = getDistanceBetweenTwoLocations()+"";
//        distanceTxtViewAPI.setText(distance);

        Log.d("Distance" , distance);

        SharedHelpers.putKey(getActivity() , "DistanceTrip" , distance);

        buse.getCars( distance)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                   mkLoaderId.setVisibility(View.GONE);
                                    final CarAvilableResponse carAvilableResponse = new Gson().fromJson(responseData, CarAvilableResponse.class);
                                    Toast.makeText(getActivity(), carAvilableResponse.getMessage_en(), Toast.LENGTH_SHORT).show();


                                    final List<Package>Packages = new ArrayList<>();
                                    for (int i = 0 ; i < carAvilableResponse.getData().size() ; i++)
                                    {
                                        Package pack = new Package();
                                        pack.setCheck(false);
                                        pack.setId(carAvilableResponse.getData().get(i).getId());
                                        pack.setName(carAvilableResponse.getData().get(i).getName());
                                        pack.setImage(carAvilableResponse.getData().get(i).getImage());
                                        pack.setPrice(carAvilableResponse.getData().get(i).getPrice());
                                        Packages.add(pack);
                                    }
                                    availableCarsAdapter = new AvailableCarsAdapter(Packages , getActivity());
                                    rv_cars.setAdapter(availableCarsAdapter);
                                    availableCarsAdapter.notifyDataSetChanged();

                                    availableCarsAdapter.setOnItemClickListener(new AvailableCarsAdapter.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(int i) {
                                            id = carAvilableResponse.getData().get(i).getId();
                                            price = carAvilableResponse.getData().get(i).getPrice();
                                            for (int k = 0 ; k < Packages.size() ; k++)
                                            {
                                                if (k == i )
                                                {
                                                    Packages.get(k).setCheck(true);

                                                }
                                                else
                                                {
                                                    Packages.get(k).setCheck(false);
                                                }
                                            }
                                            availableCarsAdapter.notifyDataSetChanged();
                                        }
                                    });
                                }
                                else
                                {
                                   mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                               mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                           mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                       mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getTimeBetweenTwoLocations(String latFrom , String lngFrom , String latDes, String lngDes, final Boolean isTripData) {

        Log.e("TimeLocation",latFrom+","+lngFrom+"//"+latDes+","+lngDes);

        ((Car) new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/maps/api/directions/")
                .build()
                .create(Car.class))
                .getDistanceAndTimeGoogleAPI(latFrom+","+lngFrom,
                        latDes+","+lngDes,
                        "AIzaSyB4ski2q_cAYGl2LA4aHyjU3LALRspXZvM","driving")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    mkLoaderId.setVisibility(View.GONE);
                                    GoogleDircetionsAPIResponse googleResponse = new Gson().fromJson(responseData, GoogleDircetionsAPIResponse.class);
                                    try {
                                        if (isTripData) {
                                            durationTxtViewId.setText(googleResponse.getRoutes().get(0).getLegs().get(0).getDuration().getText());
                                            distanceTxtViewAPI.setText(googleResponse.getRoutes().get(0).getLegs().get(0).getDistance().getText());
                                        }
                                        else{
                                            timeCarArrivedProcess.setText(getResources().getString(R.string.TaxiWillArriveAfter)+" "+googleResponse.getRoutes().get(0).getLegs().get(0).getDuration().getText());
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Toast.makeText(requireActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else
                                {
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

    }

    private double getDistanceBetweenTwoLocations(){
        Location locationA = new Location("From");

        locationA.setLatitude(latFrom);
        locationA.setLongitude(lngFrom);

        Location locationB = new Location("Des");

        locationB.setLatitude(latDes);
        locationB.setLongitude(lngDes);

        double distance = locationA.distanceTo(locationB);


        return distance;
    }

    private void makeTrip(){
        mkLoaderId.setVisibility(View.VISIBLE);

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Car buse = (Car) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Car.class);


        String distance = SharedHelpers.getKey(getActivity() , "DistanceTrip" );
        String latStart = latFrom+"" ;
        String lngStart = lngFrom+"" ;
        String latEnd = latDes+"" ;
        String lngEnd = lngDes+"" ;
        String start = SharedHelpers.getKey(getActivity() , "StartTripTaxi" ) ;
        String end = SharedHelpers.getKey(getActivity() , "EndTripTaxi" ) ;
        String taxis = SharedHelpers.getKey(getActivity() , "PromoTripTaxi");
        String note = SharedHelpers.getKey(getActivity() , "NoteTripTaxi");
        String type = SharedHelpers.getKey(getActivity() , "TypeTimeOrder");
        if (type.isEmpty() || type.equals(""))
        {
            type = "0";
        }
        String time = SharedHelpers.getKey(getActivity() , "TimeOrder");
        String date = SharedHelpers.getKey(getActivity() , "DateOrder");
        final String gender = SharedHelpers.getKey(getActivity() , "GenderDriver");
        String baka_id = id+"" ;

        Log.d("DataBOBA" , latFrom +" : "+lngFrom+" : "+latDes+" : "+lngDes+" : "+start+" : "+end);

        if (!taxis.equals(""))
        {
            promo = Integer.parseInt(taxis);
            double x = Double.parseDouble(price) - ((promo * Double.parseDouble(price))/100);
            price = x+"" ;
        }
        buse.makeTrip(latStart , lngStart , start , latEnd , lngEnd , end , distance , taxis , note , type , date , time , gender , baka_id , price)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                   mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                    MakeTripResponse makeTripResponse = new Gson().fromJson(responseData, MakeTripResponse.class);

                                    ProcessingContent.setVisibility(View.VISIBLE);
                                    BookNowContent.setVisibility(View.GONE);
//                                    drawLineDriverAfterAccept();

                                    SharedHelpers.putKey(getActivity(), "TripID" , makeTripResponse.getTrip_id()+"");
                                    SharedHelpers.putKey(getActivity() , "DistanceTrip" , "");
                                    SharedHelpers.putKey(getActivity() , "StartTripTaxi", "" ) ;
                                    SharedHelpers.putKey(getActivity() , "EndTripTaxi", "" ) ;
                                    SharedHelpers.putKey(getActivity() , "PromoTripTaxi", "");
                                    SharedHelpers.putKey(getActivity() , "NoteTripTaxi", "");
                                    SharedHelpers.putKey(getActivity() , "TypeTimeOrder", "");
                                    SharedHelpers.putKey(getActivity() , "TimeOrder", "");
                                    SharedHelpers.putKey(getActivity() , "DateOrder", "");
                                    SharedHelpers.putKey(getActivity() , "GenderDriver" , "");
                                }
                                else
                                {
                                   mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                               mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                           mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                       mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void cancelTrip(){
        mkLoaderId.setVisibility(View.VISIBLE);

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Car buse = (Car) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Car.class);

        buse.cancelTrip( SharedHelpers.getKey(getActivity(), "TripID" )).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                   mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                                    navController.navigate(R.id.action_tracking_to_chooseKind);
                                }
                                else
                                {
                                   mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                               mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                           mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                       mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getInfoDriverTrip(){
        mkLoaderId.setVisibility(View.VISIBLE);

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Car buse = (Car) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Car.class);


        buse.showDriverTrip(SharedHelpers.getKey(getActivity() , "TripID")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0) {
                           mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                            InfoDriverTripResponse infoDriverTripResponse = new Gson().fromJson(responseData, InfoDriverTripResponse.class);

                            driverID = infoDriverTripResponse.getData().getId()+"";
                            PhoneNumber = infoDriverTripResponse.getData().getPhone();
                            nameDriver.setText(infoDriverTripResponse.getData().getCar_name());
                            tv_user_name.setText(infoDriverTripResponse.getData().getFirst_name()+" "+infoDriverTripResponse.getData().getLast_name());

                            if (infoDriverTripResponse.getData().getRate() != null)
                            {
                                float x = Float.parseFloat(infoDriverTripResponse.getData().getRate()+"");
                                rate.setRating(x);
                            }
                            carCode.setText(infoDriverTripResponse.getData().getCode());
                            tripCode.setText(infoDriverTripResponse.getData().getPlate());
                            time.setText(infoDriverTripResponse.getData().getPick_time());

                            Picasso.get().load(infoDriverTripResponse.getData().getPhoto()).error(R.drawable.logo).into(driverImage);

                            SharedHelpers.putKey(getActivity() , "DriverImage" ,infoDriverTripResponse.getData().getPhoto() );
                            SharedHelpers.putKey(getActivity() , "DriverName" ,infoDriverTripResponse.getData().getFirst_name()+  " "+infoDriverTripResponse.getData().getLast_name());
                            SharedHelpers.putKey(getActivity() , "DriverPhone" ,infoDriverTripResponse.getData().getPhone() );
                            SharedHelpers.putKey(getActivity() , "DriverTrip" ,infoDriverTripResponse.getData().getCode() +"-"+infoDriverTripResponse.getData().getPlate());

                        }
                        else
                        {
                           mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                       mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                   mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
               mkLoaderId.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void callingProcess(String phone){

        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }

    private boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        return result1 == PackageManager.PERMISSION_GRANTED ;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE  }, 200);
    }

    private void drawLineDriverAfterAccept(){

        Log.i("DriverId",driverID);
//        final Query databaseReference =  FirebaseDatabase.getInstance().getReference("driverLocation").child( driverID);
        FirebaseDatabase.getInstance().getReference().child( "driverLocation")
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.i("DataSnap",dataSnapshot.toString());
                boolean allowRemoveMarker = false;
                if (dataSnapshot.exists())
                {
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        String key = snapshot.getKey();
                        if(Objects.equals(key, driverID)){
                            try {

                                Log.i("DriverLat1", snapshot.child("lat").getValue().toString());
                                Log.i("DriverLat2", snapshot.child( driverID).child("lat").getValue().toString());

                                double lat = Double.parseDouble(snapshot.child("lat").getValue().toString());
                                double lng = Double.parseDouble(snapshot.child("lon").getValue().toString());
//                        Toast.makeText(getActivity(), "Location Of Driver \n"+lat+"\n"+lng, Toast.LENGTH_SHORT).show();

                                Log.i("DriverLat", String.valueOf(lat));
                                Log.i("DriverLng", String.valueOf(lng));
                                markerOptionsList = new ArrayList<>();

                                MarkerOptions markerOptionsFrom = new MarkerOptions();
                                LatLng latLng1 = new LatLng(lat, lng);
                                markerOptionsFrom.position(latLng1);
//                        drawLine(latFrom , lngFrom , latDes , lngDes,false);

                                MarkerOptions markerOptionsDes = new MarkerOptions();
                                LatLng latLng2 = new LatLng(latFrom, lngFrom);
                                markerOptionsFrom.position(latLng2);

                                markerOptionsList.add(markerOptionsFrom);
                                markerOptionsList.add(markerOptionsDes);
                                zoomBetweenTwoLocation();


                                LatLng driverLatLng = new LatLng(lat, lng);
                                drawLine(lat, lng, latFrom, lngFrom, true);
//                        drawLine(latFrom , lngFrom , latDes , lngDes,false);

                                if (marker == null)
                                    marker = googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_test_one)));
                                else
                                    animateMarker(marker, driverLatLng, false);

                                if (getTimeForDriverFirstTime) {
//                            getTimeBetweenTwoLocations(String.valueOf(latFrom), String.valueOf(lngFrom), String.valueOf(lat), String.valueOf(lng), false);
                                    getTimeForDriverFirstTime = false;
                                }

                            }catch (Exception e){
                                Toast.makeText(requireContext(), e.getMessage().toString(), Toast.LENGTH_SHORT).show();
//                    timeCarArrivedProcess = ProcessingContent.findViewById(R.id.timeTxtId)
                            };
                        }
                    }
//                    for (DataSnapshot snapshot : dataSnapshot.getChildren())
//                    {

//
//                    cancelOrder.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            ProcessingContent.setVisibility(View.GONE);
//                            CancelConfirmTripContent.setVisibility(View.VISIBLE);
//                        }
//                    });

//        latFrom = Double.parseDouble(SharedHelpers.getKey(getActivity() , "StartTripTaxiLat" ));
//        lngFrom = Double.parseDouble(SharedHelpers.getKey(getActivity() , "StartTripTaxiLng" ));


//                        try {
//                            googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).title("Driver"));
////
////                            if (allowRemoveMarker) {
////                                marker.remove();
////                            }
////
////                            allowRemoveMarker = true;
////                            Marker marker2 = googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//
////                                googleMap.clear();
////                                googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//
//                        }catch (Exception e)
//                        {
//                            Toast.makeText(getActivity(), "Exception Marker\n"+e.getMessage(), Toast.LENGTH_SHORT).show();
//                        }

//                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler;
        handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = googleMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    private void getDetailsTrip(){
        mkLoaderId.setVisibility(View.VISIBLE);

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Ride buse = (Ride) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Ride.class);

        buse.ShowTripById(SharedHelpers.getKey(getActivity() , "TripID") , "1").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0) {
                           mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                                TripReposne tripReposne = new Gson().fromJson(responseData, TripReposne.class);

                                if (mMapView != null)
                                {

                                    latDes = Double.parseDouble(tripReposne.getData().getEnd_lat());
                                    lngDes = Double.parseDouble(tripReposne.getData().getEnd_long());
                                    latFrom = Double.parseDouble(tripReposne.getData().getStart_lat());
                                    lngFrom = Double.parseDouble(tripReposne.getData().getStart_long());



                                    SharedHelpers.putKey(getActivity() , "StartTripTaxiLat"  , latFrom+"");
                                    SharedHelpers.putKey(getActivity() , "StartTripTaxiLng"  , lngFrom+"");
                                    SharedHelpers.putKey(getActivity() , "EndTripTaxiLat"  , latDes+"");
                                    SharedHelpers.putKey(getActivity() , "EndTripTaxiLng"  , lngDes+"");


                                    LatLng latLngDes = new LatLng(latDes, lngDes);
                                    MarkerOptions markerOptionsDes = new MarkerOptions();
                                    // Setting the position for the marker
                                    markerOptionsDes.position(latLngDes);
                                    markerOptionsDes.title("Your Destination");
                                    markerOptionsDes.snippet("This location is that u wanna go to it ");
                                    // Clears the previously touched position
                                    // Animating to the touched position
                                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLngDes));
                                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLngDes, 18);
                                    googleMap.animateCamera(cameraUpdate);

                                    // Placing a marker on the touched position
                                    googleMap.addMarker(markerOptionsDes);

                                    LatLng latLngFrom = new LatLng(latFrom, lngFrom);
                                    MarkerOptions markerOptionsFrom = new MarkerOptions();
                                    // Setting the position for the marker
                                    markerOptionsFrom.position(latLngFrom);
                                    markerOptionsFrom.title("Your Start");
                                    markerOptionsFrom.snippet("This location is that u wanna go to start from it ");
                                    // Clears the previously touched position
                                    // Animating to the touched position
                                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLngFrom));
                                    googleMap.animateCamera(cameraUpdate);
                                    // Placing a marker on the touched position
                                    googleMap.addMarker(markerOptionsFrom);


                                    drawLine(latFrom , lngFrom , latDes , lngDes,false);
                                }


                                SharedHelpers.putKey(getActivity() , "EndTripTaxi" ,  tripReposne.getData().getEnd_location());
                                SharedHelpers.putKey(getActivity() , "StartTripTaxi" , tripReposne.getData().getStart_location());

                            destination.setText( tripReposne.getData().getEnd_location());
                            currant_loc.setText( tripReposne.getData().getStart_location());

                        }
                        else
                        {
                           mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                       mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                   mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
               mkLoaderId.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void zoomBetweenTwoLocation(){

        try {
            if(SharedHelpers.getKey(requireContext(),"AllowLatLngBound").equals("false")) {

            }else {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (MarkerOptions marker : markerOptionsList) {
                    builder.include(marker.getPosition());
                }
                final LatLngBounds bounds = builder.build();
                final int padding = 50; // offset from edges of the map in pixels


                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        //Your code where exception occurs goes here...
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                        googleMap.animateCamera(cu);

                    }
                });
                SharedHelpers.putKey(requireContext(),"AllowLatLngBound","true");
            }
        }catch (Exception e)
        {
//            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void getDriverLocationFromFirebase( String driverId )
    {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("driverLocation");
        myRef.child(driverId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


}
