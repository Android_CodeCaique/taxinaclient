package com.example.taxinaclient.ui.location.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.responses.MyFavoritesResponse;
import com.example.taxinaclient.utils.SharedHelpers;

import java.util.List;

public class FavoriteDialogRecyclerAdapter extends RecyclerView.Adapter<ViewHolderFavorite> {
    List<MyFavoritesResponse.DataBean> arrayList;
    Context context;
    int type;
//    Double lat , lon;
//    String locationName;
    FavoriteFragmentDialog favoriteFragmentDialog;
//    Geocoder geocoder;
MutableLiveData<String> locationNameStart;
MutableLiveData<String> locationNameEnd;
MutableLiveData<String> startLat;
MutableLiveData<String> startLon;
MutableLiveData<String> endLat;
MutableLiveData<String> endLon;

    public FavoriteDialogRecyclerAdapter(List<MyFavoritesResponse.DataBean> arrayList, Context context, int type, FavoriteFragmentDialog favoriteFragmentDialog, MutableLiveData<String> locationNameStart, MutableLiveData<String> locationNameEnd, MutableLiveData<String> startLat, MutableLiveData<String> startLon, MutableLiveData<String> endLat, MutableLiveData<String> endLon) {
        this.arrayList = arrayList;
        this.context = context;
        this.type = type;
        this.favoriteFragmentDialog = favoriteFragmentDialog;
        this.locationNameStart = locationNameStart;
        this.locationNameEnd = locationNameEnd;
        this.startLat = startLat;
        this.startLon = startLon;
        this.endLat = endLat;
        this.endLon = endLon;
    }

    View view;
    @NonNull
    @Override
    public ViewHolderFavorite onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.my_favorites_dialog_item,parent,false);
        ViewHolderFavorite viewHolderNotification = new ViewHolderFavorite(view);
        return viewHolderNotification;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderFavorite holder, int position) {
        final MyFavoritesResponse.DataBean dataBean = arrayList.get(position);
        holder.favoriteName.setText(dataBean.getName());
        holder.favoriteAddress.setText(dataBean.getAddress());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type == 1) {

                    locationNameStart.setValue(dataBean.getName());
                    startLat.setValue(dataBean.getLatitude());
                    startLon.setValue(dataBean.getLongitude());
                }
                else if(type ==2)
                {

                    locationNameEnd.setValue(dataBean.getName());
                    endLat.setValue(dataBean.getLatitude());
                    endLon.setValue(dataBean.getLongitude());
                }
//                lat = Double.parseDouble(dataBean.getLatitude());
//                lon = Double.parseDouble(dataBean.getLongitude());
//                locationName = dataBean.getName();
                favoriteFragmentDialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

class ViewHolderFavorite extends RecyclerView.ViewHolder{

    TextView favoriteName;
    TextView favoriteAddress;
    CardView cardView;
    public ViewHolderFavorite(@NonNull View itemView) {
        super(itemView);

        favoriteName = itemView.findViewById(R.id.favoriteNameId);
        favoriteAddress = itemView.findViewById(R.id.favoriteGeocoderId);
        cardView = itemView.findViewById(R.id.favoriteCarId);
    }
}