package com.example.taxinaclient.ui.Auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Authentication;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.LoginResponse;
import com.example.taxinaclient.pojo.responses.RegisterResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.example.taxinaclient.utils.Validation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUp extends Fragment {
    NavController navController ;
    private EditText FirstName , LastName , Email , Phone , Password , RatioGroup ;
    private RadioButton female , male ;
    private TextView gender ;
    private View v ;
    private MKLoader mkLoaderId;

    public SignUp() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_sign_up, container, false);
        return v ;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

        mkLoaderId = v.findViewById(R.id.mkLoaderId);
        FirstName = v.findViewById(R.id.et_firstName);
        LastName = v.findViewById(R.id.et_lastName);
        Email = v.findViewById(R.id.et_email);
        Phone = v.findViewById(R.id.et_phone);
        Phone = v.findViewById(R.id.et_phone);
        female = v.findViewById(R.id.female);
        male = v.findViewById(R.id.male);
        Password = v.findViewById(R.id.et_password);
        gender = v.findViewById(R.id.gender);
        Button btn_SignUp= v.findViewById(R.id.btn_SignUp);
        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpClick();
            }
        });

        TextView tv_signup= v.findViewById(R.id.tv_signup);
        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_signUp_to_login);

            }
        });

        if (SharedHelpers.getKey(getActivity() , "LANG").equals("ar"))
        {
            Log.d("HERE" , "ARABIC");

            Password.setGravity(View.TEXT_ALIGNMENT_TEXT_END);

        }
    }


    private void signUpClick(){
        boolean Cancel = false ;

        if (FirstName.getText().toString().isEmpty())
        {
            Cancel = true ;
            FirstName.setError(getString(R.string.Empty));
        }
        if (LastName.getText().toString().isEmpty())
        {
            Cancel = true ;
            LastName.setError(getString(R.string.Empty));
        }
        if (Email.getText().toString().isEmpty())
        {
            Cancel = true ;
            Email.setError(getString(R.string.Empty));
        }else
        {
            if (!Email.getText().toString().contains("@") || !Email.getText().toString().contains("."))
            {
                Cancel = true ;
                Email.setError(getString(R.string.EmailNotValid));
            }
        }
//        if (Phone.getText().toString().isEmpty())
//        {
//            Cancel = true ;
//            Phone.setError(getString(R.string.Empty));
//        }
        if (Password.getText().toString().isEmpty())
        {
            Cancel = true ;
            Password.setError(getString(R.string.Empty));
        }else
        {
            if (!(Password.getText().toString().length() > 5))
            {
                Cancel = true ;
                Password.setError(getString(R.string.PasswordNotValid));
            }
        }
        if (!female.isChecked() )
        {
            if ( !male.isChecked())
            {
                Cancel = true ;
                gender.setError(getString(R.string.Empty));
            }

        }
        if (!male.isChecked() )
        {
            if ( !female.isChecked())
            {
                Cancel = true ;
                gender.setError(getString(R.string.Empty));
            }

        }

        Log.d("Cancel" , Cancel+"");
        if (!Cancel) {
            Log.d("Cancel" , Cancel+"HERE");
            String Type = "";
            if (female.isChecked()) {
                Type = "1";
            } else if (male.isChecked())
            {
                Type = "0";
            }
            else
            {
                Type ="0";
            }

            if (Validation.isOnline(getActivity()))
            {
                mkLoaderId.setVisibility(View.VISIBLE);
                signUpFunctionApi(Email.getText().toString() , Password.getText().toString() ,FirstName.getText().toString()  , LastName.getText().toString() , SharedHelpers.getKey(requireContext(),"VerificationPhone") , Type);
            }
            else
                Toast.makeText(getActivity(), "Please check your connection with internet", Toast.LENGTH_SHORT).show();

        }
    }
    private void signUpFunction(final String EmailT, String PasswordT , final String FirstNameT , final String LastNameT , final String PhoneT , final String GenderT) {
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        Log.d("Cancel" , "in real function HERE");
        final DatabaseReference DB = FirebaseDatabase.getInstance().getReference().child("User");
        firebaseAuth.createUserWithEmailAndPassword(EmailT, PasswordT)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful())
                {
                    String ID = firebaseAuth.getCurrentUser().getUid();
                    DB.child(ID).child("FirstName").setValue(FirstNameT);
                    DB.child(ID).child("LastName").setValue(LastNameT);
                    DB.child(ID).child("Phone").setValue(PhoneT);
                    DB.child(ID).child("Gender").setValue(GenderT);
                    DB.child(ID).child("Email").setValue(EmailT);

                    Email.setText("");
                    FirstName.setText("");
                    LastName.setText("");
                    Password.setText("");
                    Phone.setText("");

                    male.setChecked(false);
                    female.setChecked(false);

                    SharedHelpers sharedHelper = new SharedHelpers();
                    sharedHelper.putKey(getActivity() , "TokenFirebase" , ID);

                    Toast.makeText(getActivity(), "Done , Register process is Successful", Toast.LENGTH_SHORT).show();

                    if ( sharedHelper.getKey(getActivity() , "Intro" ).equals("1"))
                    {
                        Intent intent = new Intent(getActivity() , Main2Activity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                    else
                    {
                        navController.navigate(R.id.action_signUp_to_intro1);
                    }

                     mkLoaderId.setVisibility(View.GONE);


                }
                else
                {
                     mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), " Register process is Failure", Toast.LENGTH_SHORT).show();
                    Log.d("Failure" , "Failure");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                 mkLoaderId.setVisibility(View.GONE);
                Toast.makeText(getActivity(), " Register process is Failure because "+e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("Failure" , "Failure"+e.getMessage());
            }
        });
    }

    private void signUpFunctionApi(final String EmailT, String PasswordT , final String FirstNameT , final String LastNameT , final String PhoneT , final String GenderT){
        ((Authentication) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Authentication.class))
                .register(FirstNameT , LastNameT , EmailT , PhoneT , PasswordT , GenderT , "0" , "0" , FirebaseInstanceId.getInstance().getToken()
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0 ) {
                             mkLoaderId.setVisibility(View.GONE);
                            LoginResponse registerResponse = new Gson().fromJson(responseData, LoginResponse.class);
                            Toast.makeText(getActivity(), registerResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                            SharedHelpers.putKey(getActivity() , "TokenFirebase" , registerResponse.getData().getToken());
                            SharedHelpers.putKey(getActivity() , "IDUser" , registerResponse.getData().getId()+"");
                            SharedHelpers.putKey(getActivity() , "Image" , registerResponse.getData().getImage());
                            SharedHelpers.putKey(getActivity() , "Name" , registerResponse.getData().getFirst_name()+" "+registerResponse.getData().getLast_name());
                            SharedHelpers.putKey(getActivity() , "Email" , registerResponse.getData().getEmail());

                            Email.setText("");
                            FirstName.setText("");
                            LastName.setText("");
                            Password.setText("");
                            Phone.setText("");

                            male.setChecked(false);
                            female.setChecked(false);

                            if ( SharedHelpers.getKey(getActivity() , "Intro" ).equals("1"))
                            {
                                Intent intent = new Intent(getActivity() , Main2Activity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                            else
                            {
                                navController.navigate(R.id.action_signUp_to_intro1);
                            }
                        }
                        else
                        {
                             mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                         mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                     mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                 mkLoaderId.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
