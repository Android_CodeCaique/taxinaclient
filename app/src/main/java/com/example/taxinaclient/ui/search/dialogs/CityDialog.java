package com.example.taxinaclient.ui.search.dialogs;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Buse;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.pojo.model.Areas;
import com.example.taxinaclient.pojo.responses.AreasResposne;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.PromoCodeReponse;
import com.example.taxinaclient.ui.search.AreasAdapter;
import com.example.taxinaclient.ui.search.SearchBuses;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CityDialog extends DialogFragment {

    private View view ;
    private RecyclerView Areas ;
    private ProgressDialog progressDialog ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.cities_dialog, container, false);
        getDialog().getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.back_edit_chat));

        init();
        return view ;
    }

    private void init(){
        Areas = view.findViewById(R.id.Areas);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        Areas.setLayoutManager(linearLayoutManager);
        progressDialog = new ProgressDialog(getActivity());

        Areas.setAdapter(SearchBuses.areasAdapter);
        SearchBuses.areasAdapter.setOnItemClickListener(new AreasAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int i) {
                for (int j = 0 ; j <SearchBuses.areas.size() ; j++)
                {
                    if (j == i)
                    {
                        SearchBuses.areas.get(j).setCheck(true);
                    }
                    else
                    {
                        SearchBuses.areas.get(j).setCheck(false);
                    }
                }
                SearchBuses.areasAdapter.notifyDataSetChanged();

                String Typer = SharedHelpers.getKey(getActivity() , "TYPESearch");

                if (Typer.equals("1"))
                {
                    SharedHelpers.putKey(getActivity() , "TYPESearchIDTo" , SearchBuses.areas.get(i).getArea_id());
                    SearchBuses.Destination.setText(SearchBuses.areas.get(i).getCity_name()+" - "+SearchBuses.areas.get(i).getArea_name());
                }
                else if (Typer.equals("2"))
                {
                    SharedHelpers.putKey(getActivity() , "TYPESearchIDFrom" , SearchBuses.areas.get(i).getArea_id());
                    SearchBuses.From.setText(SearchBuses.areas.get(i).getCity_name()+" - "+SearchBuses.areas.get(i).getArea_name());
                }
                dismiss();
            }
        });
    }


}
