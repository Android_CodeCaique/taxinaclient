package com.example.taxinaclient.ui.Auth;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.taxinaclient.R;
import com.example.taxinaclient.databinding.FragmentVerifyCodeBinding;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class VerifyCode extends Fragment {

    FragmentVerifyCodeBinding binding;
    String mVerificationId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentVerifyCodeBinding.inflate(inflater);
        binding.setLifecycleOwner(this);

//        binding.btnVerifyCode.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                verifyVerificationCode(requireContext(), Objects.requireNonNull(binding.codeId.getOtp()));
//            }
//        });

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                SharedHelpers.getKey(requireContext(), "VerificationPhone"),
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);

        return binding.getRoot();
    }

    public void verifyVerificationCode(Context context , String code) {
        if (code.isEmpty())
            Toast.makeText(context, "Enter code !", Toast.LENGTH_SHORT).show();
        else {
            binding.mkLoaderId.setVisibility(View.VISIBLE);
//            String verificationId = SharedHelpers.getKey(context, "ActiveCode");
            FirebaseAuth auth = FirebaseAuth.getInstance();
//            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);

            auth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        binding.mkLoaderId.setVisibility(View.INVISIBLE);
                        Navigation.findNavController(requireView()).navigate(R.id.action_verifyCode_to_signUp);
                    } else {
                        binding.mkLoaderId.setVisibility(View.INVISIBLE);
                        Toast.makeText(requireContext(), "Exception " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    binding.mkLoaderId.setVisibility(View.INVISIBLE);
                    Toast.makeText(requireContext(), "Exception " + e.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
//                editTextCode.setText(code);
                //verifying the code
                binding.codeId.setOTP(code);
                verifyVerificationCode(requireContext(),code);
//                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(requireContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };

}