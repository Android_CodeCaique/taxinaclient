package com.example.taxinaclient.ui.search;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.MapsActivity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Buse;
import com.example.taxinaclient.pojo.model.Areas;
import com.example.taxinaclient.pojo.responses.AreasResposne;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.SearchBusesResponse;
import com.example.taxinaclient.ui.search.dialogs.CityDialog;
import com.example.taxinaclient.utils.SharedHelpers;
import com.example.taxinaclient.utils.Validation;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchBuses extends Fragment {
    public static TextView Destination , From  ;
    private TextView Date;
    private String DateT ;
    private Calendar calendar ;
    private int yearT , monthT , dayT ;
    private double latDes , lngDes , latFrom , lngFrom ;
    private ProgressDialog progressDialog ;
    private  NavController navController ;
    public static SearchBusesResponse response ;
    private CircleImageView drawer ;
    public  static  AreasAdapter areasAdapter ;
    public static   List<com.example.taxinaclient.pojo.model.Areas> areas ;

    private MKLoader mkLoaderId ;

    public SearchBuses() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_buses, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);
        progressDialog = new ProgressDialog(getActivity());
        latDes = 0 ;
        latFrom = 0 ;
        lngDes = 0 ;
        lngFrom = 0 ;
        Destination = view.findViewById(R.id.sp_destination);
        From = view.findViewById(R.id.sp_from);
        Date = view.findViewById(R.id.et_date);

        mkLoaderId = view.findViewById(R.id.mkLoaderId);
        calendar =  Calendar.getInstance() ;
        yearT = calendar.get(Calendar.YEAR);
        monthT = calendar.get(Calendar.MONTH);
        dayT = calendar.get(Calendar.DAY_OF_MONTH);

        areas  = new ArrayList<>();
        Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Date.setText(year+"/"+(month+1)+"/"+dayOfMonth);
                    }
                }  , yearT , monthT , dayT);
                datePickerDialog.show();
            }
        });
        Button btn_searchBus= view.findViewById(R.id.btn_searchBus);
        btn_searchBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchClick();

            }
        });

        Destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedHelpers.putKey(getActivity() , "TYPESearch" , "1");
                if (areas.size() > 0)
                {
                    CityDialog dialog = new CityDialog();
                    dialog.show(getActivity().getSupportFragmentManager() , "AreasDialog");
                }
            }
        });
        From.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedHelpers.putKey(getActivity() , "TYPESearch" , "2");
                if (areas.size() > 0)
                {
                    CityDialog dialog = new CityDialog();
                    dialog.show(getActivity().getSupportFragmentManager() , "AreasDialog");
                }
            }
        });


        drawer = view.findViewById(R.id.drawer);
        drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2Activity.drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        if (!SharedHelpers.getKey(getActivity() , "Image").isEmpty())
            Picasso.get().load(SharedHelpers.getKey(getActivity() , "Image")).error(R.drawable.logo).into(drawer);


        getAreas();

    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION  , Manifest.permission.ACCESS_COARSE_LOCATION}, 200);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == 2) {
            if (resultCode == getActivity().RESULT_OK) {
                if (SharedHelpers.getKey(getActivity() , "TYPESearch" ).equals("1"))
                {
                    latDes = data.getDoubleExtra("lat", 0);
                    lngDes = data.getDoubleExtra("lng", 0);
                }
                else if (SharedHelpers.getKey(getActivity() , "TYPESearch" ).equals("2"))
                {
                    latFrom = data.getDoubleExtra("lat", 0);
                    lngFrom = data.getDoubleExtra("lng", 0);
                }


                try {
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    if (SharedHelpers.getKey(getActivity() , "TYPESearch" ).equals("1"))
                    {
                        addresses = geocoder.getFromLocation(latDes, lngDes, 1);
                        String LocationText = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        Destination.setText(LocationText);
                    }
                    else if (SharedHelpers.getKey(getActivity() , "TYPESearch" ).equals("2"))
                    {
                        addresses = geocoder.getFromLocation(latFrom, lngFrom, 1);
                        String LocationText = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        From.setText(LocationText);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("safrsaf", "nvklv" + e.getMessage());
                }
            }
        }

    }

    private boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        return result1 == PackageManager.PERMISSION_GRANTED ;
    }

    private void searchClick(){
        boolean Cancel = false ;

        if (Destination.getText().toString().isEmpty())
        {
            Cancel = true ;
            Destination.setError(getString(R.string.Empty));
        }
        if (From.getText().toString().isEmpty())
        {
            Cancel = true ;
            From.setError(getString(R.string.Empty));
        }
        if (Date.getText().toString().isEmpty())
        {
            Cancel = true ;
            Date.setError(getString(R.string.Empty));
        }

        if (!Cancel) {
            Log.d("Cancel" , Cancel+"HERE");
            String Type = "";

            if (Validation.isOnline(getActivity()))
            {
//                this.progressDialog.setTitle("Search process is loading");
//                this.progressDialog.setMessage("Please wait ..... ");
//                this.progressDialog.show();
//                this.progressDialog.setCanceledOnTouchOutside(false);
//                this.progressDialog.setCancelable(false);
//                progressDialog.show();

                mkLoaderId.setVisibility(View.VISIBLE);

                searchBusesApi(Destination.getText().toString() , From.getText().toString() , Date.getText().toString());
            }
            else
                Toast.makeText(getActivity(), "Please check your connection with internet", Toast.LENGTH_SHORT).show();

        }
    }

    private void searchBusesApi(final  String DesT , final  String FromT , final String DateT){
        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Buse buse = (Buse) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Buse.class);

        buse.SearchBuses(Date.getText().toString() ,  SharedHelpers.getKey(getActivity() , "TYPESearchIDFrom") , SharedHelpers.getKey(getActivity() , "TYPESearchIDTo"))
                .enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0 ) {
//                            progressDialog.dismiss();

                            mkLoaderId.setVisibility(View.GONE);
                            SearchBusesResponse searchBusesResponse = new Gson().fromJson(responseData, SearchBusesResponse.class);
                     //       Toast.makeText(getActivity(), searchBusesResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                            SharedHelpers sharedHelper = new SharedHelpers();
                            Log.i("SearchBusData",searchBusesResponse.toString());
                            SearchBuses.response = searchBusesResponse ;
                            SharedHelpers.putKey(getActivity() , "StartLocation" , From.getText().toString());
                            SharedHelpers.putKey(getActivity() , "EndLocation" , Destination.getText().toString());
                            SharedHelpers.putKey(getActivity() , "Time" , Date.getText().toString());

                            navController.navigate(R.id.action_searchBuses_to_journeyDetails);
                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mkLoaderId.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }



    private void getAreas(){
        mkLoaderId.setVisibility(View.VISIBLE);
        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Buse buse = (Buse) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Buse.class);

        buse.ShowAreas(SharedHelpers.getKey(requireContext(),"CountryIdLogin"))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    mkLoaderId.setVisibility(View.GONE);
                                    AreasResposne areasResposne = new Gson().fromJson(responseData, AreasResposne.class);
                                    areas = new ArrayList<>();

                                    if (areasResposne.getData() != null)
                                    {
                                        if (areasResposne.getData().size() > 0)
                                        {
                                            for (int i = 0 ; i < areasResposne.getData().size() ; i++)
                                            {
                                                com.example.taxinaclient.pojo.model.Areas areas1 = new Areas();
                                                areas1.setArea_id(areasResposne.getData().get(i).getArea_id());
                                                areas1.setArea_name(areasResposne.getData().get(i).getArea_name());
                                                areas1.setCity_name(areasResposne.getData().get(i).getCity_name());
                                                areas1.setCheck(false);
                                                areas.add(areas1);
                                            }
                                        }
                                    }

                                    areasAdapter = new AreasAdapter(getActivity() , areas);
                                }
                                else
                                {
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
