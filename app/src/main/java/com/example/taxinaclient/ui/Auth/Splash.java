package com.example.taxinaclient.ui.Auth;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.utils.SharedHelpers;

/**
 * A simple {@link Fragment} subclass.
 */
public class Splash extends Fragment {

    NavController navController ;

    public Splash() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

        ImageView background= view.findViewById(R.id.background);
        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   navController.navigate(R.id.action_splash_to_login);

            }
        });

        loadSplash();
    }
    private void loadSplash()
    {
        new Handler().postDelayed(new Runnable() {

            @Override

            public void run() {
                SharedHelpers sharedHelper = new SharedHelpers();
                if (sharedHelper.getKey(getActivity() , "TokenFirebase").equals("") || sharedHelper.getKey(getActivity() , "TokenFirebase").isEmpty())
                {
                    navController.navigate(R.id.action_splash_to_login);
                }
                else
                {
                    Intent intent = new Intent(getActivity() , Main2Activity.class);
                    startActivity(intent);
                    getActivity().finish();
                }

            }

        }, 3000);
    }
}
