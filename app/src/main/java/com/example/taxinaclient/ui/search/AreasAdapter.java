package com.example.taxinaclient.ui.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.model.Areas;
import com.example.taxinaclient.ui.location.AvailableCarsAdapter;

import java.util.List;

public class AreasAdapter extends RecyclerView.Adapter<AreasAdapter.AreasViewModel> {

    private Context context ;
    private List<Areas> areas ;
    private OnItemClickListener mListener;

    public AreasAdapter(Context context, List<Areas> areas) {
        this.context = context;
        this.areas = areas;
    }

    public interface OnItemClickListener {
        void onItemClick(int i);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }
    @NonNull
    @Override
    public AreasViewModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AreasViewModel(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_area, parent, false) , mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull AreasViewModel holder, int position) {
//        if (areas.get(position).isCheck())
//        {
//            holder.area.setBackgroundColor(context.getColor(R.color.yellowdark));
//        }
//        else
//        {
//            holder.area.setBackgroundColor(context.getColor(R.color.white));
//        }
        holder.area.setText(areas.get(position).getCity_name()+" - "+areas.get(position).getArea_name());
    }

    @Override
    public int getItemCount() {
        return areas.size();
    }


    public static class AreasViewModel extends RecyclerView.ViewHolder {
        TextView area ;
        AreasViewModel(View itemView, final OnItemClickListener listener ) {
            super(itemView);
            area = itemView.findViewById(R.id.area);


            itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != -1) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }
}
