package com.example.taxinaclient.ui.location;

import android.content.Context;
import android.location.Geocoder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.responses.MyFavoritesResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class FavoriteLocRecyclerViewAdabter extends RecyclerView.Adapter<ViewHolderFavoriteLoc> {
    List<MyFavoritesResponse.DataBean> arrayList;
    Context context;
//    Geocoder geocoder;

    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int i);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }


    public FavoriteLocRecyclerViewAdabter(List<MyFavoritesResponse.DataBean> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
//        geocoder = new Geocoder(context);

    }

    View view;
    @NonNull
    @Override
    public ViewHolderFavoriteLoc onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.item_my_favorite,parent,false);
        ViewHolderFavoriteLoc viewHolderNotification = new ViewHolderFavoriteLoc(view,mListener);
        return viewHolderNotification;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderFavoriteLoc holder, int position) {
        MyFavoritesResponse.DataBean dataBean = arrayList.get(position);
        holder.favoriteName.setText(dataBean.getName());
        holder.favoriteAddress.setText(dataBean.getAddress());

//        try {
//            holder.favoriteGeocoder.setText(geocoder.getFromLocation(Double.parseDouble(dataBean.getLatitude()),Double.parseDouble(dataBean.getLongitude()),1).get(0).getAddressLine(0));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        holder.tvUserName.setText("***سع");
//        holder.tvOrderName.setText("قهوة حجم كبير");

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(holder.itemView.getContext(), DelievryPlaceActivity2.class);
//                view.getContext().startActivity(intent);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

class ViewHolderFavoriteLoc extends RecyclerView.ViewHolder{

    TextView favoriteName;
    TextView favoriteAddress;
    ImageView deleteImg;
    public ViewHolderFavoriteLoc(@NonNull View itemView, final FavoriteLocRecyclerViewAdabter.OnItemClickListener listener ) {
        super(itemView);

        favoriteName = itemView.findViewById(R.id.favoriteNameId);
        favoriteAddress = itemView.findViewById(R.id.favoriteGeocoderId);
        deleteImg = itemView.findViewById(R.id.deleteImgId);
        
        deleteImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != -1) {
                        listener.onItemClick(position);
                    }
                }
            }
        });
    }
}