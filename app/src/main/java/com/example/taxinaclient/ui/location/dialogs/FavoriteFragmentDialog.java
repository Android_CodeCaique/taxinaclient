package com.example.taxinaclient.ui.location.dialogs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.MyFavoritesResponse;
import com.example.taxinaclient.ui.location.FavoriteLocRecyclerViewAdabter;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FavoriteFragmentDialog extends DialogFragment {
    int type;
    public MutableLiveData<String> locationNameStart = new MutableLiveData<>();
    public MutableLiveData<String> locationNameEnd = new MutableLiveData<>();
    public MutableLiveData<String> startLat = new MutableLiveData<>();
    public MutableLiveData<String> startLon = new MutableLiveData<>();
    public MutableLiveData<String> endLat = new MutableLiveData<>();
    public MutableLiveData<String> endLon = new MutableLiveData<>();

    public FavoriteFragmentDialog(int type) {
        this.type = type;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    //    Double lat , lon;
//    String locationName;
//    public FavoriteFragmentDialog(int type , Double lat , Double lon,String locationName) {
//        this.type = type;
//        this.lat = lat;
//        this.lon = lon;
//        this.locationName = locationName;
//    }

    MKLoader mkLoaderId;
    RecyclerView rv_favoriteLocation;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.dialog_favorite_fragment, container, false);
        rv_favoriteLocation = view.findViewById(R.id.recyclerViewDialogId);
        mkLoaderId = view.findViewById(R.id.mkLoaderId);
        getFavorites();
        return view ;
    }

    private void getFavorites(){
        mkLoaderId.setVisibility(View.VISIBLE);

        ((Car) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Car.class))
                .userFavoritePlace("bearer "+ SharedHelpers.getKey(requireContext(),"TokenFirebase"))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    mkLoaderId.setVisibility(View.GONE);
                                    MyFavoritesResponse myFavoritesResponse = new Gson().fromJson(responseData, MyFavoritesResponse.class);
//                                    Toast.makeText(getActivity(), myFavoritesResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL,false);
                                    FavoriteDialogRecyclerAdapter favoriteLocRecyclerViewAdabter = new FavoriteDialogRecyclerAdapter(myFavoritesResponse.getData(),requireContext(),type,FavoriteFragmentDialog.this ,locationNameStart,locationNameEnd,startLat,startLon,endLat,endLon);
                                    rv_favoriteLocation.setAdapter(favoriteLocRecyclerViewAdabter);
                                    rv_favoriteLocation.setLayoutManager(linearLayoutManager);
                                }
                                else
                                {
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
