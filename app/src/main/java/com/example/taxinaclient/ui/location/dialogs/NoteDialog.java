package com.example.taxinaclient.ui.location.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.taxinaclient.R;
import com.example.taxinaclient.utils.SharedHelpers;

public class NoteDialog extends DialogFragment {

    private View view ;
    private EditText NoteData ;
    private Button SaveNote ;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.bottom_sheet_note_to_driver, container, false);
        init();
        return view ;
    }

    private void init(){
        NoteData = view.findViewById(R.id.et_note);
        SaveNote = view.findViewById(R.id.ConfirmNote);
        SaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (NoteData.getText().toString().isEmpty())
                {
                    NoteData.setError(getActivity().getString(R.string.Empty));
                }
                else
                {
                    NoteData.setText("");
                    SharedHelpers.putKey(getActivity() , "NoteTripTaxi" , NoteData.getText().toString());
                    dismiss();
                }
            }
        });
    }


}
