package com.example.taxinaclient.ui.location;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.MapsFavoriteActivity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.journey.JourneyDetailsRecyclerViewAdabter;
import com.example.taxinaclient.networking.Authentication;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.pojo.responses.BaseResponse;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.LoginSocialResponse;
import com.example.taxinaclient.pojo.responses.MyFavoritesResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyFavorites extends Fragment {

    RecyclerView rv_favoriteLocation;
    List<MyFavoritesResponse.DataBean> arrayList;
    MKLoader mkLoaderId;
    ImageView close;
    TextView addTxt;
    ImageView addTxt2;
    MutableLiveData<String> allowFetch ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_favorites, container, false);
        arrayList = new ArrayList<>();
        mkLoaderId = view.findViewById(R.id.mkLoaderId);
        rv_favoriteLocation = view.findViewById(R.id.rv_favoriteLocation);
        close = view.findViewById(R.id.closeId);
        allowFetch = new MutableLiveData<>("");

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        addTxt = view.findViewById(R.id.addTxtId);
        addTxt2 = view.findViewById(R.id.addTxtId2);
        addTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MapsFavoriteActivity.class));
            }
        });
        addTxt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), MapsFavoriteActivity.class));
                startActivityForResult(new Intent(getActivity(), MapsFavoriteActivity.class), 1);
            }
        });
        allowFetch.observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {

            }
        });
        getFavorites();

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getFavorites();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController= Navigation.findNavController(view);

        ImageView house= view.findViewById(R.id.house);
        house.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // navController.navigate(R.id.action_myFavorites_to_chat);

            }
        });

    }

    private void getFavorites(){
        mkLoaderId.setVisibility(View.VISIBLE);

        ((Car) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Car.class))
                .userFavoritePlace("bearer "+SharedHelpers.getKey(requireContext(),"TokenFirebase"))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    mkLoaderId.setVisibility(View.GONE);
                                    MyFavoritesResponse myFavoritesResponse = new Gson().fromJson(responseData, MyFavoritesResponse.class);
                                    Toast.makeText(getActivity(), myFavoritesResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL,false);
                                    FavoriteLocRecyclerViewAdabter favoriteLocRecyclerViewAdabter = new FavoriteLocRecyclerViewAdabter(myFavoritesResponse.getData(),requireContext());
                                    rv_favoriteLocation.setAdapter(favoriteLocRecyclerViewAdabter);
                                    rv_favoriteLocation.setLayoutManager(linearLayoutManager);
                                    favoriteLocRecyclerViewAdabter.setOnItemClickListener(new FavoriteLocRecyclerViewAdabter.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(int i) {
                                            deleteFavorites(i+1);
                                        }
                                    });
                                }
                                else
                                {
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void deleteFavorites(int id){
        mkLoaderId.setVisibility(View.VISIBLE);

        ((Car) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Car.class))
                .deleteFavoritePlace("bearer "+SharedHelpers.getKey(requireContext(),"TokenFirebase"),id)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    mkLoaderId.setVisibility(View.GONE);
                                    BaseResponse myFavoritesResponse = new Gson().fromJson(responseData, BaseResponse.class);
                                    Toast.makeText(getActivity(), myFavoritesResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    getFavorites();
                                }
                                else
                                {
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
