package com.example.taxinaclient.ui.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.model.Message;

import java.util.List;


public class ChatRecyclerViewAdabter extends RecyclerView.Adapter<ChatRecyclerViewAdabter.ViewHolderChat> {
    public ChatRecyclerViewAdabter(Context context, List<Message> Messages) {
        this.context = context;
        this.Messages = Messages;
    }


    private View view;
    private Context context;
    private List<Message> Messages;

    @NonNull
    @Override
    public ChatRecyclerViewAdabter.ViewHolderChat onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        return new ViewHolderChat(view);

    }


    @Override
    public void onBindViewHolder(@NonNull ChatRecyclerViewAdabter.ViewHolderChat holder, int position) {

        if (Messages.get(position).getId().equals("1"))
        {
            holder.time.setVisibility(View.GONE);
            holder.tv_msg.setVisibility(View.GONE);
            holder.tv_msg.setVisibility(View.GONE);
        }
        else if (Messages.get(position).getId().equals("0"))
        {
            holder.time.setVisibility(View.VISIBLE);
            holder.tv_msg.setVisibility(View.VISIBLE);
            holder.tv_msg_me.setVisibility(View.GONE);
        }

         holder.time.setText(Messages.get(position).getCurrentTime());
         holder.tv_msg.setText(Messages.get(position).getContent());
         holder.tv_msg_me.setText(Messages.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return Messages.size();
    }

    @Override
    public int getItemViewType(int position) {


        switch (Messages.get(position).getId()) {
            case "0":
                return 0;
            case "1":
                return 1;
            default:
                return -1;
        }

    }


    class ViewHolderChat extends RecyclerView.ViewHolder {

        TextView time, tv_msg , tv_msg_me;



        public ViewHolderChat(@NonNull View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);
            tv_msg = itemView.findViewById(R.id.tv_msg);
            tv_msg_me = itemView.findViewById(R.id.tv_msg_me);
        }
    }

}