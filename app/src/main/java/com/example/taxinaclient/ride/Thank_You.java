package com.example.taxinaclient.ride;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.networking.Ride;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.RidesResponse;
import com.example.taxinaclient.pojo.responses.SpecialTripResponse;
import com.example.taxinaclient.pojo.responses.TripReposne;
import com.example.taxinaclient.ride.ReidesAdapter;
import com.example.taxinaclient.ride.dialogs.RateDialog;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class Thank_You extends Fragment {
    private NavController navController ;
    private CircleImageView driverImage ;
    private TextView tv_user_name , userName , tripCode , carCode , total , tv_pick_up ,destination1 , RateOrder;
    private RatingBar rate ;
    private String type , id , wait ;

    private ProgressDialog progressDialog ;
    private Button cancelRide ;


    public Thank_You() {
        // Required empty public constructor
    }

    String TAG = "TripData";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_thank__you, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);


        progressDialog = new ProgressDialog(getActivity());
        cancelRide = view.findViewById(R.id.cancelRide);

        driverImage = view.findViewById(R.id.driverImage);
        tv_user_name = view.findViewById(R.id.tv_user_name);
        userName = view.findViewById(R.id.userName);
        rate = view.findViewById(R.id.rate);
        tripCode = view.findViewById(R.id.tripCode);
        carCode = view.findViewById(R.id.carCode);
        total = view.findViewById(R.id.total);
        tv_pick_up = view.findViewById(R.id.tv_pick_up);
        destination1 = view.findViewById(R.id.destination1);
        type = SharedHelpers.getKey(getActivity() , "TypeTripShow");
        id = SharedHelpers.getKey(getActivity() , "IdTripShow");
        wait = SharedHelpers.getKey(getActivity(),"waiting");

        try {
            Log.e(TAG, "onViewCreated22: " + getArguments().getString("waiting") );
        }
        catch (Exception e)
        {
            Log.e(TAG, "onViewCreated: Catch" + e.getMessage() );
        }


        RateOrder = view.findViewById(R.id.RateOrder);
        RateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RateDialog dialog = new RateDialog();
                dialog.show(getActivity().getSupportFragmentManager() , "RateDialog");
            }
        });

        cancelRide.setVisibility(View.GONE);
        cancelRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelTrip();
            }
        });
        getDetailsTrip();
    }
    private void getDetailsTrip(){
        this.progressDialog.setTitle("Ride process is loading");
        this.progressDialog.setMessage("Please wait ..... ");
        this.progressDialog.show();
        this.progressDialog.setCanceledOnTouchOutside(false);
        this.progressDialog.setCancelable(false);
        progressDialog.show();

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Ride buse = (Ride) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Ride.class);

        buse.ShowTripById(id , type).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                            if (type.equals("1"))
                            {
                                TripReposne tripReposne = new Gson().fromJson(responseData, TripReposne.class);
                                if (tripReposne.getData().getImage() != null)
                                {
                                    Picasso.get().load(tripReposne.getData().getImage()).error(R.drawable.logo).into(driverImage);
                                }
                                userName.setText(tripReposne.getData().getCar_name());

                                Log.e(TAG, "onResponse: " +  tripReposne.getData().getFirst_name()+" "+tripReposne.getData().getLast_name());

                                tv_user_name.setText(tripReposne.getData().getFirst_name()+" "+tripReposne.getData().getLast_name());

                                if (tripReposne.getData().getRate() != null)
                                {
                                    float x = Float.parseFloat(tripReposne.getData().getRate());
                                    rate.setRating(x);
                                }
                                tripCode.setText(tripReposne.getData().getCode());
                                carCode.setText(tripReposne.getData().getPlate());
                                total.setText(tripReposne.getData().getPrice());
                                tv_pick_up.setText(tripReposne.getData().getStart_location());
                                destination1.setText(tripReposne.getData().getEnd_location());



                                if (tripReposne.getData().getState() == 0)
                                {
                                    cancelRide.setVisibility(View.VISIBLE);
                                }

                                else if (tripReposne.getData().getState() == 1)
                                {
                                    cancelRide.setVisibility(View.VISIBLE);
                                }
                                else if (tripReposne.getData().getState() == 2)
                                {
                                    cancelRide.setVisibility(View.GONE);
                                }
                                else if (tripReposne.getData().getState() == 3)
                                {
                                    cancelRide.setVisibility(View.GONE);
                                }
                            }
                            else if (type.equals("2"))
                            {
                                SpecialTripResponse specialTripResponse = new Gson().fromJson(responseData, SpecialTripResponse.class);

                                if (specialTripResponse.getData().getImage() != null)
                                {
                                    if (!!specialTripResponse.getData().getImage().equals(null) )
                                        if( !specialTripResponse.getData().getImage().equals(""))
                                            Picasso.get().load(specialTripResponse.getData().getImage()).error(R.drawable.logo).into(driverImage);
                                }
                                userName.setText(specialTripResponse.getData().getCar_name());
                                tv_user_name.setText(specialTripResponse.getData().getFirst_name()+" "+specialTripResponse.getData().getLast_name());
                                if (specialTripResponse.getData().getRate() != null)
                                {
                                    float x = Float.parseFloat(specialTripResponse.getData().getRate());
                                    rate.setRating(x);
                                }

                                tripCode.setText(specialTripResponse.getData().getCode());
                                carCode.setText(specialTripResponse.getData().getPlate());
                                total.setText(specialTripResponse.getData().getPrice());
                                tv_pick_up.setText(specialTripResponse.getData().getFrom_area());
                                destination1.setText(specialTripResponse.getData().getTo_area());


                                if (specialTripResponse.getData().getState() == 0)
                                {
                                    cancelRide.setVisibility(View.VISIBLE);
                                }

                                else if (specialTripResponse.getData().getState() == 1)
                                {
                                    cancelRide.setVisibility(View.VISIBLE);
                                }
                                else if (specialTripResponse.getData().getState() == 2)
                                {
                                    cancelRide.setVisibility(View.GONE);
                                }
                            }

                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void cancelTrip(){
        this.progressDialog.setTitle("Cancel a trip process is loading");
        this.progressDialog.setMessage("Please wait ..... ");
        this.progressDialog.show();
        this.progressDialog.setCanceledOnTouchOutside(false);
        this.progressDialog.setCancelable(false);
        progressDialog.show();

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Car buse = new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Car.class);

        if (wait.equals("yes"))
        {
            Log.e(TAG, "cancelTrip: " + wait );
            buse.cancelTrip(id).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.code() == 200)
                    {
                        try {
                            String responseData = response.body().string();

                            GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                            if (generalResponse.getError() == 0) {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                                navController.navigate(R.id.chooseKind);
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else if (wait.equals("no")){
            buse.UserRequestCancelTrip(id).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.code() == 200)
                    {
                        try {
                            String responseData = response.body().string();

                            GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                            if (generalResponse.getError() == 0) {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                                navController.navigate(R.id.chooseKind);
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }


}
