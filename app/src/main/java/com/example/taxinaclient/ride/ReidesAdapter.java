package com.example.taxinaclient.ride;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.model.Ride;
import com.example.taxinaclient.pojo.responses.RidesResponse;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.EncodedPolyline;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ReidesAdapter extends RecyclerView.Adapter<ReidesAdapter.RidesViewHolder> {

    private OnItemClickListener mListener;
    private Context context ;
    private List<Ride>Rides ;
    private GoogleMap googleMap; ;

    public ReidesAdapter(Context context, List<Ride> rides) {
        this.context = context;
        Rides = rides;
    }

    @NonNull
    @Override
    public RidesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RidesViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_ride, parent, false) , mListener);

    }

    @Override
    public void onBindViewHolder(@NonNull RidesViewHolder holder, int position) {
        GoogleMap thisMap = holder.mapCurrent;
        if (Rides.get(position).getType() == 2)
        {
            holder.map.setVisibility(View.GONE);
            holder.distance.setVisibility(View.GONE);

            holder.date.setText(Rides.get(position).getCreated_at());
        }
        else if (Rides.get(position).getType() == 1)
        {
            if(thisMap != null) {
                LatLng latLngDes = new LatLng(Double.parseDouble(Rides.get(position).getEnd_lat()), Double.parseDouble(Rides.get(position).getEnd_long()));
                MarkerOptions markerOptionsDes = new MarkerOptions();
                // Setting the position for the marker
                markerOptionsDes.position(latLngDes);
                markerOptionsDes.title("Your Destination");
                markerOptionsDes.snippet("This location is that u wanna go to it ");
                // Clears the previously touched position
                // Animating to the touched position
                thisMap.animateCamera(CameraUpdateFactory.newLatLng(latLngDes));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLngDes, 10);
                thisMap.animateCamera(cameraUpdate);

                // Placing a marker on the touched position
                thisMap.addMarker(markerOptionsDes);



                LatLng latLngFrom = new LatLng(Double.parseDouble(Rides.get(position).getStart_lat()), Double.parseDouble(Rides.get(position).getStart_long()));
                MarkerOptions markerOptionsFrom = new MarkerOptions();
                // Setting the position for the marker
                markerOptionsFrom.position(latLngFrom);
                markerOptionsFrom.title("Your Start");
                markerOptionsFrom.snippet("This location is that u wanna go to start from it ");
                // Clears the previously touched position
                // Animating to the touched position
                thisMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngFrom , 10));
                thisMap.animateCamera(cameraUpdate);
                // Placing a marker on the touched position
                thisMap.addMarker(markerOptionsFrom);


                drawLine(Rides.get(position).getStart_lat() , Rides.get(position).getStart_long() , Rides.get(position).getEnd_lat() , Rides.get(position).getStart_long() , thisMap);
            }
            holder.distance.setText(Rides.get(position).getDistance());
            holder.date.setText(Rides.get(position).getCreated_at().substring(0 , 10));
        }

        holder.cost.setText(Rides.get(position).getPrice()+" "+context.getString(R.string.money));
        holder.from_to.setText(Rides.get(position).getStart_location()+"-"+Rides.get(position).getEnd_location());

    }


    @Override
    public int getItemCount() {
        return Rides.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int i);
    }
    public class RidesViewHolder extends RecyclerView.ViewHolder {
        TextView date ,cost , distance , from_to;
        MapView map ;
        GoogleMap mapCurrent;
        RidesViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            cost = itemView.findViewById(R.id.price);
            distance = itemView.findViewById(R.id.distance);
            from_to = itemView.findViewById(R.id.from_to);
            map = itemView.findViewById(R.id.map);
            if (map != null)
            {
                map.onCreate(null);
                map.onResume();
                map.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        MapsInitializer.initialize(context);
                        mapCurrent = googleMap;

                        notifyDataSetChanged();
                    }
                });
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != -1) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

        }

    }

    private void drawLine(String latFrom , String lngFrom , String latDes , String lngDes , GoogleMap thisMap){
        final String origin = ""+latFrom+","+lngFrom;
        final String des = ""+latDes+","+lngDes;
        //Define list to get all latlng for the route
        List<LatLng> path = new ArrayList();

        //Execute Directions API request
        GeoApiContext context = new GeoApiContext().setQueryRateLimit(3).setApiKey("AIzaSyB4ski2q_cAYGl2LA4aHyjU3LALRspXZvM")
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
        DirectionsApiRequest req = DirectionsApi.getDirections(context, origin , des);
        try {
            DirectionsResult res = req.await();

            //Loop through legs and steps to get encoded polylines of each step
            if (res.routes != null && res.routes.length > 0) {
                DirectionsRoute route = res.routes[0];

                if (route.legs !=null) {
                    for(int i=0; i<route.legs.length; i++) {
                        DirectionsLeg leg = route.legs[i];
                        if (leg.steps != null) {
                            for (int j=0; j<leg.steps.length;j++){
                                DirectionsStep step = leg.steps[j];
                                if (step.steps != null && step.steps.length >0) {
                                    for (int k=0; k<step.steps.length;k++){
                                        DirectionsStep step1 = step.steps[k];
                                        EncodedPolyline points1 = step1.polyline;
                                        if (points1 != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                            for (com.google.maps.model.LatLng coord1 : coords1) {
                                                path.add(new LatLng(coord1.lat, coord1.lng));
                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline points = step.polyline;
                                    if (points != null) {
                                        //Decode polyline and add points to list of route coordinates
                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
                                        for (com.google.maps.model.LatLng coord : coords) {
                                            path.add(new LatLng(coord.lat, coord.lng));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch(Exception ex) {
            Log.e("ERROR", ex.getLocalizedMessage());
        }

        //Draw the polyline
        if (path.size() > 0) {
            PolylineOptions opts = new PolylineOptions().addAll(path).color(Color.BLUE).width(5);
            thisMap.addPolyline(opts);
        }

        thisMap.getUiSettings().setZoomControlsEnabled(true);

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }
}
