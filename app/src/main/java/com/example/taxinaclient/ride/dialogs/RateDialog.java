package com.example.taxinaclient.ride.dialogs;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Ride;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.SpecialTripResponse;
import com.example.taxinaclient.pojo.responses.TripReposne;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RateDialog extends DialogFragment {
    private View view ;
    private RatingBar rate2 ;
    private EditText et_comment ;
    private Button Submit ;
    private ProgressDialog progressDialog ;
    private String type , id ;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_rating, container, false);
        getDialog().getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.back_edit_chat));

        init();
        return view ;
    }

    private void init(){
        type = SharedHelpers.getKey(getActivity() , "TypeTripShow");
        id = SharedHelpers.getKey(getActivity() , "IdTripShow");

        progressDialog = new ProgressDialog(getActivity());
        rate2 = view.findViewById(R.id.rate2);
        et_comment = view.findViewById(R.id.et_comment);
        Submit = view.findViewById(R.id.Submit);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateRide();
            }
        });
    }

    private void rateRide(){

        this.progressDialog.setTitle("Rate process is loading");
        this.progressDialog.setMessage("Please wait ..... ");
        this.progressDialog.show();
        this.progressDialog.setCanceledOnTouchOutside(false);
        this.progressDialog.setCancelable(false);
        progressDialog.show();

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Ride buse = (Ride) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Ride.class);

        buse.RateTripById(id , rate2.getRating()+"" , type).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                            dismiss();
                        }
                        else
                        {
                            dismiss();
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        dismiss();
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    dismiss();
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                dismiss();
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
