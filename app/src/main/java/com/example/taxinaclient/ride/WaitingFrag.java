package com.example.taxinaclient.ride;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.model.Ride;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.RidesResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class WaitingFrag extends Fragment {

    private View view ;
    private RecyclerView rides ;
    private MKLoader mkLoaderId ;
    private List<Ride> Rides ;
    private List<com.example.taxinaclient.pojo.model.Ride> Specials ;
    private Button Trips , Special;
    private NavController navController ;

    String TAG = "Waiting";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_waiting, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

        mkLoaderId = view.findViewById(R.id.mkLoaderId);

        rides = view.findViewById(R.id.rides);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rides.setLayoutManager(linearLayoutManager);

        Rides = new ArrayList<>();
        Specials = new ArrayList<>();

        Trips = view.findViewById(R.id.Trips);
        Special = view.findViewById(R.id.Special);

        getRides();

        Trips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Trips.setBackgroundColor(getActivity().getColor(R.color.colorPrimaryDark));
                Special.setBackgroundColor(getActivity().getColor(R.color.yellowdark));
                ReidesAdapter adapter = new ReidesAdapter(getActivity() , Rides);
                rides.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                adapter.setOnItemClickListener(new ReidesAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int i) {
                        Log.e(TAG, "onItemClick: " + Rides.get(i).getId() );
                        SharedHelpers.putKey(getActivity() , "TypeTripShow" , Rides.get(i).getType()+"");
                        SharedHelpers.putKey(getActivity() , "IdTripShow" , Rides.get(i).getId()+"");
                        SharedHelpers.putKey(getActivity(),"waiting","yes");

                        navController.navigate(R.id.action_myRide_to_thank_You);
                    }
                });
            }
        });
        Special.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Special.setBackgroundColor(getActivity().getColor(R.color.colorPrimaryDark));
                Trips.setBackgroundColor(getActivity().getColor(R.color.yellowdark));
                ReidesAdapter adapter = new ReidesAdapter(getActivity() , Specials);
                rides.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                adapter.setOnItemClickListener(new ReidesAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int i) {
                        SharedHelpers.putKey(getActivity() , "TypeTripShow" , Specials.get(i).getType()+"");
                        SharedHelpers.putKey(getActivity() , "IdTripShow" , Specials.get(i).getId()+"");
                        SharedHelpers.putKey(getActivity(),"waiting","yes");


                        Log.e(TAG, "onItemClick: " + Rides.get(i).getId() );
                        navController.navigate(R.id.action_myRide_to_thank_You);
                    }
                });
            }
        });

    }

    private void getRides(){
        mkLoaderId.setVisibility(View.VISIBLE);


        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Log.e(TAG, "getRides: " +  Token);
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final com.example.taxinaclient.networking.Ride buse = (com.example.taxinaclient.networking.Ride) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(com.example.taxinaclient.networking.Ride.class);

        buse.WaitingTrip().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0) {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                            Rides.clear();
                            Specials.clear();

                            RidesResponse ridesResponse = new Gson().fromJson(responseData, RidesResponse.class);
                            if (ridesResponse.getData() != null)
                            {
                                if (ridesResponse.getData().size() > 0) {
                                    for (int i = 0; i < ridesResponse.getData().size(); i++)
                                    {
                                        com.example.taxinaclient.pojo.model.Ride ride = new com.example.taxinaclient.pojo.model.Ride();
                                        ride.setType(1);
                                        ride.setId(ridesResponse.getData().get(i).getId());
                                        ride.setStart_lat(ridesResponse.getData().get(i).getStart_lat());
                                        ride.setStart_long(ridesResponse.getData().get(i).getStart_long());
                                        ride.setEnd_lat(ridesResponse.getData().get(i).getEnd_lat());
                                        ride.setEnd_long(ridesResponse.getData().get(i).getEnd_long());
                                        ride.setCreated_at(ridesResponse.getData().get(i).getCreated_at());
                                        ride.setDistance(ridesResponse.getData().get(i).getDistance());
                                        ride.setStart_location(ridesResponse.getData().get(i).getStart_location());
                                        ride.setEnd_location(ridesResponse.getData().get(i).getEnd_location());
                                        ride.setPrice(ridesResponse.getData().get(i).getPrice());


                                        Rides.add(ride);
                                    }
                                }
                            }


                            if (ridesResponse.getSpecial_trip() != null)
                            {
                                if (ridesResponse.getSpecial_trip().size() > 0) {
                                    for (int i = 0; i < ridesResponse.getSpecial_trip().size(); i++)
                                    {
                                        com.example.taxinaclient.pojo.model.Ride ride = new com.example.taxinaclient.pojo.model.Ride();
                                        ride.setType(2);
                                        ride.setId(ridesResponse.getSpecial_trip().get(i).getId());
                                        ride.setCreated_at(ridesResponse.getSpecial_trip().get(i).getDate());
                                        ride.setStart_location(ridesResponse.getSpecial_trip().get(i).getFrom_area());
                                        ride.setEnd_location(ridesResponse.getSpecial_trip().get(i).getTo_area());
                                        ride.setPrice(ridesResponse.getSpecial_trip().get(i).getPrice());


                                        Specials.add(ride);
                                    }
                                }
                            }
                            ReidesAdapter adapter = new ReidesAdapter(getActivity() , Rides);
                            adapter.setOnItemClickListener(new ReidesAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(int i) {
                                    SharedHelpers.putKey(getActivity() , "TypeTripShow" , Rides.get(i).getType()+"");
                                    SharedHelpers.putKey(getActivity() , "IdTripShow" , Rides.get(i).getId()+"");
                                    Log.e(TAG, "onItemClickFun: " +  Rides.get(i).getId());

                                    SharedHelpers.putKey(getActivity(),"waiting","yes");


                                    navController.navigate(R.id.action_myRide_to_thank_You);
                                }
                            });
                            rides.setAdapter(adapter);
                            adapter.notifyDataSetChanged();


                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mkLoaderId.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}