package com.example.taxinaclient.ride;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class MyAdapterTabs extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public MyAdapterTabs(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                WaitingFrag waitingFrag = new WaitingFrag();
                return waitingFrag;
            case 1:
                UpcomingFrag upcomingFrag = new UpcomingFrag();
                return upcomingFrag;
            case 2:
                CompletedFrag completedFrag = new CompletedFrag();
                return completedFrag;
            case 3:
                CanceledFrag canceledFrag = new CanceledFrag();
                return canceledFrag;
            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}