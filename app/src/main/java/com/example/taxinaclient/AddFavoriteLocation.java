package com.example.taxinaclient;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.taxinaclient.databinding.FragmentAddFavoriteLocationBinding;
import com.google.android.gms.maps.MapFragment;

public class AddFavoriteLocation extends Fragment {

    private MapFragment mapFragment;
    FragmentAddFavoriteLocationBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentAddFavoriteLocationBinding.inflate(inflater);


        return binding.getRoot();
    }
}