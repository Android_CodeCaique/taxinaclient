package com.example.taxinaclient.forgetPassword;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Authentication;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.LoginResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.example.taxinaclient.utils.Validation;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForgetPasswordFragment extends Fragment {
    private EditText et_email ;
    private MKLoader mkLoaderId ;
    private Button btn_Continue ;
    private View v ;
    private NavController navController ;

    public ForgetPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_forget_password, container, false);
        return v ;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

        et_email = view.findViewById(R.id.et_email);
        mkLoaderId = view.findViewById(R.id.mkLoaderId);
        btn_Continue = view.findViewById(R.id.btn_Continue);
        btn_Continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgetClick();
            }
        });
    }

    private void forgetClick(){
        boolean Cancel = false ;
        if (et_email.getText().toString().isEmpty())
        {
            Cancel = true ;
            et_email.setError(getString(R.string.Empty));
        }else
        {
            if (!et_email.getText().toString().contains("@") || !et_email.getText().toString().contains("."))
            {
                Cancel = true ;
                et_email.setError(getString(R.string.EmailNotValid));
            }
        }
        Log.d("Cancel" , Cancel+"");
        if (!Cancel) {
            Log.d("Cancel" , Cancel+"HERE");
            if (Validation.isOnline(getActivity()))
            {
                mkLoaderId.setVisibility(View.VISIBLE);
                forgetFunctionApi(et_email.getText().toString() );
            }
            else
                Toast.makeText(getActivity(), "Please check your connection with internet", Toast.LENGTH_SHORT).show();
        }
    }

    private void forgetFunctionApi(String email){
        ((Authentication) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Authentication.class))
                .forget(email)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                    et_email.setText("");

                                    navController.navigate(R.id.changePasswordFragment);
                                }
                                else
                                {
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
