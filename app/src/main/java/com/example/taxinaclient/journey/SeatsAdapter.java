package com.example.taxinaclient.journey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.model.Seat;
import com.example.taxinaclient.pojo.responses.DetailsTripResponse;
import com.example.taxinaclient.utils.SharedHelpers;

import java.util.List;

public class SeatsAdapter extends RecyclerView.Adapter<SeatsAdapter.SeatViewHolder> {
    private Context context ;
    private List<Seat>Seats ;

    public SeatsAdapter(Context context, List<Seat> seats) {
        this.context = context;
        Seats = seats;
    }

    @NonNull
    @Override
    public SeatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SeatViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_seat, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SeatViewHolder holder, final int position) {
        if (Seats.get(position).getType().equals("0"))
        {
            holder.imageSeat.setImageDrawable(context.getDrawable(R.drawable.seat_hint));
        }
        else if (Seats.get(position).getType().equals("1"))
        {
            if (SharedHelpers.getKey(context , "IDUser").equals(Seats.get(position).getUser()))
            {
                holder.imageSeat.setImageDrawable(context.getDrawable(R.drawable.seat_yellow));
            }
            else
            {
                holder.imageSeat.setImageDrawable(context.getDrawable(R.drawable.seat_black));
            }

        }
        else if (Seats.get(position).getType().equals("2"))
        {
            holder.imageSeat.setImageDrawable(context.getDrawable(R.drawable.seat_yellow));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Seats.get(position).getType().equals("0") &&  !Seats.get(position).isReverse())
                {
                    Seats.get(position).setReverse(true);
                    Seats.get(position).setType("2");
                    notifyDataSetChanged();
                }
                else if (Seats.get(position).getType().equals("2") &&  Seats.get(position).isReverse())
                {
                    Seats.get(position).setReverse(false);
                    Seats.get(position).setType("0");
                    notifyDataSetChanged();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return Seats.size();
    }

    public static class SeatViewHolder extends RecyclerView.ViewHolder {
        ImageView imageSeat;
        SeatViewHolder(View itemView) {
            super(itemView);
            this.imageSeat = (ImageView) itemView.findViewById(R.id.imageSeat);
        }
    }

}
