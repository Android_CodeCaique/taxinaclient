package com.example.taxinaclient.journey;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.ui.search.SearchBuses;
import com.example.taxinaclient.utils.SharedHelpers;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class JourneyDetails extends Fragment {

    RecyclerView rv_journey;
    private NavController navController ;
    private TextView currant_loc ,to_loc ,timeTrip;
    private CircleImageView drawer ;
    private MKLoader mkLoaderId;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_journey_details, container, false);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

        currant_loc= view.findViewById(R.id.currant_loc);
        to_loc= view.findViewById(R.id.to_loc);
        timeTrip= view.findViewById(R.id.time);
        rv_journey= view.findViewById(R.id.rv_journey);


        mkLoaderId = view.findViewById(R.id.mkLoaderId);
        mkLoaderId.setVisibility(View.VISIBLE);
        currant_loc.setText(SharedHelpers.getKey(getActivity() , "StartLocation"));
        to_loc.setText(SharedHelpers.getKey(getActivity() , "EndLocation" ));
        timeTrip.setText(SharedHelpers.getKey(getActivity() , "Time" ));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rv_journey.setLayoutManager(linearLayoutManager);
        rv_journey.setHasFixedSize(true);
        JourneyDetailsRecyclerViewAdabter adabter = new JourneyDetailsRecyclerViewAdabter(getActivity() , SearchBuses.response.getData());
        rv_journey.setAdapter(adabter);

        adabter.setOnItemClickListener(new JourneyDetailsRecyclerViewAdabter.OnItemClickListener() {
            @Override
            public void onItemClick(int i) {
                SharedHelpers.putKey(getActivity() , "IDTrip" , SearchBuses.response.getData().get(i).getId());
                navController.navigate(R.id.action_journeyDetails_to_inMicroBus);
            }
        });

        drawer = view.findViewById(R.id.drawer);
        drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2Activity.drawerLayout.openDrawer(GravityCompat.START);
            }
        });


        mkLoaderId.setVisibility(View.GONE);
        if (!SharedHelpers.getKey(getActivity() , "Image").isEmpty())
            Picasso.get().load(SharedHelpers.getKey(getActivity() , "Image")).error(R.drawable.logo).into(drawer);
    }
}
