package com.example.taxinaclient.journey;

import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.responses.SearchBusesResponse;

import java.util.List;


public class JourneyDetailsRecyclerViewAdabter extends RecyclerView.Adapter<ViewHolderJourneyDetails> {

    public JourneyDetailsRecyclerViewAdabter(Context context , List<SearchBusesResponse.data> data) {
        this.context = context;
        list = data ;
    }

    private View view;
    private Context context;
    private List<SearchBusesResponse.data> list ;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int i);
    }


    @NonNull
    @Override
    public ViewHolderJourneyDetails onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.item_journy_details,parent,false);
        ViewHolderJourneyDetails viewHolderNotification = new ViewHolderJourneyDetails(view , mListener);
        return viewHolderNotification;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderJourneyDetails holder, final int position) {
        holder.tv_timeStart.setText(list.get(position).getFrom_time());
        holder.timeEnd.setText(list.get(position).getTo_time());
        holder.des.setText(list.get(position).getFromcity_name()+" - "+list.get(position).getStart_location());
        holder.bus.setText(list.get(position).getTocity_name()+" - "+list.get(position).getEnd_location());
        holder.price.setText(list.get(position).getPrice()+" "+context.getString(R.string.money));
        holder.time.setText("");
        holder.locationOfDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isGPSEnabled(context)) {
                    Log.i("Driver Id Bus", list.get(position).getUser_id());
                    Bundle bundle = new Bundle();
                    bundle.putString("driverId", list.get(position).getUser_id());
                    Navigation.findNavController(v).navigate(R.id.action_journeyDetails_to_busTracking, bundle);
                }else{
                    Toast.makeText(context, context.getString(R.string.youNeedToOpenGPSFirst), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.directionId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isGPSEnabled(context)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("startLat", list.get(position).getStart_Latitude());
                    bundle.putString("startLon", list.get(position).getStart_Longitude());

                    Navigation.findNavController(v).navigate(R.id.action_journeyDetails_to_busNavigator, bundle);
                }else{
                    Toast.makeText(context, context.getString(R.string.youNeedToOpenGPSFirst), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }
    public boolean isGPSEnabled (Context mContext){
        LocationManager locationManager = (LocationManager)
                mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
}

class ViewHolderJourneyDetails extends RecyclerView.ViewHolder{

    TextView tv_timeStart , timeEnd , des , bus , price , time;
    ImageView locationOfDriver , directionId ;
    public ViewHolderJourneyDetails(@NonNull View itemView , final JourneyDetailsRecyclerViewAdabter.OnItemClickListener listener) {
        super(itemView);
        tv_timeStart = itemView.findViewById(R.id.tv_timeStart);
        timeEnd = itemView.findViewById(R.id.timeEnd);
        des = itemView.findViewById(R.id.des);
        bus = itemView.findViewById(R.id.bus);
        price = itemView.findViewById(R.id.price);
        time = itemView.findViewById(R.id.time);
        locationOfDriver = itemView.findViewById(R.id.locationDriverImgID);
        directionId = itemView.findViewById(R.id.directionId);


        itemView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != -1) {
                        listener.onItemClick(position);
                    }
                }
            }
        });


    }
}