package com.example.taxinaclient.journey;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.TokenWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Buse;
import com.example.taxinaclient.pojo.model.Seat;
import com.example.taxinaclient.pojo.responses.DetailsTripResponse;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.SearchBusesResponse;
import com.example.taxinaclient.ui.search.SearchBuses;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class InMicroBus extends Fragment {
    private TextView StartTrip , EndTrip , Date ;
    private RecyclerView recyclerView ;
    private TextView FromTime , ToTime , StartLocation , EndLocation , Price ;
    private NavController navController ;
    private Button btn_Confirm , resetBtn;
    private MKLoader mkLoaderId ;
    private  List<Seat> Seats ;
    private CircleImageView drawer ;

    public InMicroBus() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_in_micro_bus, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

        recyclerView = view.findViewById(R.id.seats);
        GridLayoutManager linearLayoutManager = new GridLayoutManager(getActivity() , 3);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        StartLocation = view.findViewById(R.id.StartLocation);
        EndLocation = view.findViewById(R.id.EndLocation);
        FromTime = view.findViewById(R.id.tv_timeStart);
        ToTime = view.findViewById(R.id.timeEnd);
        StartTrip = view.findViewById(R.id.StartTrip);
        EndTrip = view.findViewById(R.id.EndTrip);
        Date = view.findViewById(R.id.Date);
        Price = view.findViewById(R.id.Price);
//        resetBtn = view.findViewById(R.id.resetBtnId);

        mkLoaderId = view.findViewById(R.id.mkLoaderId);

//        StartLocation.setText(SharedHelpers.getKey(getActivity() , "StartLocation"));
//        EndLocation.setText(SharedHelpers.getKey(getActivity() , "EndLocation" ));
//        Date.setText(SharedHelpers.getKey(getActivity() , "Time" ));

        btn_Confirm = view.findViewById(R.id.btn_Confirm);
        btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reverseSeats();
            }
        });

//        resetBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                reverseSeats();
//            }
//        });

        initData();


        drawer = view.findViewById(R.id.drawer);
        drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2Activity.drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        if (!SharedHelpers.getKey(getActivity() , "Image").isEmpty())
            Picasso.get().load(SharedHelpers.getKey(getActivity() , "Image")).error(R.drawable.logo).into(drawer);

    }

    private void clearSeats()
    {

    }


    private void initData(){

        mkLoaderId.setVisibility(View.VISIBLE);


        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");

        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Buse buse = (Buse) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Buse.class);

        buse.ShowSeats(SharedHelpers.getKey(getActivity() , "IDTrip") )
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    mkLoaderId.setVisibility(View.GONE);
                                    DetailsTripResponse detailsTripResponse = new Gson().fromJson(responseData, DetailsTripResponse.class);
                                    Toast.makeText(getActivity(), detailsTripResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                                    StartTrip.setText(detailsTripResponse.getData().getFromcity_name()+" - "+detailsTripResponse.getData().getStart_location());
                                    EndTrip.setText(detailsTripResponse.getData().getTocity_name()+" - "+detailsTripResponse.getData().getEnd_location());
                                    Price.setText(detailsTripResponse.getData().getPrice()+" "+getActivity().getString(R.string.money));
                                    FromTime.setText(detailsTripResponse.getData().getFrom_time());
                                    ToTime.setText(detailsTripResponse.getData().getTo_time());
                                    Log.d("SeatsCount" , detailsTripResponse.getData().getSeats().size()+"");

                                    StartLocation.setText(detailsTripResponse.getData().getStart_location());
                                    EndLocation.setText(detailsTripResponse.getData().getEnd_location());
                                    Date.setText(detailsTripResponse.getData().getDate());



                                    Seats = new ArrayList<>();

                                    for (int i = 0 ; i < detailsTripResponse.getData().getSeats().size() ; i++)
                                    {
                                        Seat seat = new Seat();
                                        seat.setType(detailsTripResponse.getData().getSeats().get(i).getIs_ride());
                                        seat.setID(detailsTripResponse.getData().getSeats().get(i).getSeat_num());
                                        seat.setUser(detailsTripResponse.getData().getSeats().get(i).getUser_id());
                                        seat.setReverse(false);
                                        Seats.add(seat);
                                    }
                                    SeatsAdapter adapter = new SeatsAdapter(getActivity() , Seats);

                                    recyclerView.setAdapter(adapter);
                                }
                                else
                                {
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void reverseSeats(){

        boolean needToReverse = false ;
        for (int i = 0 ; i < Seats.size() ; i++)
        {
            if (Seats.get(i).isReverse())
            {
                needToReverse = true ;
            }
        }

        if (needToReverse) {
            mkLoaderId.setVisibility(View.VISIBLE);

            final String Token = SharedHelpers.getKey(getActivity(), "TokenFirebase");
            Interceptor interceptor = new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer" + Token).build());
                }
            };

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.interceptors().add(interceptor);
            final Buse buse = (Buse) new Retrofit.Builder()
                    .baseUrl("http://taxiApi.codecaique.com/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(builder.build()).build()
                    .create(Buse.class);

            String SeatsTxt = "";
            for (int i = 0; i < Seats.size(); i++) {
                if (Seats.get(i).isReverse()) {
                    SeatsTxt = SeatsTxt+Seats.get(i).getID() + ",";
                }
            }
            Log.d("SeatTxt", SeatsTxt.length()+" is length "+SeatsTxt);
            if (SeatsTxt.length() > 2)
            {
                int last =SeatsTxt.length() ;
                last = last - 1 ;
                SeatsTxt = SeatsTxt.substring(0,last );
            }

            Log.d("SeatTxt", (SharedHelpers.getKey(getActivity() , "IDTrip")));
            buse.JoinTrip((SharedHelpers.getKey(getActivity() , "IDTrip") ), SeatsTxt)
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            if (response.code() == 200)
                            {
                                try {
                                    String responseData = response.body().string();

                                    GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                    if (generalResponse.getError() == 0 ) {
                                        mkLoaderId.setVisibility(View.GONE);
                                        Toast.makeText(getActivity(), generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                                       // navController.navigate(R.id.action_inMicroBus_to_journeyDetails);
                                    }
                                    else
                                    {
                                        mkLoaderId.setVisibility(View.GONE);
                                        Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                            else
                            {
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        else
        {
            Toast.makeText(getActivity(), getActivity().getString(R.string.pleaseChoose), Toast.LENGTH_SHORT).show();
        }

    }

}
