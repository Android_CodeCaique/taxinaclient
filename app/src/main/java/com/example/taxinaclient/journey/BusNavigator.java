package com.example.taxinaclient.journey;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.taxinaclient.R;
import com.example.taxinaclient.databinding.FragmentBusNavigatorBinding;
import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.GoogleDircetionsAPIResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.EncodedPolyline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.LOCATION_SERVICE;


public class BusNavigator extends Fragment implements OnMapReadyCallback {

    FragmentBusNavigatorBinding binding;
    GoogleMap map;
    LocationManager mLocationManager;
    double startLatTrip, startLonTrip , myLocationLat , myLocationLon;
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentBusNavigatorBinding.inflate(inflater);
        binding.setLifecycleOwner(this);
        binding.mapViewId.onCreate(savedInstanceState);
        MapsInitializer.initialize(this.requireActivity());
        binding.mapViewId.getMapAsync(this);
        mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000,
                    0, mLocationListener);
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity());

        assert getArguments() != null;
        startLatTrip = Double.parseDouble(getArguments().getString("startLat"));
        startLonTrip = Double.parseDouble(getArguments().getString("startLon"));

        return binding.getRoot();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(requireActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            myLocationLat = location.getLatitude();
                            myLocationLon = location.getLongitude();
                            getTimeBetweenTwoLocations(String.valueOf(startLatTrip), String.valueOf(startLonTrip), String.valueOf(myLocationLat), String.valueOf(myLocationLon));
                            drawLine(myLocationLat,myLocationLon,startLatTrip,startLonTrip,false);
                        }
                    }
                });


    }


    @Override
    public void onResume() {
        super.onResume();
        binding.mapViewId.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapViewId.onDestroy();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapViewId.onLowMemory();
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            LatLng latLng = new LatLng(location.getLatitude() , location.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,15);
            map.animateCamera(cameraUpdate);

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private void drawLine(double latFrom , double lngFrom , double latDes , double lngDes , boolean isDriver){
        final String origin = ""+latFrom+","+lngFrom;
        final String des = ""+latDes+","+lngDes;
        //Define list to get all latlng for the route
        List<LatLng> path = new ArrayList();

        //Execute Directions API request
        GeoApiContext context = new GeoApiContext().setQueryRateLimit(3).setApiKey("AIzaSyB4ski2q_cAYGl2LA4aHyjU3LALRspXZvM")
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
        DirectionsApiRequest req = DirectionsApi.getDirections(context, origin , des);
        try {
            DirectionsResult res = req.await();

            //Loop through legs and steps to get encoded polylines of each step
            if (res.routes != null && res.routes.length > 0) {
                DirectionsRoute route = res.routes[0];

                if (route.legs !=null) {
                    for(int i=0; i<route.legs.length; i++) {
                        DirectionsLeg leg = route.legs[i];
                        if (leg.steps != null) {
                            for (int j=0; j<leg.steps.length;j++){
                                DirectionsStep step = leg.steps[j];
                                if (step.steps != null && step.steps.length >0) {
                                    for (int k=0; k<step.steps.length;k++){
                                        DirectionsStep step1 = step.steps[k];
                                        EncodedPolyline points1 = step1.polyline;
                                        if (points1 != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                            for (com.google.maps.model.LatLng coord1 : coords1) {
                                                path.add(new LatLng(coord1.lat, coord1.lng));
                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline points = step.polyline;
                                    if (points != null) {
                                        //Decode polyline and add points to list of route coordinates
                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
                                        for (com.google.maps.model.LatLng coord : coords) {
                                            path.add(new LatLng(coord.lat, coord.lng));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch(Exception ex) {
            Log.e("ERROR", ex.getLocalizedMessage());
        }

        //Draw the polyline
        if (path.size() > 0) {
            PolylineOptions opts;
            if(isDriver) {
                opts = new PolylineOptions().addAll(path).color(Color.BLACK).width(5);
            }else{
                opts = new PolylineOptions().addAll(path).color(Color.BLUE).width(5);
            }
            map.addPolyline(opts);
        }

        map.getUiSettings().setZoomControlsEnabled(true);

    }
    private void getTimeBetweenTwoLocations(String latFrom , String lngFrom , String latDes, String lngDes) {

        Log.e("TimeLocation",latFrom+","+lngFrom+"//"+latDes+","+lngDes);

        binding.mkLoaderId.setVisibility(View.VISIBLE);
        ((Car) new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/maps/api/directions/")
                .build()
                .create(Car.class))
                .getDistanceAndTimeGoogleAPI(latFrom+","+lngFrom,
                        latDes+","+lngDes,
                        "AIzaSyB4ski2q_cAYGl2LA4aHyjU3LALRspXZvM","driving")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                    binding.mkLoaderId.setVisibility(View.GONE);
                                    GoogleDircetionsAPIResponse googleResponse = new Gson().fromJson(responseData, GoogleDircetionsAPIResponse.class);
                                    try {
                                        binding.tripDurationId.setText(googleResponse.getRoutes().get(0).getLegs().get(0).getDuration().getText());
                                        binding.tripDistanceId.setText(googleResponse.getRoutes().get(0).getLegs().get(0).getDistance().getText());
                                        binding.mkLoaderId.setVisibility(View.INVISIBLE);

                                    }
                                    catch (Exception e)
                                    {
                                        Toast.makeText(requireActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                        binding.mkLoaderId.setVisibility(View.INVISIBLE);

                                    }
                                }
                                else
                                {
                                    binding.mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                binding.mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            binding. mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        binding. mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

    }

}