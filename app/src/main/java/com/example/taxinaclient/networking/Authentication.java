package com.example.taxinaclient.networking;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Authentication {

    @FormUrlEncoded
    @POST("user_registration")
    Call<ResponseBody> register(
            @Field("first_name")String first_name ,
            @Field("last_name")String last_name ,
            @Field("email")String email ,
            @Field("phone")String phone ,
            @Field("password")String password ,
            @Field("gender")String gender ,
            @Field("Latitude")String Latitude ,
            @Field("Longitude")String Longitude ,
            @Field("firebase_token")String firebase_token
    );

    @FormUrlEncoded
    @POST("user_login")
    Call<ResponseBody> login(
            @Field("email")String email ,
            @Field("password") String password ,
            @Field("firebase_token") String firebase_token
    );


    @FormUrlEncoded
    @POST("social_login")
    Call<ResponseBody> loginSocial(@Field("email")String email);



    @GET("forget_password")
    Call<ResponseBody> forget(
            @Query("email")String email
    );

    @GET("check_phone")
    Call<ResponseBody> checkPhone(@Query("phone")String phone);

    @FormUrlEncoded
    @POST("Reset_password")
    Call<ResponseBody> resetPassword(
            @Field("code") String code,
            @Field("password") String password
    );

    @GET("show_user_ByID")
    Call<ResponseBody> showProfile();

    @GET("country/show_country")
    Call<ResponseBody> getCountries(@Header("Authorization") String token);


    @GET("add_user_country")
    Call<ResponseBody> addUserCountry(@Header("Authorization") String token , @Query("country_id") int country_id);



    @FormUrlEncoded
    @POST("update_profile")
    Call<ResponseBody>updateProfile(@Field("email") String email ,
                                   @Field("phone") String phone );

    @FormUrlEncoded
    @POST("change_passward")
    Call<ResponseBody>changePasswordProfile(@Field("password") String password );

}
