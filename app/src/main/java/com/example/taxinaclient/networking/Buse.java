package com.example.taxinaclient.networking;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Buse {

    @GET("search_trip")
    Call<ResponseBody>SearchBuses(@Query("date") String date ,
                                  @Query("from_areaId") String from_areaId ,
                                  @Query("to_areaId") String to_areaId );

    @GET("show_trip_seats")
    Call<ResponseBody>ShowSeats(@Query("trip_id") String trip_id);

    @GET("show_area_city")
    Call<ResponseBody>ShowAreas( @Query("country_id") String CountryId );

    @GET("join_trip")
    Call<ResponseBody>JoinTrip(@Query("trip_id") String trip_id  ,
                               @Query("seat_id") String seat_id );

}
