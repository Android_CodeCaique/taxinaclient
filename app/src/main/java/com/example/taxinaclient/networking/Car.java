package com.example.taxinaclient.networking;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Car {


    @GET("json")
    Call<ResponseBody> getDistanceAndTimeGoogleAPI(@Query("origin") String origin,
                                            @Query("destination") String destination,
                                            @Query("key") String key,
                                            @Query("mode") String mode) ;


    @GET("show_bakat")
    Call<ResponseBody>getCars(@Query("distance") String distance);

    @GET("check_promoCode")
    Call<ResponseBody>checkPromoCode(@Query("promo_code") String promo_code);

    @GET("show_userFavorite_place")
    Call<ResponseBody>userFavoritePlace(@Header("Authorization") String token);

    @GET("delete_favorite")
    Call<ResponseBody>deleteFavoritePlace(@Header("Authorization") String token,@Query("id") int id);

    @FormUrlEncoded
    @POST("add_favorite_place")
    Call<ResponseBody>addFavoritePlace(@Header("Authorization") String token,
                                       @Field("name") String name,
                                       @Field("Latitude") String Latitude,
                                       @Field("Longitude") String Longitude,@Field("address") String address);

    @FormUrlEncoded
    @POST("make_trip")
    Call<ResponseBody>makeTrip(@Field("start_lat") String start_lat ,
                               @Field("start_long") String start_long ,
                               @Field("start_location") String start_location ,
                               @Field("end_lat") String end_lat ,
                               @Field("end_long") String end_long ,
                               @Field("end_location") String end_location ,
                               @Field("distance") String distance ,
                               @Field("promo_code") String promo_code ,
                               @Field("note") String note ,
                               @Field("type") String type ,
                               @Field("date") String date ,
                               @Field("time") String time ,
                               @Field("gender") String  gender ,
                               @Field("baka_id") String baka_id ,
                               @Field("price") String price );

    @GET("cancel_trip")
    Call<ResponseBody>cancelTrip(@Query("trip_id") String trip_id);

    @GET("user_request_cancel_trip")
    Call<ResponseBody>UserRequestCancelTrip(@Query("trip_id") String trip_id);

    @GET("user_accept_cancel_trip")
    Call<ResponseBody>UserAcceptCancelTrip(@Query("trip_id") String trip_id,
                                           @Query("accept")String accept);

    @GET("show_driver_trip")
    Call<ResponseBody>showDriverTrip(@Query("trip_id") String trip_id);


    @GET("family/search_phone")
    Call<ResponseBody> searchPhoneFamily(@Header("Authorization") String token,@Query("phone") String phone);

    @FormUrlEncoded
    @POST("family/send_familyRequest")
    Call<ResponseBody> sendFamilyRequest(@Header("Authorization") String token,@Field("user_id") int user_id); // Base Response

    @GET("family/show_familyMembers_accept")
    Call<ResponseBody> ShowFamilyMembersAccepted(@Header("Authorization") String token);

    @GET("family/show_familyMembers_wait")
    Call<ResponseBody> ShowFamilyMembersWaited(@Header("Authorization") String token );
    
    @FormUrlEncoded
    @POST("family/accept_request")
    Call<ResponseBody> familyAcceptRequest(@Header("Authorization") String token,@Field("request_id") int request_id); // Base Response
}
