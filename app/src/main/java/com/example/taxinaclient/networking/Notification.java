package com.example.taxinaclient.networking;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Notification {

    @GET("my_notification")
    Call<ResponseBody>getNotification();
}
