package com.example.taxinaclient.networking;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Ride {

    @GET("User_complete_trip")
    Call<ResponseBody>CompleteTrip();

    @GET("User_upcoming_trip")
    Call<ResponseBody>UpcomingTrip();

    @GET("User_waiting_trip")
    Call<ResponseBody>WaitingTrip();

    @GET("User_canceled_trip")
    Call<ResponseBody>CanceledTrip();

    @GET("show_trip_ByID")
    Call<ResponseBody>ShowTripById(@Query("trip_id") String trip_id ,
                                   @Query("type") String type );

    @GET("rate_trip")
    Call<ResponseBody>RateTripById(@Query("trip_id") String trip_id ,
                                     @Query("rate") String rate ,
                                     @Query("type") String type);

}
