package com.example.taxinaclient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.example.taxinaclient.utils.LanguageHelper;
import com.example.taxinaclient.utils.SharedHelpers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity {

    NavController nav ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nav = Navigation.findNavController(this, R.id.nav_host_fragment);
        SharedHelpers.putKey(this , "OPEN" , "OPEN");
        final String Token = SharedHelpers.getKey(this, "TokenFirebase");
        Log.d("TOKKKKKENN" , Token);
        SharedHelpers.putKey(this , "LANG" ,"ar");
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getBaseContext().getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.i("KeyHashExce:", e.getMessage());

        } catch (NoSuchAlgorithmException e) {
            Log.i("KeyHashExce:", e.getMessage());

        }
        LanguageHelper.ChangeLang(getResources() , "ar");
    }

    @Override
    public void onBackPressed() {
        if(nav.getCurrentDestination().getLabel().equals("fragment_sign_up") || nav.getCurrentDestination().getLabel().equals("fragment_user_country"))
        {
            //            Toast.makeText(this,"hhhh",Toast.LENGTH_SHORT).show()
        }else
            super.onBackPressed();
    }
}
