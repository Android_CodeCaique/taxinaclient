package com.example.taxinaclient.notification11_1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Notification;
import com.example.taxinaclient.pojo.responses.NotificationResponse;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class NotificationRecyclerViewAdabter extends RecyclerView.Adapter<NotificationRecyclerViewAdabter.ViewHolderNotification> {
    private List<NotificationResponse.data>list ;
    private Context context ;
    private View view;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int i);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }


    public NotificationRecyclerViewAdabter(List<NotificationResponse.data> list, Context context) {
        this.list = list;
        this.context = context;
    }

    class ViewHolderNotification extends RecyclerView.ViewHolder{

        CardView notifications_card;
        TextView notifications_name;
        TextView notifications_details;
        TextView notification_Date;
        CircleImageView notifications_icon;
        public ViewHolderNotification(@NonNull View itemView , final OnItemClickListener listener) {
            super(itemView);
            notifications_name=itemView.findViewById(R.id.notification_title);
            notifications_details=itemView.findViewById(R.id.notification_details);
            notification_Date=itemView.findViewById(R.id.notification_Date);
            notifications_icon=itemView.findViewById(R.id.notification_icon);

            itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != -1) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }

    @NonNull
    @Override
    public ViewHolderNotification onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification,parent,false);
        ViewHolderNotification viewHolderNotification = new ViewHolderNotification(view , mListener);
        return viewHolderNotification;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderNotification holder, int position) {
       holder.notifications_name.setText(list.get(position).getTitle());
       holder.notifications_details.setText(list.get(position).getBody());
       holder.notification_Date.setText(list.get(position).getCreated_at());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(NotificationResponse.data item, int position) {
        list.add(position, item);
        notifyItemInserted(position);
    }

    public List<NotificationResponse.data> getData() {
        return list;
    }
}


