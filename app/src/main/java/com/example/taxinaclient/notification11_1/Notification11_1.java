package com.example.taxinaclient.notification11_1;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.networking.Notification;
import com.example.taxinaclient.networking.Ride;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.NotificationResponse;
import com.example.taxinaclient.pojo.responses.SpecialTripResponse;
import com.example.taxinaclient.pojo.responses.TripReposne;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class Notification11_1 extends Fragment {

    private RecyclerView rv_notification;
    private CoordinatorLayout constrain_notification;
    private NotificationRecyclerViewAdabter notificationRecyclerViewAdabter;
    private List<NotificationResponse.data>list ;
    private MKLoader mkLoaderId ;
    private NavController navController ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_notification11_1, container, false);

        constrain_notification=view.findViewById(R.id.constrain_notification);
        rv_notification = view.findViewById(R.id.rv_notification);

        LinearLayoutManager layoutManager =new LinearLayoutManager(view.getContext(),
                LinearLayoutManager.VERTICAL,false);
        rv_notification.setLayoutManager(layoutManager);

        mkLoaderId = view.findViewById(R.id.mkLoaderId);
        list = new ArrayList<>();

        navController= Navigation.findNavController(getActivity() , R.id.nav_host_fragment);

        populateRecyclerView(view);

        return view;
    }

    String TAG = "Notifications";

    private void populateRecyclerView(final View v) {
        mkLoaderId.setVisibility(View.VISIBLE);

        final String Token = SharedHelpers.getKey(getActivity() , "TokenFirebase");
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("Authorization", "bearer"+Token).build());
            }
        };

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        final Notification buse = (Notification) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build()).build()
                .create(Notification.class);

        buse.getNotification().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() == 200)
                {
                    try {
                        String responseData = response.body().string();

                        GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                        if (generalResponse.getError() == 0) {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity() , generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();

                            final NotificationResponse notificationResponse = new Gson().fromJson(responseData, NotificationResponse.class);
                            notificationRecyclerViewAdabter = new NotificationRecyclerViewAdabter(notificationResponse.getData() , getActivity());

                            notificationRecyclerViewAdabter.setOnItemClickListener(new NotificationRecyclerViewAdabter.OnItemClickListener() {
                                @Override
                                public void onItemClick(int i) {
                                    String id = notificationResponse.getData().get(i).getRedirect_id();
                                    String status = notificationResponse.getData().get(i).getStatus();
                                    String type = notificationResponse.getData().get(i).getType();

                                    Log.e(TAG, "onItemClick: " + id );

                                    navController= Navigation.findNavController(getActivity() , R.id.nav_host_fragment);

                                    Log.d("DATA" , id+" : "+status+" : "+type);
                                    if(status != null)
                                    {
                                        Log.d("ERROE" , "Notification is not null"+status);
                                        if (status.equals("1"))
                                        {
                                            // Open Graph  ....

                                            SharedHelpers.putKey(getActivity() , "OPENGRAPH" , "1");
                                            SharedHelpers.putKey(getActivity() , "TripID" , id);
                                            navController.navigate(R.id.tracking);
                                        }
                                        else if (status.equals("2"))
                                        {
                                            // Details Trip
                                            SharedHelpers.putKey(getActivity() , "IdTripShow" , id);
                                            SharedHelpers.putKey(getActivity() , "TypeTripShow" , type);
                                            navController.navigate(R.id.thank_You);
                                        }
                                        else if (status.equals("3"))
                                        {
                                            // Details Trip
                                            SharedHelpers.putKey(getActivity() , "IdTripShow" , id);
                                            SharedHelpers.putKey(getActivity() , "TypeTripShow" , type);
                                            navController.navigate(R.id.thank_You);
                                        }
                                    }

                                    else
                                    {
                                        Log.d("ERROE" , "Notification is null");
                                    }
                                }
                            });
                            rv_notification.setAdapter(notificationRecyclerViewAdabter);
                            enableSwipeToDeleteAndUndo(v);
                        }
                        else
                        {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                     mkLoaderId.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                 mkLoaderId.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void enableSwipeToDeleteAndUndo(View view) {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(view.getContext()) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {


                final int position = viewHolder.getAdapterPosition();
                final NotificationResponse.data item = notificationRecyclerViewAdabter.getData().get(position);

                notificationRecyclerViewAdabter.removeItem(position);


                Snackbar snackbar = Snackbar
                        .make(constrain_notification, "Item was removed from the list.", Snackbar.LENGTH_LONG);
                snackbar.setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        notificationRecyclerViewAdabter.restoreItem(item, position);
                        rv_notification.scrollToPosition(position);
                    }
                });

                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();

            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(rv_notification);
    }
}
