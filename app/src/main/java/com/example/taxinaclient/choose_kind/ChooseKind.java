package com.example.taxinaclient.choose_kind;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.taxinaclient.Main2Activity;
import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.model.Kinds;
import com.example.taxinaclient.utils.SharedHelpers;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseKind extends Fragment {

    private RecyclerView types ;
    private  NavController navController ;
    private List<Kinds>kinds ;
    private CircleImageView drawer ;
    public ChooseKind() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_kind, container, false);

    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

         kinds = new ArrayList<>();

         Kinds kind = new Kinds();
         kind.setId(1);
         kind.setImage(R.drawable.car);
         kind.setName(getString(R.string.car));
         kinds.add(kind);

        Kinds kind1 = new Kinds();
        kind1.setId(2);
        kind1.setImage(R.drawable.bus);
        kind1.setName(getString(R.string.bus));
        kinds.add(kind1);


        types = view.findViewById(R.id.types);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity() , 2);
        types.setLayoutManager(gridLayoutManager);

        KindsAdapter adapter = new KindsAdapter(getActivity() , kinds);
        types.setAdapter(adapter);
        adapter.setOnItemClickListener(new KindsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int i) {
                if (i == 0)
                {
                    navController.navigate(R.id.action_chooseKind_to_enterDestination);
                }
                else  if (i == 1)
                {
                    navController.navigate(R.id.action_chooseKind_to_searchBuses);
                }
            }
        });

        drawer = view.findViewById(R.id.drawer);
        drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2Activity.drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        if (!SharedHelpers.getKey(getActivity() , "Image").isEmpty())
            Picasso.get().load(SharedHelpers.getKey(getActivity() , "Image")).error(R.drawable.logo).into(drawer);

//        ImageView bus= view.findViewById(R.id.bus);
//        bus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                navController.navigate(R.id.action_chooseKind_to_searchBuses);
//
//            }
//        });
//        TextView txtBus = view.findViewById(R.id.txtBus);
//        txtBus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                navController.navigate(R.id.action_chooseKind_to_searchBuses);
//
//            }
//        });
//
//        ImageView car= view.findViewById(R.id.car);
//        car.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                navController.navigate(R.id.action_chooseKind_to_enterDestination);
//            }
//        });
//        TextView txtCar = view.findViewById(R.id.txtCar);
//        txtCar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                navController.navigate(R.id.action_chooseKind_to_enterDestination);
//            }
//        });
    }
}
