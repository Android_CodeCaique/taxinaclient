package com.example.taxinaclient.choose_kind;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxinaclient.R;
import com.example.taxinaclient.pojo.model.Kinds;
import com.example.taxinaclient.ui.search.AreasAdapter;

import java.util.List;

public class KindsAdapter extends RecyclerView.Adapter<KindsAdapter.KindsViewModel> {

    private Context context ;
    private List<Kinds>Kinds ;
    private OnItemClickListener mListener;

    public KindsAdapter(Context context, List<Kinds> kinds) {
        this.context = context;
        Kinds = kinds;
    }

    @NonNull
    @Override
    public KindsViewModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new KindsViewModel(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kind, parent, false) , mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull KindsViewModel holder, int position) {
   holder.itemImage.setImageDrawable(context.getDrawable(Kinds.get(position).getImage()));
        holder.itemName.setText(Kinds.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return Kinds.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int i);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    public static class KindsViewModel extends RecyclerView.ViewHolder {
        TextView itemName ;
        ImageView itemImage ;
        KindsViewModel(View itemView, final OnItemClickListener listener ) {
            super(itemView);
            itemName = itemView.findViewById(R.id.itemName);
            itemImage = itemView.findViewById(R.id.itemImage);


            itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != -1) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }
}
