package com.example.taxinaclient;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.example.taxinaclient.databinding.FragmentBusTrackingBinding;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

public class BusTracking extends Fragment implements OnMapReadyCallback {

    FragmentBusTrackingBinding binding;
    GoogleMap map;
    Marker marker;
    String driverId;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentBusTrackingBinding.inflate(inflater);
        binding.setLifecycleOwner(this);

        binding.mapViewId.onCreate(savedInstanceState);
        MapsInitializer.initialize(this.requireActivity());
        binding.mapViewId.getMapAsync(this);

        assert getArguments() != null;
        driverId = getArguments().getString("driverId");

        Log.i("driverIdTracking",driverId);
        return binding.getRoot();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
           return;
        }
        map.setMyLocationEnabled(true);
        final DatabaseReference databaseReference =  FirebaseDatabase.getInstance().getReference().child("driverLocation").child(driverId);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean allowRemoveMarker = false;
                if (dataSnapshot.exists())
                {
//                    for (DataSnapshot snapshot : dataSnapshot.getChildren())
//                    {

                        double lat =  Double.parseDouble(dataSnapshot.child("lat").getValue().toString());
                        double lng = Double.parseDouble(dataSnapshot.child("lon").getValue().toString());


                        LatLng driverLatLng = new LatLng(lat,lng);

                        if(marker == null) {
                            marker = map.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_test_one)));
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(driverLatLng,15));

                        }
                        else
                            animateMarker(marker,driverLatLng,false);


//                        try {
//                            googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).title("Driver"));
////
////                            if (allowRemoveMarker) {
////                                marker.remove();
////                            }
////
////                            allowRemoveMarker = true;
////                            Marker marker2 = googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//
////                                googleMap.clear();
////                                googleMap.addMarker(new MarkerOptions().position(driverLatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//
//                        }catch (Exception e)
//                        {
//                            Toast.makeText(getActivity(), "Exception Marker\n"+e.getMessage(), Toast.LENGTH_SHORT).show();
//                        }

//                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onResume() {
        binding.mapViewId.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapViewId.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapViewId.onLowMemory();
    }

    public void animateMarker(final com.google.android.gms.maps.model.Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler;
        handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }
}