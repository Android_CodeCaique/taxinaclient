package com.example.taxinaclient;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.taxinaclient.databinding.FragmentUserCountryBinding;
import com.example.taxinaclient.networking.Authentication;
import com.example.taxinaclient.pojo.responses.AddUserCountryResponse;
import com.example.taxinaclient.pojo.responses.AllCountryResponse;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.LoginResponse;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UserCountryFragment extends Fragment {

    FragmentUserCountryBinding binding;
//    ArrayList<String> arrayList;
    int countryId ;


    ArrayList<CountriesSpinnerModel> countriesListModel;
    ArrayList<String> countriesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_user_country, container, false);
        binding = FragmentUserCountryBinding.inflate(inflater);
//        arrayList = new ArrayList<>();
        countriesListModel = new ArrayList<>();
        countriesList = new ArrayList<>();
        getCountries();
        binding.materialSpinnerId.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                countryId = position+1;

                for(int i = 0 ; i < countriesListModel.size() ; i++){
                    if(countriesListModel.get(i).getName() == item.toString()){
                        countryId = countriesListModel.get(i).getId();

                    }
                }

            }
        });

        binding.btnVerifyCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCountry(v);
            }
        });
        return binding.getRoot();
    }

    private void sendCountry(final View view) {
        if (countryId == 0)
            Toast.makeText(getActivity(), "Please select country", Toast.LENGTH_SHORT).show();
        else {
            binding.mkLoaderId.setVisibility(View.VISIBLE);
            ((Authentication) new Retrofit.Builder()
                    .baseUrl("http://taxiApi.codecaique.com/api/")
                    .build()
                    .create(Authentication.class))
                    .addUserCountry("bearer " + SharedHelpers.getKey(requireContext(), "TokenFirebaseCountry"), countryId)
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            if (response.code() == 200) {
                                try {
                                    String responseData = response.body().string();

                                    GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                    if (generalResponse.getError() == 0) {
                                        binding.mkLoaderId.setVisibility(View.GONE);
                                        AddUserCountryResponse loginResponse = new Gson().fromJson(responseData, AddUserCountryResponse.class);
                                        Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                        SharedHelpers.putKey(getActivity() , "TokenFirebase" , SharedHelpers.getKey(requireContext(), "TokenFirebaseCountry"));
                                    SharedHelpers.putKey(getActivity() , "IDUser" , loginResponse.getData().getId()+"");
                                    SharedHelpers.putKey(getActivity() , "Image" , loginResponse.getData().getImage());
                                    SharedHelpers.putKey(getActivity() , "Name" , loginResponse.getData().getFirst_name()+" "+loginResponse.getData().getLast_name());
                                    SharedHelpers.putKey(getActivity() , "Email" , loginResponse.getData().getEmail());
                                    SharedHelpers.putKey(getActivity() , "CountryIdLogin" , String.valueOf(countryId));
                                        SharedHelpers.putKey(getActivity() , "OPEN" , "OPEN");

                                    if ( SharedHelpers.getKey(getActivity() , "Intro" ).equals("1"))
                                    {
                                        Intent intent = new Intent(getActivity() , Main2Activity.class);
                                        startActivity(intent);
                                        getActivity().finish();
                                    }
                                    else
                                    {
                                        Navigation.findNavController(view).navigate(R.id.action_userCountryFragment_to_intro1);
                                    }

                                    } else {
                                        binding.mkLoaderId.setVisibility(View.GONE);
                                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    binding.mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                binding.mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            binding.mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }



    private void getCountries() {
        binding.mkLoaderId.setVisibility(View.VISIBLE);
        ((Authentication) new Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .build()
                .create(Authentication.class))
                .getCountries("bearer "+ SharedHelpers.getKey(requireContext(),"TokenFirebaseCountry"))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.code() == 200)
                        {
                            try {
                                String responseData = response.body().string();

                                GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                if (generalResponse.getError() == 0) {
                                   binding.mkLoaderId.setVisibility(View.GONE);
                                    AllCountryResponse loginResponse = new Gson().fromJson(responseData, AllCountryResponse.class);
                                    Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                                    arrayList.add(loginResponse.getData());
                                    Log.i("Response",loginResponse.getData().toString());
                                    for (int i = 0 ; i <loginResponse.getData().size() ; i++) {
//                                        arrayList.add(loginResponse.getData().get(i).getName());
                                        Log.i("Name",loginResponse.getData().get(i).getName());

                                        countriesListModel.add(new CountriesSpinnerModel(loginResponse.getData().get(i).getName(),loginResponse.getData().get(i).getId()));
                                        countriesList.add(loginResponse.getData().get(i).getName());
                                    }
                                    binding.materialSpinnerId.setItems(countriesList);

                                }
                                else
                                {
                                    binding.mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "عفوا بيانات الدخول خاطئه من فضلك حاول مره اخري", Toast.LENGTH_SHORT).show();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                binding.mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            binding.mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        binding.mkLoaderId.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}