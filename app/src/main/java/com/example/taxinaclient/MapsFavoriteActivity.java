package com.example.taxinaclient;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.taxinaclient.networking.Car;
import com.example.taxinaclient.pojo.responses.BaseResponse;
import com.example.taxinaclient.pojo.responses.GeneralResponse;
import com.example.taxinaclient.pojo.responses.MyFavoritesResponse;
import com.example.taxinaclient.ui.location.FavoriteLocRecyclerViewAdabter;
import com.example.taxinaclient.utils.SharedHelpers;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MapsFavoriteActivity extends FragmentActivity implements OnMapReadyCallback ,GoogleMap.OnMarkerDragListener{

    private GoogleMap mMap;
    private LatLng sydney = new LatLng(29.9426994,31.3939916);
    private MapFragment mapFragment;
    private String adress;
    LatLng selectedlatLng;
    private ImageView marker;
    boolean dropOff = false;
//    DateTimeAddressInform pickUp;
    private EditText placeName;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    AutocompleteSupportFragment autocompleteFragment;
    Button doneButton;
    MKLoader mkLoaderId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_favorite);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        setupAutoCompleteFragment();
        configureCameraIdle();
        marker=findViewById(R.id.imgLocationPinUp);
        placeName = findViewById(R.id.placeNameID);
        mkLoaderId = findViewById(R.id.mkLoaderId2);
//        Toolbar toolbar=findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MapsFavoriteActivity.this.onBackPressed();
//            }
//        });
//        toolbar.setTitle(getIntent().getStringExtra("title"));
        doneButton=findViewById(R.id.done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Done();
            }
        });

    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    private void setupAutoCompleteFragment() {
        Places.initialize(this, "AIzaSyB4ski2q_cAYGl2LA4aHyjU3LALRspXZvM");
        mapFragment.getMapAsync(MapsFavoriteActivity.this);

        autocompleteFragment =
                (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocompleteDes_fragmentt);
        autocompleteFragment.setPlaceFields(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.LAT_LNG , Place.Field.ADDRESS)).setCountry(getUserCountry(this));

        autocompleteFragment.setHint(getResources().getString(R.string.searchForPlace));
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {


            @Override
            public void onPlaceSelected(@NonNull com.google.android.libraries.places.api.model.Place place) {
                sydney = place.getLatLng();
            }

            @Override
            public void onError(Status status) {
                Log.e("Error", status.getStatusMessage());
            }
        });
    }

    private void configureCameraIdle() {
        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                marker.setVisibility(View.GONE);
                selectedlatLng = mMap.getCameraPosition().target;
                Geocoder geocoder = new Geocoder(MapsFavoriteActivity.this);

                try {
                    List<Address> addressList = geocoder.getFromLocation(selectedlatLng.latitude, selectedlatLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        adress = addressList.get(0).getAddressLine(0);
                        String country = addressList.get(0).getCountryName();
                        if (!adress.isEmpty() && !country.isEmpty()){
                            Toast.makeText(MapsFavoriteActivity.this, adress , Toast.LENGTH_SHORT).show();
                            mMap.addMarker(new MarkerOptions()
                                    .position(mMap.getCameraPosition().target)
                                    .title(adress)
                                    .draggable(false)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_dwon)));

                            doneButton.setEnabled(true);

                        }


                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(dropOff)
        {
            mMap.addMarker(new MarkerOptions()
                    .position(sydney)
                    .title("Test")
                    .draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_dwon)));

            doneButton.setEnabled(true);
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
//        mMap.addMarker(new MarkerOptions()
//                .position(sydney)
//                .title(title!=null?title:"")
//                .draggable(false)
////                .anchor(0,0)
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));


//        mMap.setOnMarkerDragListener(this);
        mMap.setOnCameraIdleListener(onCameraIdleListener);
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                mMap.clear();
                marker.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        CameraUpdate center= CameraUpdateFactory.newLatLng(marker.getPosition());
        CameraUpdate zoom= CameraUpdateFactory.zoomTo(15);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mMap != null) {
            mMap.clear();
        }
    }

    void Done(){
//        Intent data = new Intent();
//        data.putExtra("address",adress);
//        data.putExtra("lat",String.valueOf(selectedlatLng.latitude));
//        data.putExtra("lon",String.valueOf(selectedlatLng.longitude));
//        setResult(RESULT_OK,data);
//        finish();

        if(placeName.getText().toString().isEmpty())
            Toast.makeText(this, getResources().getString(R.string.pleaseEnterPlaceName), Toast.LENGTH_SHORT).show();
        else
        {
            mkLoaderId.setVisibility(View.VISIBLE);

            ((Car) new Retrofit.Builder()
                    .baseUrl("http://taxiApi.codecaique.com/api/")
                    .build()
                    .create(Car.class))
                    .addFavoritePlace("bearer "+ SharedHelpers.getKey(this,"TokenFirebase"),placeName.getText().toString(),String.valueOf(selectedlatLng.latitude),String.valueOf(selectedlatLng.longitude),adress)
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            if (response.code() == 200)
                            {
                                try {
                                    String responseData = response.body().string();

                                    GeneralResponse generalResponse = new Gson().fromJson(responseData, GeneralResponse.class);
                                    if (generalResponse.getError() == 0) {
                                        mkLoaderId.setVisibility(View.GONE);
                                        BaseResponse myBaseResponse = new Gson().fromJson(responseData, BaseResponse.class);
                                        Toast.makeText(MapsFavoriteActivity.this, myBaseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                        Intent data = new Intent();
                                        data.putExtra("AllDoneMap","true");
                                        setResult(RESULT_OK,data);
                                        finish();
                                    }
                                    else
                                    {
                                        mkLoaderId.setVisibility(View.GONE);
                                        Toast.makeText(MapsFavoriteActivity.this, "Error "+ generalResponse.getMessage_en(), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    mkLoaderId.setVisibility(View.GONE);
                                    Toast.makeText(MapsFavoriteActivity.this, "Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                            else
                            {
                                mkLoaderId.setVisibility(View.GONE);
                                Toast.makeText(MapsFavoriteActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            mkLoaderId.setVisibility(View.GONE);
                            Toast.makeText(MapsFavoriteActivity.this, "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        }

    public String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            }
            else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        }
        catch (Exception e) {
            Toast.makeText(context, "Exception "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return null;
    }
    }
