package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

public class TripReposne {

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message_en;

    @SerializedName("data")
    private data data ;

    public TripReposne.data getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage_en() {
        return message_en;
    }

    public void setMessage_en(String message_en) {
        this.message_en = message_en;
    }

    public void setData(TripReposne.data data) {
        this.data = data;
    }

    public class data{
        @SerializedName("id")
        private int id ;

        @SerializedName("driver_id")
        private String driver_id ;

        @SerializedName("car_name")
        private String car_name ;

        @SerializedName("first_name")
        private String first_name ;


        @SerializedName("last_name")
        private String last_name ;

        @SerializedName("image")
        private String image ;

        @SerializedName("state")
        private int state ;

        @SerializedName("start_lat")
        private String start_lat ;

        @SerializedName("start_long")
        private String start_long ;

        @SerializedName("start_location")
        private String start_location ;

        @SerializedName("end_lat")
        private String end_lat ;

        @SerializedName("end_long")
        private String end_long ;

        @SerializedName("end_location")
        private String end_location ;

        @SerializedName("distance")
        private String distance ;

        @SerializedName("promo_code")
        private String promo_code ;

        @SerializedName("price")
        private String price ;

        @SerializedName("note")
        private String note ;

        @SerializedName("rate")
        private String rate ;

        @SerializedName("status")
        private String status ;

        @SerializedName("payment_method")
        private String payment_method ;

        @SerializedName("type")
        private String type ;

        @SerializedName("date")
        private String date ;

        @SerializedName("time")
        private String time ;

        @SerializedName("created_at")
        private String created_at ;

        @SerializedName("pick_time")
        private String pick_time ;

        @SerializedName("code")
        private String code ;

        @SerializedName("plate")
        private String plate ;

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public String getCar_name() {
            return car_name;
        }

        public String getPlate() {
            return plate;
        }

        public void setPlate(String plate) {
            this.plate = plate;
        }

        public void setCar_name(String car_name) {
            this.car_name = car_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDriver_id() {
            return driver_id;
        }

        public void setDriver_id(String driver_id) {
            this.driver_id = driver_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getStart_lat() {
            return start_lat;
        }

        public void setStart_lat(String start_lat) {
            this.start_lat = start_lat;
        }

        public String getStart_long() {
            return start_long;
        }

        public void setStart_long(String start_long) {
            this.start_long = start_long;
        }

        public String getStart_location() {
            return start_location;
        }

        public void setStart_location(String start_location) {
            this.start_location = start_location;
        }

        public String getEnd_lat() {
            return end_lat;
        }

        public void setEnd_lat(String end_lat) {
            this.end_lat = end_lat;
        }

        public String getEnd_long() {
            return end_long;
        }

        public void setEnd_long(String end_long) {
            this.end_long = end_long;
        }

        public String getEnd_location() {
            return end_location;
        }

        public void setEnd_location(String end_location) {
            this.end_location = end_location;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getPromo_code() {
            return promo_code;
        }

        public void setPromo_code(String promo_code) {
            this.promo_code = promo_code;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public void setPayment_method(String payment_method) {
            this.payment_method = payment_method;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getPick_time() {
            return pick_time;
        }

        public void setPick_time(String pick_time) {
            this.pick_time = pick_time;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }

}
