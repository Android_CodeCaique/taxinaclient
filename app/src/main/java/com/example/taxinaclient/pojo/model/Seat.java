package com.example.taxinaclient.pojo.model;

public class Seat {
    private String Type ;
    private String ID ;
    private String User ;
    private boolean isReverse ;

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public boolean isReverse() {
        return isReverse;
    }

    public void setReverse(boolean reverse) {
        isReverse = reverse;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}
