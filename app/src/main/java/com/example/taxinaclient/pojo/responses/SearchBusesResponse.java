package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchBusesResponse {

    @SerializedName("error")
    private int error ;

    @SerializedName("message")
    private String message_en ;


    @SerializedName("data")
    private List<data> data ;

    public String getMessage_en() {
        return message_en;
    }

    public void setMessage_en(String message_en) {
        this.message_en = message_en;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public List<SearchBusesResponse.data> getData() {
        return data;
    }

    public void setData(List<SearchBusesResponse.data> data) {
        this.data = data;
    }

    public class data{


        @SerializedName("id")
        private String id ;

        @SerializedName("from_area")
        private String start_location ;

        @SerializedName("start_Latitude")
        private String start_Latitude ;

        @SerializedName("start_Longitude")
        private String start_Longitude ;

        @SerializedName("to_area")
        private String end_location ;

        @SerializedName("end_Latitude")
        private String end_Latitude ;

        @SerializedName("end_Longitude")
        private String end_Longitude ;

        @SerializedName("seat_count")
        private String seat_count ;

        @SerializedName("date")
        private String date ;

        @SerializedName("from_time")
        private String from_time ;

        @SerializedName("to_time")
        private String to_time ;

        @SerializedName("price")
        private String price ;

        @SerializedName("user_id")
        private String user_id ;

        @SerializedName("distance")
        private String distance ;

        @SerializedName("state")
        private String state ;

        @SerializedName("fromcity_name")
        private String fromcity_name ;

        @SerializedName("tocity_name")
        private String tocity_name ;

        public String getFromcity_name() {
            return fromcity_name;
        }

        public void setFromcity_name(String fromcity_name) {
            this.fromcity_name = fromcity_name;
        }

        public String getTocity_name() {
            return tocity_name;
        }

        public void setTocity_name(String tocity_name) {
            this.tocity_name = tocity_name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStart_location() {
            return start_location;
        }

        public void setStart_location(String start_location) {
            this.start_location = start_location;
        }

        public String getStart_Latitude() {
            return start_Latitude;
        }

        public void setStart_Latitude(String start_Latitude) {
            this.start_Latitude = start_Latitude;
        }

        public String getStart_Longitude() {
            return start_Longitude;
        }

        public void setStart_Longitude(String start_Longitude) {
            this.start_Longitude = start_Longitude;
        }

        public String getEnd_location() {
            return end_location;
        }

        public void setEnd_location(String end_location) {
            this.end_location = end_location;
        }

        public String getEnd_Latitude() {
            return end_Latitude;
        }

        public void setEnd_Latitude(String end_Latitude) {
            this.end_Latitude = end_Latitude;
        }

        public String getEnd_Longitude() {
            return end_Longitude;
        }

        public void setEnd_Longitude(String end_Longitude) {
            this.end_Longitude = end_Longitude;
        }

        public String getSeat_count() {
            return seat_count;
        }

        public void setSeat_count(String seat_count) {
            this.seat_count = seat_count;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getFrom_time() {
            return from_time;
        }

        public void setFrom_time(String from_time) {
            this.from_time = from_time;
        }

        public String getTo_time() {
            return to_time;
        }

        public void setTo_time(String to_time) {
            this.to_time = to_time;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }
}
