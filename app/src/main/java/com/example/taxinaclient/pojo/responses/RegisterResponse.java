package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

    @SerializedName("token")
    private String token ;

    @SerializedName("error")
    private int error ;

    @SerializedName("message")
    private String message_en ;



    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage_en() {
        return message_en;
    }

    public void setMessage_en(String message_en) {
        this.message_en = message_en;
    }


}
