package com.example.taxinaclient.pojo.responses;

public class AddUserCountryResponse {
    /**
     * data : {"id":2,"first_name":"heba","last_name":"muhammad","email":"hebamuhammed22@gmail.com","phone":"01060945044","code":"4abccc","gender":"female","image":null,"Latitude":"0","Longitude":"0","rate":"0","role":"2","is_online":"0","is_driver":"0","is_busy":"0","wallet":"0","target":"0","token":null,"firebase_token":"e9RaNibCBVo:APA91bHXzpUT6km0pWymAwJYlLpdAcFZ0Q4phEzkJ6BhdB6wcUV1Qdb2eYZbmP8DhC96YkQBJjM66oXXf4bCXJavcBZWHwNgdEFJTwQmbd0tyC0--PCCFiNcE2O1nHI9J7mcDaN3Hzhi","created_at":"2020-07-22 15:37:56","updated_at":"2020-08-26 13:40:14"}
     * error : 0
     * message : Your country is updated successfully
     */

    private DataBean data;
    private int error;
    private String message;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataBean {
        /**
         * id : 2
         * first_name : heba
         * last_name : muhammad
         * email : hebamuhammed22@gmail.com
         * phone : 01060945044
         * code : 4abccc
         * gender : female
         * image : null
         * Latitude : 0
         * Longitude : 0
         * rate : 0
         * role : 2
         * is_online : 0
         * is_driver : 0
         * is_busy : 0
         * wallet : 0
         * target : 0
         * token : null
         * firebase_token : e9RaNibCBVo:APA91bHXzpUT6km0pWymAwJYlLpdAcFZ0Q4phEzkJ6BhdB6wcUV1Qdb2eYZbmP8DhC96YkQBJjM66oXXf4bCXJavcBZWHwNgdEFJTwQmbd0tyC0--PCCFiNcE2O1nHI9J7mcDaN3Hzhi
         * created_at : 2020-07-22 15:37:56
         * updated_at : 2020-08-26 13:40:14
         */

        private int id;
        private String first_name;
        private String last_name;
        private String email;
        private String phone;
        private String code;
        private String gender;
        private String image;
        private String Latitude;
        private String Longitude;
        private String rate;
        private String role;
        private String is_online;
        private String is_driver;
        private String is_busy;
        private String wallet;
        private String target;
        private String token;
        private String firebase_token;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getIs_online() {
            return is_online;
        }

        public void setIs_online(String is_online) {
            this.is_online = is_online;
        }

        public String getIs_driver() {
            return is_driver;
        }

        public void setIs_driver(String is_driver) {
            this.is_driver = is_driver;
        }

        public String getIs_busy() {
            return is_busy;
        }

        public void setIs_busy(String is_busy) {
            this.is_busy = is_busy;
        }

        public String getWallet() {
            return wallet;
        }

        public void setWallet(String wallet) {
            this.wallet = wallet;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }

        public Object getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getFirebase_token() {
            return firebase_token;
        }

        public void setFirebase_token(String firebase_token) {
            this.firebase_token = firebase_token;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
