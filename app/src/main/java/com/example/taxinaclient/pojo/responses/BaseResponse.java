package com.example.taxinaclient.pojo.responses;

public class BaseResponse {
    /**
     * error : 0
     * message : new place add to favorite
     */

    private int error;
    private String message;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
