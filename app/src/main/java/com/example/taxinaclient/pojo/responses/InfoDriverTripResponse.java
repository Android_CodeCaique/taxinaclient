package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

public class InfoDriverTripResponse {

    @SerializedName("error")
    private int error ;

    @SerializedName("message")
    private String  message ;

    @SerializedName("data")
    private data  data ;


    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public InfoDriverTripResponse.data getData() {
        return data;
    }

    public void setData(InfoDriverTripResponse.data data) {
        this.data = data;
    }

    public class data{

        @SerializedName("id")
        private int id ;

        @SerializedName("first_name")
        private String first_name ;

        @SerializedName("last_name")
        private String last_name ;

         @SerializedName("phone")
        private String phone ;

        @SerializedName("rate")
        private String rate ;

         @SerializedName("car_name")
         private String car_name ;

         @SerializedName("plate")
         private String plate ;

         @SerializedName("pick_time")
         private String pick_time ;

         @SerializedName("code")
         private String code ;

         @SerializedName("image")
         private String photo ;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getCar_name() {
            return car_name;
        }

        public void setCar_name(String car_name) {
            this.car_name = car_name;
        }

        public String getPlate() {
            return plate;
        }

        public void setPlate(String plate) {
            this.plate = plate;
        }

        public String getPick_time() {
            return pick_time;
        }

        public void setPick_time(String pick_time) {
            this.pick_time = pick_time;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}
