package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {


    @SerializedName("error")
    private int error ;

    @SerializedName("message")
    private String message_en ;


    @SerializedName("data")
    private data data ;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage_en() {
        return message_en;
    }

    public void setMessage_en(String message_en) {
        this.message_en = message_en;
    }

    public LoginResponse.data getData() {
        return data;
    }

    public void setData(LoginResponse.data data) {
        this.data = data;
    }

    public class data{
        @SerializedName("id")
        private int id ;

        @SerializedName("first_name")
        private String first_name ;

        @SerializedName("last_name")
        private String last_name ;

        @SerializedName("email")
        private String email ;

        @SerializedName("phone")
        private String phone ;

        @SerializedName("gender")
        private String gender ;

        @SerializedName("image")
        private String image ;

        @SerializedName("Latitude")
        private String Latitude ;

        @SerializedName("Longitude")
        private String Longitude ;

        @SerializedName("token")
        private String token ;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}
