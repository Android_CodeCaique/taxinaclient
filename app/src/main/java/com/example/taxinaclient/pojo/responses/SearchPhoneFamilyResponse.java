package com.example.taxinaclient.pojo.responses;

public class SearchPhoneFamilyResponse {
    /**
     * data : {"id":2,"first_name":"Ahmed","last_name":"helal"}
     * error : 0
     * message : user data
     */

    private DataBean data;
    private int error;
    private String message;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataBean {
        /**
         * id : 2
         * first_name : Ahmed
         * last_name : helal
         */

        private int id;
        private String first_name;
        private String last_name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }
    }
}
