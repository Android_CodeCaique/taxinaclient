package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

public class PromoCodeReponse {


    @SerializedName("error")
    private int error ;

    @SerializedName("message")
    private String message ;

    @SerializedName("data")
    private String data ;


    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
