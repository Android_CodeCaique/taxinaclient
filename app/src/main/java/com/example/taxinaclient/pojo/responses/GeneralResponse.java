package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

public class GeneralResponse {

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message_en;


    public String getMessage_en() {
        return message_en;
    }

    public void setMessage_en(String message_en) {
        this.message_en = message_en;
    }


    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
