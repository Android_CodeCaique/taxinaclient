package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PromotionResponse {

    @SerializedName("error")
    private int error ;

    @SerializedName("message")
    private String message ;

    @SerializedName("data")
    private List<data> data ;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PromotionResponse.data> getData() {
        return data;
    }

    public void setData(List<PromotionResponse.data> data) {
        this.data = data;
    }

    public class data{


        @SerializedName("id")
        private int id ;

        @SerializedName("title")
        private String title ;

        @SerializedName("end_date")
        private String end_date ;

         @SerializedName("value")
        private String value ;

         @SerializedName("promo_code")
        private String promo_code ;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getEnd_date() {
            return end_date;
        }

        public void setEnd_date(String end_date) {
            this.end_date = end_date;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getPromo_code() {
            return promo_code;
        }

        public void setPromo_code(String promo_code) {
            this.promo_code = promo_code;
        }
    }

}
