package com.example.taxinaclient.pojo.model;

public class Kinds {

    private int image ;
    private int id ;
    private String name ;

    public Kinds() {
    }

    public Kinds(int image, int id, String name) {
        this.image = image;
        this.id = id;
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
