package com.example.taxinaclient.pojo.responses;

import java.util.List;

public class FamilyMembersAccepted {
    /**
     * data : [{"id":1,"user_id":"2","first_name":"Ahmed","last_name":"helal","email":"Ashrafhelal2000@gmail.com","phone":"01254785410","image":"http://taxiApi.codecaique.com/uploads/users/img.png","state":"no"}]
     * error : 0
     * message : all family members
     */

    private int error;
    private String message;
    private List<DataBean> data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * user_id : 2
         * first_name : Ahmed
         * last_name : helal
         * email : Ashrafhelal2000@gmail.com
         * phone : 01254785410
         * image : http://taxiApi.codecaique.com/uploads/users/img.png
         * state : no
         */

        private int id;
        private String user_id;
        private String first_name;
        private String last_name;
        private String email;
        private String phone;
        private String image;
        private String state;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }
}
