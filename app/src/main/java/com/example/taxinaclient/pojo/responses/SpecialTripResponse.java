package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

public class SpecialTripResponse {
    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message_en;

    @SerializedName("data")
    private data data ;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage_en() {
        return message_en;
    }

    public void setMessage_en(String message_en) {
        this.message_en = message_en;
    }

    public data getData() {
        return data;
    }

    public void setData(data data) {
        this.data = data;
    }

    public class data{
        @SerializedName("id")
        private int id ;

        @SerializedName("from_areaId")
        private String from_areaId ;
        @SerializedName("code")
        private String code ;
        @SerializedName("image")
        private String image ;

        @SerializedName("fromcity_name")
        private String fromcity_name ;

        @SerializedName("from_area")
        private String from_area ;

        @SerializedName("to_areaId")
        private String to_areaId ;

        @SerializedName("tocity_name")
        private String tocity_name ;

        @SerializedName("to_area")
        private String to_area ;

        @SerializedName("seat_count")
        private String seat_count ;

        @SerializedName("date")
        private String date ;

        @SerializedName("from_time")
        private String from_time ;

        @SerializedName("price")
        private String price ;


        @SerializedName("plate")
        private String plate ;

        @SerializedName("color")
        private String color ;

        @SerializedName("state")
        private int state ;

        @SerializedName("car_name")
        private String car_name ;

        @SerializedName("first_name")
        private String first_name ;


        @SerializedName("last_name")
        private String last_name ;

        @SerializedName("rate")
        private String rate ;

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getImage() {
            return image;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFrom_areaId() {
            return from_areaId;
        }

        public void setFrom_areaId(String from_areaId) {
            this.from_areaId = from_areaId;
        }

        public String getFromcity_name() {
            return fromcity_name;
        }

        public void setFromcity_name(String fromcity_name) {
            this.fromcity_name = fromcity_name;
        }

        public String getFrom_area() {
            return from_area;
        }

        public void setFrom_area(String from_area) {
            this.from_area = from_area;
        }

        public String getTo_areaId() {
            return to_areaId;
        }

        public void setTo_areaId(String to_areaId) {
            this.to_areaId = to_areaId;
        }

        public String getTocity_name() {
            return tocity_name;
        }

        public void setTocity_name(String tocity_name) {
            this.tocity_name = tocity_name;
        }

        public String getTo_area() {
            return to_area;
        }

        public void setTo_area(String to_area) {
            this.to_area = to_area;
        }

        public String getSeat_count() {
            return seat_count;
        }

        public void setSeat_count(String seat_count) {
            this.seat_count = seat_count;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getFrom_time() {
            return from_time;
        }

        public void setFrom_time(String from_time) {
            this.from_time = from_time;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getCar_name() {
            return car_name;
        }

        public void setCar_name(String car_name) {
            this.car_name = car_name;
        }

        public String getPlate() {
            return plate;
        }

        public void setPlate(String plate) {
            this.plate = plate;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }
    }

}
