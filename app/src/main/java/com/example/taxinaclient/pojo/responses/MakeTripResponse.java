package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

public class MakeTripResponse {

    @SerializedName("trip_id")
    private int trip_id ;

    @SerializedName("error")
    private int error ;

    @SerializedName("message")
    private String message ;

    public int getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(int trip_id) {
        this.trip_id = trip_id;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
