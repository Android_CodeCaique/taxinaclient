package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarAvilableResponse {

    @SerializedName("error")
    private int error ;

    @SerializedName("message")
    private String message_en ;



    @SerializedName("data")
    private List<data> data ;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage_en() {
        return message_en;
    }

    public void setMessage_en(String message_en) {
        this.message_en = message_en;
    }

    public List<CarAvilableResponse.data> getData() {
        return data;
    }

    public void setData(List<CarAvilableResponse.data> data) {
        this.data = data;
    }

    public class data{
        @SerializedName("id")
        private int id ;
        @SerializedName("name")
        private String name ;
        @SerializedName("image")
        private String image ;
        @SerializedName("price")
        private String price ;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }

}
