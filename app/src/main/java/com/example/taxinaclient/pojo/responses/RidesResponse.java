package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RidesResponse {


    @SerializedName("error")
    private int error ;

     @SerializedName("message")
    private String message ;

     @SerializedName("data")
    private List<data> data ;

     @SerializedName("special_trip")
    private List<special_trip> special_trip ;

    public List<RidesResponse.special_trip> getSpecial_trip() {
        return special_trip;
    }

    public void setSpecial_trip(List<RidesResponse.special_trip> special_trip) {
        this.special_trip = special_trip;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RidesResponse.data> getData() {
        return data;
    }

    public void setData(List<RidesResponse.data> data) {
        this.data = data;
    }

    public class data{


         @SerializedName("id")
         private int id ;

         @SerializedName("userCar_id")
         private String userCar_id ;

          @SerializedName("user_id")
          private String user_id ;

          @SerializedName("first_name")
          private String first_name ;


          @SerializedName("last_name")
          private String last_name ;

          @SerializedName("image")
          private String image ;

           @SerializedName("driver_id")
          private String driver_id ;

           @SerializedName("start_lat")
          private String start_lat ;

          @SerializedName("start_long")
          private String start_long ;

           @SerializedName("start_location")
          private String start_location ;

          @SerializedName("end_lat")
          private String end_lat ;

           @SerializedName("end_long")
          private String end_long ;

            @SerializedName("end_location")
          private String end_location ;

           @SerializedName("distance")
          private String distance ;

           @SerializedName("promo_code")
          private String promo_code ;

          @SerializedName("price")
          private String price ;

          @SerializedName("note")
          private String note ;

          @SerializedName("rate")
          private String rate ;

          @SerializedName("status")
          private String status ;

          @SerializedName("payment_method")
          private String payment_method ;

          @SerializedName("type")
          private String type ;

           @SerializedName("date")
          private String date ;

          @SerializedName("time")
          private String time ;

          @SerializedName("created_at")
          private String created_at ;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUserCar_id() {
            return userCar_id;
        }

        public void setUserCar_id(String userCar_id) {
            this.userCar_id = userCar_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDriver_id() {
            return driver_id;
        }

        public void setDriver_id(String driver_id) {
            this.driver_id = driver_id;
        }

        public String getStart_lat() {
            return start_lat;
        }

        public void setStart_lat(String start_lat) {
            this.start_lat = start_lat;
        }

        public String getStart_long() {
            return start_long;
        }

        public void setStart_long(String start_long) {
            this.start_long = start_long;
        }

        public String getStart_location() {
            return start_location;
        }

        public void setStart_location(String start_location) {
            this.start_location = start_location;
        }

        public String getEnd_lat() {
            return end_lat;
        }

        public void setEnd_lat(String end_lat) {
            this.end_lat = end_lat;
        }

        public String getEnd_long() {
            return end_long;
        }

        public void setEnd_long(String end_long) {
            this.end_long = end_long;
        }

        public String getEnd_location() {
            return end_location;
        }

        public void setEnd_location(String end_location) {
            this.end_location = end_location;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getPromo_code() {
            return promo_code;
        }

        public void setPromo_code(String promo_code) {
            this.promo_code = promo_code;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public void setPayment_method(String payment_method) {
            this.payment_method = payment_method;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
    public class special_trip{

        @SerializedName("id")
        private int id ;

        @SerializedName("from_areaId")
        private String from_areaId ;

        @SerializedName("fromcity_name")
        private String fromcity_name ;

        @SerializedName("from_area")
        private String from_area ;

        @SerializedName("to_areaId")
        private String to_areaId ;

        @SerializedName("tocity_name")
        private String tocity_name ;

        @SerializedName("to_area")
        private String to_area ;

        @SerializedName("seat_count")
        private String seat_count ;

        @SerializedName("date")
        private String date ;

        @SerializedName("from_time")
        private String from_time ;

        @SerializedName("price")
        private String price ;


        @SerializedName("money")
        private String money ;


        @SerializedName("car_name")
        private String car_name ;

        @SerializedName("plate")
        private String plate ;

        @SerializedName("color")
        private String color ;

        @SerializedName("type_id")
        private String type_id ;


        public String getFrom_area() {
            return from_area;
        }

        public void setFrom_area(String from_area) {
            this.from_area = from_area;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFrom_areaId() {
            return from_areaId;
        }

        public void setFrom_areaId(String from_areaId) {
            this.from_areaId = from_areaId;
        }

        public String getFromcity_name() {
            return fromcity_name;
        }

        public void setFromcity_name(String fromcity_name) {
            this.fromcity_name = fromcity_name;
        }

        public String getTo_areaId() {
            return to_areaId;
        }

        public void setTo_areaId(String to_areaId) {
            this.to_areaId = to_areaId;
        }

        public String getTocity_name() {
            return tocity_name;
        }

        public void setTocity_name(String tocity_name) {
            this.tocity_name = tocity_name;
        }

        public String getTo_area() {
            return to_area;
        }

        public void setTo_area(String to_area) {
            this.to_area = to_area;
        }

        public String getSeat_count() {
            return seat_count;
        }

        public void setSeat_count(String seat_count) {
            this.seat_count = seat_count;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getFrom_time() {
            return from_time;
        }

        public void setFrom_time(String from_time) {
            this.from_time = from_time;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getCar_name() {
            return car_name;
        }

        public void setCar_name(String car_name) {
            this.car_name = car_name;
        }

        public String getPlate() {
            return plate;
        }

        public void setPlate(String plate) {
            this.plate = plate;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }
    }


}
