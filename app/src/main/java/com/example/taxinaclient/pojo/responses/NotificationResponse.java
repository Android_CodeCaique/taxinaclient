package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponse {

    @SerializedName("error")
    private int error ;

    @SerializedName("message")
    private String message ;

    @SerializedName("data")
    private List<data>  data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NotificationResponse.data> getData() {
        return data;
    }

    public void setData(List<NotificationResponse.data> data) {
        this.data = data;
    }

    public class data {
        @SerializedName("id")
        private int id ;

        @SerializedName("title")
        private String title ;

        @SerializedName("user_id")
        private String user_id ;

        @SerializedName("body")
        private String body ;

        @SerializedName("created_at")
        private String created_at ;

        @SerializedName("updated_at")
        private String updated_at ;

        @SerializedName("click_action")
        private String click_action ;

        @SerializedName("redirect_id")
        private String redirect_id ;

        @SerializedName("type")
        private String type ;

        @SerializedName("status")
        private String status ;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getClick_action() {
            return click_action;
        }

        public void setClick_action(String click_action) {
            this.click_action = click_action;
        }

        public String getRedirect_id() {
            return redirect_id;
        }

        public void setRedirect_id(String redirect_id) {
            this.redirect_id = redirect_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }


}
