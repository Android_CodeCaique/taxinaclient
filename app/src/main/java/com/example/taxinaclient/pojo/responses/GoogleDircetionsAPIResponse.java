package com.example.taxinaclient.pojo.responses;

import java.util.List;

public class GoogleDircetionsAPIResponse {
    /**
     * geocoded_waypoints : [{"geocoder_status":"OK","place_id":"ChIJH74prUEFWBQRavcuhN8p8wE","types":["route"]},{"geocoder_status":"OK","place_id":"ChIJ6Z3_LhkFWBQRaFzEukU9ZFU","types":["establishment","point_of_interest","shopping_mall"]}]
     * routes : [{"bounds":{"northeast":{"lat":30.2414941,"lng":31.4766742},"southwest":{"lat":30.2360871,"lng":31.4654702}},"copyrights":"Map data ©2020","legs":[{"distance":{"text":"1.6 km","value":1588},"duration":{"text":"5 mins","value":290},"end_address":"الحى الاول، العبور،، Al Obour, Al Qalyubia Governorate, Egypt","end_location":{"lat":30.2369094,"lng":31.4766742},"start_address":"Abd El-Rahman El-Rafie, Al Obour, Cairo Governorate, Egypt","start_location":{"lat":30.2414941,"lng":31.4654702},"steps":[{"distance":{"text":"0.1 km","value":132},"duration":{"text":"1 min","value":16},"end_location":{"lat":30.2413956,"lng":31.4668285},"html_instructions":"Head <b>east<\/b>","polyline":{"points":"ipqwDer`_EBOHk@@WB{@?c@?GAsA"},"start_location":{"lat":30.2414941,"lng":31.4654702},"travel_mode":"DRIVING"},{"distance":{"text":"0.3 km","value":325},"duration":{"text":"1 min","value":38},"end_location":{"lat":30.24010659999999,"lng":31.4687144},"html_instructions":"Turn <b>right<\/b>","maneuver":"turn-right","polyline":{"points":"woqwDuz`_ETMn@i@BCV[Va@HQJa@@Q@KD_@B]Au@@c@@GD]FWHSJMDEBADAD?D?RD@V?B@F@FBJBV"},"start_location":{"lat":30.2413956,"lng":31.4668285},"travel_mode":"DRIVING"},{"distance":{"text":"46 m","value":46},"duration":{"text":"1 min","value":8},"end_location":{"lat":30.2399723,"lng":31.4690539},"html_instructions":"Sharp <b>left<\/b>","maneuver":"turn-sharp-left","polyline":{"points":"ugqwDmfa_EXC?I@E?G?G?M?O?A"},"start_location":{"lat":30.24010659999999,"lng":31.4687144},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":112},"duration":{"text":"1 min","value":16},"end_location":{"lat":30.2391125,"lng":31.4693412},"html_instructions":"Keep <b>right<\/b>","maneuver":"keep-right","polyline":{"points":"yfqwDqha_E?KBM@G@ADEBCFCPE`@ETAN?b@DR?"},"start_location":{"lat":30.2399723,"lng":31.4690539},"travel_mode":"DRIVING"},{"distance":{"text":"30 m","value":30},"duration":{"text":"1 min","value":4},"end_location":{"lat":30.239178,"lng":31.46964809999999},"html_instructions":"Turn <b>left<\/b>","maneuver":"turn-left","polyline":{"points":"maqwDkja_EM}@"},"start_location":{"lat":30.2391125,"lng":31.4693412},"travel_mode":"DRIVING"},{"distance":{"text":"0.4 km","value":398},"duration":{"text":"2 mins","value":95},"end_location":{"lat":30.2360871,"lng":31.4715629},"html_instructions":"Turn <b>right<\/b>","maneuver":"turn-right","polyline":{"points":"{aqwDila_ERCrCm@^Qb@Ib@GNEBAHELINMx@q@~@}@V[v@{@LO`@Ub@S"},"start_location":{"lat":30.239178,"lng":31.46964809999999},"travel_mode":"DRIVING"},{"distance":{"text":"0.3 km","value":348},"duration":{"text":"1 min","value":66},"end_location":{"lat":30.2369153,"lng":31.4750491},"html_instructions":"Turn <b>left<\/b>","maneuver":"turn-left","polyline":{"points":"qnpwDgxa_EOoAMoAG{@O_A_@mCI}@Ge@}@kD"},"start_location":{"lat":30.2360871,"lng":31.4715629},"travel_mode":"DRIVING"},{"distance":{"text":"9 m","value":9},"duration":{"text":"1 min","value":2},"end_location":{"lat":30.2369956,"lng":31.4750328},"html_instructions":"Turn <b>left<\/b>","maneuver":"turn-left","polyline":{"points":"wspwDanb_EOB"},"start_location":{"lat":30.2369153,"lng":31.4750491},"travel_mode":"DRIVING"},{"distance":{"text":"0.2 km","value":156},"duration":{"text":"1 min","value":38},"end_location":{"lat":30.2371926,"lng":31.4766375},"html_instructions":"Turn <b>right<\/b>","maneuver":"turn-right","polyline":{"points":"gtpwD}mb_EOwBG{@A]KoB"},"start_location":{"lat":30.2369956,"lng":31.4750328},"travel_mode":"DRIVING"},{"distance":{"text":"32 m","value":32},"duration":{"text":"1 min","value":7},"end_location":{"lat":30.2369094,"lng":31.4766742},"html_instructions":"Turn <b>right<\/b><div style=\"font-size:0.9em\">Destination will be on the right<\/div>","maneuver":"turn-right","polyline":{"points":"mupwD_xb_Ev@E"},"start_location":{"lat":30.2371926,"lng":31.4766375},"travel_mode":"DRIVING"}],"traffic_speed_entry":[],"via_waypoint":[]}],"overview_polyline":{"points":"ipqwDer`_EL{@DsA?k@AsATMr@m@n@}@Ts@B]H}@?yAFe@Pk@PSHCJ?RD@V@JDRBVXC@O?m@Dc@ROr@Kd@Av@DM}@RCrCm@^QfAQRGVOhA_AvAyAdAkAdAi@]_DG{@O_Ai@kEGe@}@kDOBOwBIyAKoBv@E"},"summary":"","warnings":[],"waypoint_order":[]}]
     * status : OK
     */

    private String status;
    private List<GeocodedWaypointsBean> geocoded_waypoints;
    private List<RoutesBean> routes;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<GeocodedWaypointsBean> getGeocoded_waypoints() {
        return geocoded_waypoints;
    }

    public void setGeocoded_waypoints(List<GeocodedWaypointsBean> geocoded_waypoints) {
        this.geocoded_waypoints = geocoded_waypoints;
    }

    public List<RoutesBean> getRoutes() {
        return routes;
    }

    public void setRoutes(List<RoutesBean> routes) {
        this.routes = routes;
    }

    public static class GeocodedWaypointsBean {
        /**
         * geocoder_status : OK
         * place_id : ChIJH74prUEFWBQRavcuhN8p8wE
         * types : ["route"]
         */

        private String geocoder_status;
        private String place_id;
        private List<String> types;

        public String getGeocoder_status() {
            return geocoder_status;
        }

        public void setGeocoder_status(String geocoder_status) {
            this.geocoder_status = geocoder_status;
        }

        public String getPlace_id() {
            return place_id;
        }

        public void setPlace_id(String place_id) {
            this.place_id = place_id;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }
    }

    public static class RoutesBean {
        /**
         * bounds : {"northeast":{"lat":30.2414941,"lng":31.4766742},"southwest":{"lat":30.2360871,"lng":31.4654702}}
         * copyrights : Map data ©2020
         * legs : [{"distance":{"text":"1.6 km","value":1588},"duration":{"text":"5 mins","value":290},"end_address":"الحى الاول، العبور،، Al Obour, Al Qalyubia Governorate, Egypt","end_location":{"lat":30.2369094,"lng":31.4766742},"start_address":"Abd El-Rahman El-Rafie, Al Obour, Cairo Governorate, Egypt","start_location":{"lat":30.2414941,"lng":31.4654702},"steps":[{"distance":{"text":"0.1 km","value":132},"duration":{"text":"1 min","value":16},"end_location":{"lat":30.2413956,"lng":31.4668285},"html_instructions":"Head <b>east<\/b>","polyline":{"points":"ipqwDer`_EBOHk@@WB{@?c@?GAsA"},"start_location":{"lat":30.2414941,"lng":31.4654702},"travel_mode":"DRIVING"},{"distance":{"text":"0.3 km","value":325},"duration":{"text":"1 min","value":38},"end_location":{"lat":30.24010659999999,"lng":31.4687144},"html_instructions":"Turn <b>right<\/b>","maneuver":"turn-right","polyline":{"points":"woqwDuz`_ETMn@i@BCV[Va@HQJa@@Q@KD_@B]Au@@c@@GD]FWHSJMDEBADAD?D?RD@V?B@F@FBJBV"},"start_location":{"lat":30.2413956,"lng":31.4668285},"travel_mode":"DRIVING"},{"distance":{"text":"46 m","value":46},"duration":{"text":"1 min","value":8},"end_location":{"lat":30.2399723,"lng":31.4690539},"html_instructions":"Sharp <b>left<\/b>","maneuver":"turn-sharp-left","polyline":{"points":"ugqwDmfa_EXC?I@E?G?G?M?O?A"},"start_location":{"lat":30.24010659999999,"lng":31.4687144},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":112},"duration":{"text":"1 min","value":16},"end_location":{"lat":30.2391125,"lng":31.4693412},"html_instructions":"Keep <b>right<\/b>","maneuver":"keep-right","polyline":{"points":"yfqwDqha_E?KBM@G@ADEBCFCPE`@ETAN?b@DR?"},"start_location":{"lat":30.2399723,"lng":31.4690539},"travel_mode":"DRIVING"},{"distance":{"text":"30 m","value":30},"duration":{"text":"1 min","value":4},"end_location":{"lat":30.239178,"lng":31.46964809999999},"html_instructions":"Turn <b>left<\/b>","maneuver":"turn-left","polyline":{"points":"maqwDkja_EM}@"},"start_location":{"lat":30.2391125,"lng":31.4693412},"travel_mode":"DRIVING"},{"distance":{"text":"0.4 km","value":398},"duration":{"text":"2 mins","value":95},"end_location":{"lat":30.2360871,"lng":31.4715629},"html_instructions":"Turn <b>right<\/b>","maneuver":"turn-right","polyline":{"points":"{aqwDila_ERCrCm@^Qb@Ib@GNEBAHELINMx@q@~@}@V[v@{@LO`@Ub@S"},"start_location":{"lat":30.239178,"lng":31.46964809999999},"travel_mode":"DRIVING"},{"distance":{"text":"0.3 km","value":348},"duration":{"text":"1 min","value":66},"end_location":{"lat":30.2369153,"lng":31.4750491},"html_instructions":"Turn <b>left<\/b>","maneuver":"turn-left","polyline":{"points":"qnpwDgxa_EOoAMoAG{@O_A_@mCI}@Ge@}@kD"},"start_location":{"lat":30.2360871,"lng":31.4715629},"travel_mode":"DRIVING"},{"distance":{"text":"9 m","value":9},"duration":{"text":"1 min","value":2},"end_location":{"lat":30.2369956,"lng":31.4750328},"html_instructions":"Turn <b>left<\/b>","maneuver":"turn-left","polyline":{"points":"wspwDanb_EOB"},"start_location":{"lat":30.2369153,"lng":31.4750491},"travel_mode":"DRIVING"},{"distance":{"text":"0.2 km","value":156},"duration":{"text":"1 min","value":38},"end_location":{"lat":30.2371926,"lng":31.4766375},"html_instructions":"Turn <b>right<\/b>","maneuver":"turn-right","polyline":{"points":"gtpwD}mb_EOwBG{@A]KoB"},"start_location":{"lat":30.2369956,"lng":31.4750328},"travel_mode":"DRIVING"},{"distance":{"text":"32 m","value":32},"duration":{"text":"1 min","value":7},"end_location":{"lat":30.2369094,"lng":31.4766742},"html_instructions":"Turn <b>right<\/b><div style=\"font-size:0.9em\">Destination will be on the right<\/div>","maneuver":"turn-right","polyline":{"points":"mupwD_xb_Ev@E"},"start_location":{"lat":30.2371926,"lng":31.4766375},"travel_mode":"DRIVING"}],"traffic_speed_entry":[],"via_waypoint":[]}]
         * overview_polyline : {"points":"ipqwDer`_EL{@DsA?k@AsATMr@m@n@}@Ts@B]H}@?yAFe@Pk@PSHCJ?RD@V@JDRBVXC@O?m@Dc@ROr@Kd@Av@DM}@RCrCm@^QfAQRGVOhA_AvAyAdAkAdAi@]_DG{@O_Ai@kEGe@}@kDOBOwBIyAKoBv@E"}
         * summary :
         * warnings : []
         * waypoint_order : []
         */

        private BoundsBean bounds;
        private String copyrights;
        private OverviewPolylineBean overview_polyline;
        private String summary;
        private List<LegsBean> legs;
        private List<?> warnings;
        private List<?> waypoint_order;

        public BoundsBean getBounds() {
            return bounds;
        }

        public void setBounds(BoundsBean bounds) {
            this.bounds = bounds;
        }

        public String getCopyrights() {
            return copyrights;
        }

        public void setCopyrights(String copyrights) {
            this.copyrights = copyrights;
        }

        public OverviewPolylineBean getOverview_polyline() {
            return overview_polyline;
        }

        public void setOverview_polyline(OverviewPolylineBean overview_polyline) {
            this.overview_polyline = overview_polyline;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public List<LegsBean> getLegs() {
            return legs;
        }

        public void setLegs(List<LegsBean> legs) {
            this.legs = legs;
        }

        public List<?> getWarnings() {
            return warnings;
        }

        public void setWarnings(List<?> warnings) {
            this.warnings = warnings;
        }

        public List<?> getWaypoint_order() {
            return waypoint_order;
        }

        public void setWaypoint_order(List<?> waypoint_order) {
            this.waypoint_order = waypoint_order;
        }

        public static class BoundsBean {
            /**
             * northeast : {"lat":30.2414941,"lng":31.4766742}
             * southwest : {"lat":30.2360871,"lng":31.4654702}
             */

            private NortheastBean northeast;
            private SouthwestBean southwest;

            public NortheastBean getNortheast() {
                return northeast;
            }

            public void setNortheast(NortheastBean northeast) {
                this.northeast = northeast;
            }

            public SouthwestBean getSouthwest() {
                return southwest;
            }

            public void setSouthwest(SouthwestBean southwest) {
                this.southwest = southwest;
            }

            public static class NortheastBean {
                /**
                 * lat : 30.2414941
                 * lng : 31.4766742
                 */

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }

            public static class SouthwestBean {
                /**
                 * lat : 30.2360871
                 * lng : 31.4654702
                 */

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }
        }

        public static class OverviewPolylineBean {
            /**
             * points : ipqwDer`_EL{@DsA?k@AsATMr@m@n@}@Ts@B]H}@?yAFe@Pk@PSHCJ?RD@V@JDRBVXC@O?m@Dc@ROr@Kd@Av@DM}@RCrCm@^QfAQRGVOhA_AvAyAdAkAdAi@]_DG{@O_Ai@kEGe@}@kDOBOwBIyAKoBv@E
             */

            private String points;

            public String getPoints() {
                return points;
            }

            public void setPoints(String points) {
                this.points = points;
            }
        }

        public static class LegsBean {
            /**
             * distance : {"text":"1.6 km","value":1588}
             * duration : {"text":"5 mins","value":290}
             * end_address : الحى الاول، العبور،، Al Obour, Al Qalyubia Governorate, Egypt
             * end_location : {"lat":30.2369094,"lng":31.4766742}
             * start_address : Abd El-Rahman El-Rafie, Al Obour, Cairo Governorate, Egypt
             * start_location : {"lat":30.2414941,"lng":31.4654702}
             * steps : [{"distance":{"text":"0.1 km","value":132},"duration":{"text":"1 min","value":16},"end_location":{"lat":30.2413956,"lng":31.4668285},"html_instructions":"Head <b>east<\/b>","polyline":{"points":"ipqwDer`_EBOHk@@WB{@?c@?GAsA"},"start_location":{"lat":30.2414941,"lng":31.4654702},"travel_mode":"DRIVING"},{"distance":{"text":"0.3 km","value":325},"duration":{"text":"1 min","value":38},"end_location":{"lat":30.24010659999999,"lng":31.4687144},"html_instructions":"Turn <b>right<\/b>","maneuver":"turn-right","polyline":{"points":"woqwDuz`_ETMn@i@BCV[Va@HQJa@@Q@KD_@B]Au@@c@@GD]FWHSJMDEBADAD?D?RD@V?B@F@FBJBV"},"start_location":{"lat":30.2413956,"lng":31.4668285},"travel_mode":"DRIVING"},{"distance":{"text":"46 m","value":46},"duration":{"text":"1 min","value":8},"end_location":{"lat":30.2399723,"lng":31.4690539},"html_instructions":"Sharp <b>left<\/b>","maneuver":"turn-sharp-left","polyline":{"points":"ugqwDmfa_EXC?I@E?G?G?M?O?A"},"start_location":{"lat":30.24010659999999,"lng":31.4687144},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":112},"duration":{"text":"1 min","value":16},"end_location":{"lat":30.2391125,"lng":31.4693412},"html_instructions":"Keep <b>right<\/b>","maneuver":"keep-right","polyline":{"points":"yfqwDqha_E?KBM@G@ADEBCFCPE`@ETAN?b@DR?"},"start_location":{"lat":30.2399723,"lng":31.4690539},"travel_mode":"DRIVING"},{"distance":{"text":"30 m","value":30},"duration":{"text":"1 min","value":4},"end_location":{"lat":30.239178,"lng":31.46964809999999},"html_instructions":"Turn <b>left<\/b>","maneuver":"turn-left","polyline":{"points":"maqwDkja_EM}@"},"start_location":{"lat":30.2391125,"lng":31.4693412},"travel_mode":"DRIVING"},{"distance":{"text":"0.4 km","value":398},"duration":{"text":"2 mins","value":95},"end_location":{"lat":30.2360871,"lng":31.4715629},"html_instructions":"Turn <b>right<\/b>","maneuver":"turn-right","polyline":{"points":"{aqwDila_ERCrCm@^Qb@Ib@GNEBAHELINMx@q@~@}@V[v@{@LO`@Ub@S"},"start_location":{"lat":30.239178,"lng":31.46964809999999},"travel_mode":"DRIVING"},{"distance":{"text":"0.3 km","value":348},"duration":{"text":"1 min","value":66},"end_location":{"lat":30.2369153,"lng":31.4750491},"html_instructions":"Turn <b>left<\/b>","maneuver":"turn-left","polyline":{"points":"qnpwDgxa_EOoAMoAG{@O_A_@mCI}@Ge@}@kD"},"start_location":{"lat":30.2360871,"lng":31.4715629},"travel_mode":"DRIVING"},{"distance":{"text":"9 m","value":9},"duration":{"text":"1 min","value":2},"end_location":{"lat":30.2369956,"lng":31.4750328},"html_instructions":"Turn <b>left<\/b>","maneuver":"turn-left","polyline":{"points":"wspwDanb_EOB"},"start_location":{"lat":30.2369153,"lng":31.4750491},"travel_mode":"DRIVING"},{"distance":{"text":"0.2 km","value":156},"duration":{"text":"1 min","value":38},"end_location":{"lat":30.2371926,"lng":31.4766375},"html_instructions":"Turn <b>right<\/b>","maneuver":"turn-right","polyline":{"points":"gtpwD}mb_EOwBG{@A]KoB"},"start_location":{"lat":30.2369956,"lng":31.4750328},"travel_mode":"DRIVING"},{"distance":{"text":"32 m","value":32},"duration":{"text":"1 min","value":7},"end_location":{"lat":30.2369094,"lng":31.4766742},"html_instructions":"Turn <b>right<\/b><div style=\"font-size:0.9em\">Destination will be on the right<\/div>","maneuver":"turn-right","polyline":{"points":"mupwD_xb_Ev@E"},"start_location":{"lat":30.2371926,"lng":31.4766375},"travel_mode":"DRIVING"}]
             * traffic_speed_entry : []
             * via_waypoint : []
             */

            private DistanceBean distance;
            private DurationBean duration;
            private String end_address;
            private EndLocationBean end_location;
            private String start_address;
            private StartLocationBean start_location;
            private List<StepsBean> steps;
            private List<?> traffic_speed_entry;
            private List<?> via_waypoint;

            public DistanceBean getDistance() {
                return distance;
            }

            public void setDistance(DistanceBean distance) {
                this.distance = distance;
            }

            public DurationBean getDuration() {
                return duration;
            }

            public void setDuration(DurationBean duration) {
                this.duration = duration;
            }

            public String getEnd_address() {
                return end_address;
            }

            public void setEnd_address(String end_address) {
                this.end_address = end_address;
            }

            public EndLocationBean getEnd_location() {
                return end_location;
            }

            public void setEnd_location(EndLocationBean end_location) {
                this.end_location = end_location;
            }

            public String getStart_address() {
                return start_address;
            }

            public void setStart_address(String start_address) {
                this.start_address = start_address;
            }

            public StartLocationBean getStart_location() {
                return start_location;
            }

            public void setStart_location(StartLocationBean start_location) {
                this.start_location = start_location;
            }

            public List<StepsBean> getSteps() {
                return steps;
            }

            public void setSteps(List<StepsBean> steps) {
                this.steps = steps;
            }

            public List<?> getTraffic_speed_entry() {
                return traffic_speed_entry;
            }

            public void setTraffic_speed_entry(List<?> traffic_speed_entry) {
                this.traffic_speed_entry = traffic_speed_entry;
            }

            public List<?> getVia_waypoint() {
                return via_waypoint;
            }

            public void setVia_waypoint(List<?> via_waypoint) {
                this.via_waypoint = via_waypoint;
            }

            public static class DistanceBean {
                /**
                 * text : 1.6 km
                 * value : 1588
                 */

                private String text;
                private int value;

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public int getValue() {
                    return value;
                }

                public void setValue(int value) {
                    this.value = value;
                }
            }

            public static class DurationBean {
                /**
                 * text : 5 mins
                 * value : 290
                 */

                private String text;
                private int value;

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public int getValue() {
                    return value;
                }

                public void setValue(int value) {
                    this.value = value;
                }
            }

            public static class EndLocationBean {
                /**
                 * lat : 30.2369094
                 * lng : 31.4766742
                 */

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }

            public static class StartLocationBean {
                /**
                 * lat : 30.2414941
                 * lng : 31.4654702
                 */

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }

            public static class StepsBean {
                /**
                 * distance : {"text":"0.1 km","value":132}
                 * duration : {"text":"1 min","value":16}
                 * end_location : {"lat":30.2413956,"lng":31.4668285}
                 * html_instructions : Head <b>east</b>
                 * polyline : {"points":"ipqwDer`_EBOHk@@WB{@?c@?GAsA"}
                 * start_location : {"lat":30.2414941,"lng":31.4654702}
                 * travel_mode : DRIVING
                 * maneuver : turn-right
                 */

                private DistanceBeanX distance;
                private DurationBeanX duration;
                private EndLocationBeanX end_location;
                private String html_instructions;
                private PolylineBean polyline;
                private StartLocationBeanX start_location;
                private String travel_mode;
                private String maneuver;

                public DistanceBeanX getDistance() {
                    return distance;
                }

                public void setDistance(DistanceBeanX distance) {
                    this.distance = distance;
                }

                public DurationBeanX getDuration() {
                    return duration;
                }

                public void setDuration(DurationBeanX duration) {
                    this.duration = duration;
                }

                public EndLocationBeanX getEnd_location() {
                    return end_location;
                }

                public void setEnd_location(EndLocationBeanX end_location) {
                    this.end_location = end_location;
                }

                public String getHtml_instructions() {
                    return html_instructions;
                }

                public void setHtml_instructions(String html_instructions) {
                    this.html_instructions = html_instructions;
                }

                public PolylineBean getPolyline() {
                    return polyline;
                }

                public void setPolyline(PolylineBean polyline) {
                    this.polyline = polyline;
                }

                public StartLocationBeanX getStart_location() {
                    return start_location;
                }

                public void setStart_location(StartLocationBeanX start_location) {
                    this.start_location = start_location;
                }

                public String getTravel_mode() {
                    return travel_mode;
                }

                public void setTravel_mode(String travel_mode) {
                    this.travel_mode = travel_mode;
                }

                public String getManeuver() {
                    return maneuver;
                }

                public void setManeuver(String maneuver) {
                    this.maneuver = maneuver;
                }

                public static class DistanceBeanX {
                    /**
                     * text : 0.1 km
                     * value : 132
                     */

                    private String text;
                    private int value;

                    public String getText() {
                        return text;
                    }

                    public void setText(String text) {
                        this.text = text;
                    }

                    public int getValue() {
                        return value;
                    }

                    public void setValue(int value) {
                        this.value = value;
                    }
                }

                public static class DurationBeanX {
                    /**
                     * text : 1 min
                     * value : 16
                     */

                    private String text;
                    private int value;

                    public String getText() {
                        return text;
                    }

                    public void setText(String text) {
                        this.text = text;
                    }

                    public int getValue() {
                        return value;
                    }

                    public void setValue(int value) {
                        this.value = value;
                    }
                }

                public static class EndLocationBeanX {
                    /**
                     * lat : 30.2413956
                     * lng : 31.4668285
                     */

                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }

                public static class PolylineBean {
                    /**
                     * points : ipqwDer`_EBOHk@@WB{@?c@?GAsA
                     */

                    private String points;

                    public String getPoints() {
                        return points;
                    }

                    public void setPoints(String points) {
                        this.points = points;
                    }
                }

                public static class StartLocationBeanX {
                    /**
                     * lat : 30.2414941
                     * lng : 31.4654702
                     */

                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }
            }
        }
    }
}
