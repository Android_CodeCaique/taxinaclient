package com.example.taxinaclient.pojo.responses;

import java.util.List;

public class MyFavoritesResponse {

    /**
     * data : [{"id":2,"name":"احلي حلواني","Latitude":"30.120301525084546","Longitude":"31.340502910315994","address":"67 Ismail Al Fangari, AZ Zahraa WA Masaken Al Helmeyah, Ein Shams, Cairo Governorate, Egypt"},{"id":8,"name":"مستشفي فريد حبيب","Latitude":"30.225643508956537","Longitude":"31.481642089784142","address":"Unnamed Road, Al Obour, Cairo Governorate, Egypt"},{"id":9,"name":"مسجد السيده عائشه","Latitude":"30.025265306821566","Longitude":"31.256925277411934","address":"31 El-Fath, Al Abageyah, El-Khalifa, Cairo Governorate, Egypt"},{"id":10,"name":"مسجد الخليفه","Latitude":"30.13023949704674","Longitude":"31.6360280290246","address":"Unnamed Road, Al Shorouk, Cairo Governorate, Egypt"},{"id":11,"name":"مستشفي التوحيد","Latitude":"30.420436553709973","Longitude":"31.558967493474483","address":"Al Teraa Al Hamraa, Belbes, Bilbeis, Ash Sharqia Governorate, Egypt"},{"id":12,"name":"مستشفي ام المصريين","Latitude":"30.00628183920731","Longitude":"31.20989441871643","address":"10 El-Sanadeely, Rabaa, Giza District, Giza Governorate, Egypt"},{"id":13,"name":"اسماك وادي النيل","Latitude":"30.080288682257255","Longitude":"31.198441050946716","address":"30 El-Madina El-Monawwara, Al Munirah, Imbaba, Giza Governorate, Egypt"},{"id":14,"name":"حلواني ايتوال","Latitude":"30.120599069787666","Longitude":"31.340207196772095","address":"73 Maher Badawi, AZ Zahraa WA Masaken Al Helmeyah, Ein Shams, Cairo Governorate, Egypt"},{"id":15,"name":"نادي المنيب","Latitude":"29.97842365214904","Longitude":"31.206327080726624","address":"Youssef Soliman, Tersa, Giza District, Giza Governorate, Egypt"},{"id":16,"name":"استاد الاسكندريه","Latitude":"31.198025225231714","Longitude":"29.913847334682945","address":"20 Omar Toson, Bab Sharqi WA Wabour Al Meyah, Qism Bab Sharqi, Alexandria Governorate, Egypt"},{"id":17,"name":"قلعه قايتباي","Latitude":"31.214210509839887","Longitude":"29.885786063969135","address":"Kayetbai, Qesm Al Gomrok, Alexandria Governorate, Egypt"}]
     * error : 0
     * message : this is all favorite places
     */

    private int error;
    private String message;
    private List<DataBean> data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 2
         * name : احلي حلواني
         * Latitude : 30.120301525084546
         * Longitude : 31.340502910315994
         * address : 67 Ismail Al Fangari, AZ Zahraa WA Masaken Al Helmeyah, Ein Shams, Cairo Governorate, Egypt
         */

        private int id;
        private String name;
        private String Latitude;
        private String Longitude;
        private String address;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
