package com.example.taxinaclient.pojo.responses;

import java.util.List;

public class AllCountryResponse {
    /**
     * data : [{"id":1,"name":"مصر","created_at":"2020-07-23 11:24:42","updated_at":"2020-07-23 09:24:42"},{"id":2,"name":"السعوديه","created_at":"2020-07-23 11:24:50","updated_at":"2020-07-23 09:24:50"},{"id":3,"name":"الامارات","created_at":"2020-07-23 11:25:00","updated_at":"2020-07-23 09:25:00"}]
     * error : 0
     * message : show all country
     */

    private int error;
    private String message;
    private List<DataBean> data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * name : مصر
         * created_at : 2020-07-23 11:24:42
         * updated_at : 2020-07-23 09:24:42
         */

        private int id;
        private String name;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
