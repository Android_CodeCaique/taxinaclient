package com.example.taxinaclient.pojo.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AreasResposne {

    @SerializedName("error")
    private int error ;

    @SerializedName("message")
    private String message ;

    @SerializedName("data")
    private List<data>data ;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AreasResposne.data> getData() {
        return data;
    }

    public void setData(List<AreasResposne.data> data) {
        this.data = data;
    }

    public class  data {
        @SerializedName("area_id")
        private String area_id ;

          @SerializedName("area_name")
        private String area_name ;

          @SerializedName("city_name")
        private String city_name ;

        public String getArea_id() {
            return area_id;
        }

        public void setArea_id(String area_id) {
            this.area_id = area_id;
        }

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }
    }
}
