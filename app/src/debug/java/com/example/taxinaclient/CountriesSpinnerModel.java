package com.example.taxinaclient;

public class CountriesSpinnerModel {
    private String name ;
    private int id;

    public CountriesSpinnerModel(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public CountriesSpinnerModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
